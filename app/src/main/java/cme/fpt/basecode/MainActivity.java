package cme.fpt.basecode;

import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import androidx.navigation.NavController;
import androidx.navigation.NavHost;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import cme.fpt.basecode.common.BaseActivity;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.screens.logout.repository.LogoutRepository;
import cme.fpt.basecode.screens.main.MainSharedViewModel;

public class MainActivity extends BaseActivity implements NavHost {
    private DrawerLayout mDrawerLayout;

    @Inject
    LogoutRepository logoutRepository;
    MainSharedViewModel uiViewModel;
    SharedPreferences prefFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);
        setContentView(R.layout.activity_main);

        uiViewModel = ViewModelProviders.of(this).get(MainSharedViewModel.class);

        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        Button button = toolbar.findViewById(R.id.action_bar_button);
        button.setOnClickListener(v -> uiViewModel.getIsActionButtonClick().postValue(true));

        uiViewModel.getButtonActionState().observe(this, list -> {
            if(list.size() >= 2) {
                boolean isShow = (boolean) list.get(0);
                String buttonText = (String) list.get(1);
                if (isShow) {
                    button.setText(buttonText);
                    button.setVisibility(View.VISIBLE);
                } else {
                    button.setVisibility(View.GONE);
                }
            }
        });

        NavigationView navigationView = findViewById(R.id.main_navigation_view);
        NavigationUI.setupWithNavController(navigationView, getNavController());
        mDrawerLayout = findViewById(R.id.main_drawer_layout);

        prefFile = getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        uiViewModel.getMenuItem().observe(this, index -> navigationView.getMenu().getItem(index).setChecked(true));
        uiViewModel.getHideToolbar().observe(this, isHide -> {
            if (isHide) {
                actionBar.hide();
            } else {
                actionBar.show();
            }
        });
    }

    @NonNull
    @Override
    public NavController getNavController() {
        return Navigation.findNavController(this, R.id.nav_host_fragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (uiViewModel.getIsWebviewShow().getValue()) {
            uiViewModel.setIsWebviewShow(false);
        } else {
            super.onBackPressed();
        }
    }
}
