package cme.fpt.basecode;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.util.Calendar;

import javax.inject.Inject;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.util.DateTimeParser;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.ActivityRegisterBinding;
import cme.fpt.basecode.model.User;
import cme.fpt.basecode.screens.register.viewmodel.RegisterViewModel;

public class RegisterActivity extends AppCompatActivity {

    @Inject
    RegisterViewModel.RegisterFactory registerFactory;
    ActivityRegisterBinding binding;
    RegisterViewModel registerViewModel;
    private Calendar selectedDOB = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        binding.setLifecycleOwner(this);
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.nav_back);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);
        addValidationForTextField();
        binding.buttonRegister.setOnClickListener(registerUser);
        binding.registerDOB.setOnFocusChangeListener(DOBClickListener);
    }

    private void addValidationForTextField() {
        binding.registerEmail.addTextChangedListener(emailTextWatcher);
        setValidateLength(binding.registerIDnumber,binding.idnumberLayout, 8, 12);
        setValidateLength(binding.registerPhone, binding.phoneLayout, 10, 12);
        setValidateLength(binding.registerPassword, binding.passwordLayout, 8, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerViewModel = ViewModelProviders.of(this, registerFactory).get(RegisterViewModel.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener registerUser = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(!isAllFieldValid()) {
                createRegisterDialog(R.string.register_field_invalid, false);
            } else {
                binding.progressBar.showHideLoadingState(true);
                registerViewModel.registerUser(getInputUser()).observe(RegisterActivity.this, response -> {
                    if(response != null) {
                        binding.progressBar.showHideLoadingState(false);
                        handleRegisterResponseCode(response);
                    }
                });
            }
        }
    };



    private void createRegisterDialog(int message, boolean isShutdownActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(isShutdownActivity) {
                    createLoginReturnBundle(binding.registerUsername.getText(), binding.registerPassword.getText());
                    finish();
                }
                binding.progressBar.setVisibility(View.GONE);
            }
        });
        builder.setMessage(message);
        builder.create().setCanceledOnTouchOutside(false);
        builder.show();
    }

    private void setValidateLength (EditText view, TextInputLayout errorHolder, int minLength, int maxLength) {
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing because we don't need to handle beforeTextChanged
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Do nothing because we don't need to handle onTextChanged
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int textLength = editable.length();
                if(minLength != 0 && textLength < minLength) {
                    errorHolder.setError(getString(R.string.input_too_short));
                } else if(maxLength != 0 && textLength > maxLength) {
                    errorHolder.setError(getString(R.string.input_too_long));
                } else {
                    errorHolder.setError("");
                }
            }
        });
    }


    private TextWatcher emailTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Do nothing because we don't need to handle beforeTextChanged
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Do nothing because we don't need to handle onTextChanged
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(!isValidEmail(editable)) {
                binding.emailLayout.setError(getString(R.string.invalid_email_format));
            } else {
                binding.emailLayout.setError("");
            }
        }
    };
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private View.OnFocusChangeListener DOBClickListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        selectedDOB.set(year, month, dayOfMonth);
                        String day_string = dayOfMonth > 9 ? Integer.toString(dayOfMonth) : "0" + Integer.toString(dayOfMonth);
                        String month_string = month > 9 ? Integer.toString(month + 1) : "0" + Integer.toString(month + 1);
                        binding.registerDOB.setText(day_string + "/" + month_string + "/" + year);
                        findViewById(v.getNextFocusForwardId()).requestFocus();
                    }}, selectedDOB.get(Calendar.YEAR), selectedDOB.get(Calendar.MONTH), selectedDOB.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        }
    };

    private void handleRegisterResponseCode(BaseResponse response) {
        switch (response.getResponseCode()) {
            case BaseResponse.CODE_513:
                if(response.getErrorMessage().indexOf(getString(R.string.user_duplicate)) != -1) {
                    createRegisterDialog(R.string.error_513_duplicate, false);
                } else {
                    createRegisterDialog(R.string.error_513_invalid, false);
                }
                break;
            case BaseResponse.CODE_400:
                createRegisterDialog(R.string.error_400, false);
                break;
            case BaseResponse.SUCCESS_CODE:
                createRegisterDialog(R.string.code_200, true);
                break;
            case BaseResponse.UNKNOWN_CODE:
                createRegisterDialog(R.string.request_timeout, false);
                break;
            default:
                break;
        }
    }

    private User getInputUser() {
        User user = new User();
        user.setUsername(binding.registerUsername.getText().toString());
        user.setEmail(binding.registerEmail.getText().toString());
        user.setIdentifyNumber(binding.registerIDnumber.getText().toString());
        user.setPassword(binding.registerPassword.getText().toString());
        user.setPhone(binding.registerPhone.getText().toString());
        user.setStrDOB(binding.registerDOB.getText().toString());
        try {
            user.setDateOfBirth(DateTimeParser.getFormattedDateFromString(binding.registerDOB.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return user;
    }

    private void createLoginReturnBundle(Editable username, Editable password) {
        Intent returnIntent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(LoginActivity.USERNAME_INTENT, username.toString());
        bundle.putString(LoginActivity.PASWD_INTENT, password.toString());
        returnIntent.putExtra(LoginActivity.RESULT_BUNDLE, bundle);
        returnIntent.putExtra(LoginActivity.PASWD_INTENT, binding.registerPassword.getText());
        setResult(Activity.RESULT_OK, returnIntent);
    }

    private boolean isAllFieldValid() {
        boolean result;
        result = isTextFieldValid(binding.registerPassword, binding.passwordLayout, R.string.password_empty);
        result = result && isTextFieldValid(binding.registerUsername, binding.usernameLayout, R.string.username_empty);
        result = result && isTextFieldValid(binding.registerDOB, binding.dobLayout, R.string.date_empty);
        result = result && isTextFieldValid(binding.registerPhone, binding.phoneLayout, R.string.phone_empty);
        result = result && isTextFieldValid(binding.registerIDnumber, binding.idnumberLayout, R.string.idNum_empty);
        result = result && isTextFieldValid(binding.registerEmail, binding.emailLayout, R.string.email_empty);
        return result;
    }


    private boolean isTextFieldValid(EditText view, TextInputLayout errorHolder, int messsageId) {
        String message = getResources().getString(messsageId);
        if (TextUtils.isEmpty(view.getText().toString().trim())) {
            errorHolder.setError(message);
            return false;
        } else if ((errorHolder.getError() != null && !errorHolder.getError().equals("") && !errorHolder.getError().equals(message))) {
            return false;
        } else {
            errorHolder.setError("");
            return true;
        }
    }

}
