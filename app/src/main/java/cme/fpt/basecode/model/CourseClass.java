
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CourseClass {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("version")
    @Expose
    private int version;
    @SerializedName("registrations")
    @Expose
    private List<Registration> registrations = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("trainers")
    @Expose
    private List<AIAUser> trainers = null;


    /**
     * No args constructor for use in serialization
     */
    public CourseClass() {
        // Default constructor
    }

    /**
     * @param id
     * @param registrations
     * @param version
     */
    public CourseClass(int id, int version, List<Registration> registrations) {
        super();
        this.id = id;
        this.version = version;
        this.registrations = registrations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AIAUser> getTrainers() {
        return trainers;
    }

    public void setTrainers(List<AIAUser> trainers) {
        this.trainers = trainers;
    }
}
