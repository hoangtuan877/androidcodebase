package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Topic implements Serializable{
    private static final long serialVersionUID = -2002331705351908193L;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments;

    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("countComment")
    @Expose
    private int countComment;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("discussionName")
    @Expose
    private String discussionName;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;

    @SerializedName("owner")
    @Expose
    private Owner owner;

    @SerializedName("parentId")
    @Expose
    private int parentId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("version")
    @Expose
    private int version;

    public Topic() {
        // Default constructor
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCountComment() {
        return countComment;
    }

    public void setCountComment(int countComment) {
        this.countComment = countComment;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDiscussionName() {
        return discussionName;
    }

    public void setDiscussionName(String discussionName) {
        this.discussionName = discussionName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
