package cme.fpt.basecode.model;

public class Result {
    private boolean completed;
    private String evaluation;
    private int id;
    private double rate;
    private Registration registration;
    private int version;

    public Result() {
        // Default constructor
    }

    public Result(boolean completed, String evaluation, int id, double rate, Registration registration, int version) {
        this.completed = completed;
        this.evaluation = evaluation;
        this.id = id;
        this.rate = rate;
        this.registration = registration;
        this.version = version;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
