
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageInfo {

    @SerializedName("defaultUrl")
    @Expose
    private FileUrl defaultUrl;
    @SerializedName("source")
    @Expose
    private FileUrl source;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ImageInfo() {
        // Default constructor
    }

    /**
     * 
     * @param source
     * @param defaultUrl
     */
    public ImageInfo(FileUrl defaultUrl, FileUrl source) {
        super();
        this.defaultUrl = defaultUrl;
        this.source = source;
    }

    public FileUrl getDefaultUrl() {
        return defaultUrl;
    }

    public void setDefaultUrl(FileUrl defaultUrl) {
        this.defaultUrl = defaultUrl;
    }

    public FileUrl getSource() {
        return source;
    }

    public void setSource(FileUrl source) {
        this.source = source;
    }

}
