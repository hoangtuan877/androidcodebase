package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeyValueDTO {

    @SerializedName("key")
    @Expose
    private Object key;

    @SerializedName("value")
    @Expose
    private String value;

    public KeyValueDTO() {
        // Default constructor
    }

    public KeyValueDTO(Object key, String value) {
        this.key = key;
        this.value = value;
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
