
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Registration {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("version")
    @Expose
    private int version;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("clazz")
    @Expose
    private CourseClass clazz;
    @SerializedName("trainee")
    @Expose
    private AIAUser trainee;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("createdDate")
    @Expose
    private long createdDate;
    @SerializedName("certificateImagePath")
    @Expose
    private String certificateImagePath;
    @SerializedName("certificatePath")
    @Expose
    private String certificatePath;

    /**
     * No args constructor for use in serialization
     */
    public Registration() {
        // Default constructor
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CourseClass getClazz() {
        return clazz;
    }

    public void setClazz(CourseClass clazz) {
        this.clazz = clazz;
    }

    public AIAUser getTrainee() {
        return trainee;
    }

    public void setTrainee(AIAUser trainee) {
        this.trainee = trainee;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public String getCertificateImagePath() {
        return certificateImagePath;
    }

    public void setCertificateImagePath(String certificateImagePath) {
        this.certificateImagePath = certificateImagePath;
    }

    public String getCertificatePath() {
        return certificatePath;
    }

    public void setCertificatePath(String certificatePath) {
        this.certificatePath = certificatePath;
    }
}
