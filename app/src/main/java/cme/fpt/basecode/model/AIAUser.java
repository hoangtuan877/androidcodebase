
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AIAUser {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("adminAccount")
    @Expose
    private boolean adminAccount;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("enabled")
    @Expose
    private boolean enabled;
    @SerializedName("superAccount")
    @Expose
    private boolean superAccount;
    @SerializedName("groups")
    @Expose
    private List<Group> groups = null;
    @SerializedName("fullTimeFlag")
    @Expose
    private boolean fullTimeFlag;
    @SerializedName("avatar")
    @Expose
    private ImageInfo avatar;
    @SerializedName("identifyNumber")
    @Expose
    private String identifyNumber;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("leader")
    @Expose
    private boolean leader;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("displayName")
    @Expose
    private String displayName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AIAUser() {
        // Default constructor
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAdminAccount() {
        return adminAccount;
    }

    public void setAdminAccount(boolean adminAccount) {
        this.adminAccount = adminAccount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSuperAccount() {
        return superAccount;
    }

    public void setSuperAccount(boolean superAccount) {
        this.superAccount = superAccount;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public boolean isFullTimeFlag() {
        return fullTimeFlag;
    }

    public void setFullTimeFlag(boolean fullTimeFlag) {
        this.fullTimeFlag = fullTimeFlag;
    }

    public ImageInfo getAvatar() {
        return avatar;
    }

    public void setAvatar(ImageInfo avatar) {
        this.avatar = avatar;
    }

    public String getIdentifyNumber() {
        return identifyNumber;
    }

    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isLeader() {
        return leader;
    }

    public void setLeader(boolean leader) {
        this.leader = leader;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
