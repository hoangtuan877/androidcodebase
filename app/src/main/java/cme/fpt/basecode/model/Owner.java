package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Owner implements Serializable{
    private static final long serialVersionUID = -8067175743396530578L;

    @SerializedName("avatarUrl")
    @Expose
    private String avatarUrl;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("version")
    @Expose
    private Integer version;

    /**
     * No args constructor for use in serialization
     *
     */
    public Owner() {
        // Default constructor
    }


    /***
     * @param avatarUrl
     * @param displayName
     * @param id
     * @param userId
     * @param version
     */
    public Owner(String avatarUrl, String displayName, Integer id, Integer userId, Integer version) {
        super();
        this.avatarUrl = avatarUrl;
        this.displayName = displayName;
        this.id = id;
        this.userId = userId;
        this.version = version;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
