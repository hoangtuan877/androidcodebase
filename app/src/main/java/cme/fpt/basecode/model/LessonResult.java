package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessonResult {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("version")
    @Expose
    private Integer version;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("lesson")
    @Expose
    private Lesson lesson;
    @SerializedName("registration")
    @Expose
    private Registration registration;

    /**
     * No args constructor for use in serialization
     *
     */
    public LessonResult() {
        // Default constructor
    }

    /**
     *
     * @param id
     * @param lesson
     * @param status
     * @param registration
     * @param version
     */
    public LessonResult(Integer id, Integer version, String status, Lesson lesson, Registration registration) {
        super();
        this.id = id;
        this.version = version;
        this.status = status;
        this.lesson = lesson;
        this.registration = registration;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

}