package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RelatedCourseDTO {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("category")
    @Expose
    private Category category;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("rateCount")
    @Expose
    private float rateCount;

    @SerializedName("ratePoint")
    @Expose
    private float ratePoint;

    @SerializedName("startTime")
    @Expose
    private String startTime;

    @SerializedName("posterInfo")
    @Expose
    private ImageInfo posterInfo;

    public RelatedCourseDTO(int code, String s) {
        // Default constructor
    }

    public RelatedCourseDTO(String name, Category category, String type, float rateCount, float ratePoint, String startTime, ImageInfo posterInfo) {
        this.name = name;
        this.category = category;
        this.type = type;
        this.rateCount = rateCount;
        this.ratePoint = ratePoint;
        this.startTime = startTime;
        this.posterInfo = posterInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getRateCount() {
        return rateCount;
    }

    public void setRateCount(float rateCount) {
        this.rateCount = rateCount;
    }

    public float getRatePoint() {
        return ratePoint;
    }

    public void setRatePoint(float ratePoint) {
        this.ratePoint = ratePoint;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public ImageInfo getPosterInfo() {
        return posterInfo;
    }

    public void setPosterInfo(ImageInfo posterInfo) {
        this.posterInfo = posterInfo;
    }
}
