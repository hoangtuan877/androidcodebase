
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Course {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("version")
    @Expose
    private int version;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("summarize")
    @Expose
    private String summarize;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("duration")
    @Expose
    private long duration;
    @SerializedName("mandatory")
    @Expose
    private boolean mandatory;
    @SerializedName("bannerInfo")
    @Expose
    private ImageInfo bannerInfo;
    @SerializedName("posterInfo")
    @Expose
    private ImageInfo posterInfo;
    @SerializedName("scormUrl")
    @Expose
    private FileInfo scormUrl;
    @SerializedName("titles")
    @Expose
    private List<Title> titles = null;
    @SerializedName("classes")
    @Expose
    private List<CourseClass> classes = null;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("approve")
    @Expose
    private String approve;
    @SerializedName("approver")
    @Expose
    private AIAUser approver;
    @SerializedName("lessons")
    @Expose
    private List<Lesson> lessons = null;
    @SerializedName("rateCount")
    @Expose
    private float rateCount;
    @SerializedName("ratePoint")
    @Expose
    private float ratePoint;
    @SerializedName("registedDate")
    @Expose
    private int registedDate;
    @SerializedName("daysOfReset")
    @Expose
    private int daysOfReset;
    @SerializedName("completedDate")
    @Expose
    private int completedDate;
    @SerializedName("banner")
    @Expose
    private boolean banner;
    @SerializedName("available")
    @Expose
    private boolean available;
    @SerializedName("documents")
    @Expose
    private List<Document> documents;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    /**
     * No args constructor for use in serialization
     */
    public Course() {
        // Default constructor
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description.trim();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummarize() {
        return summarize;
    }

    public void setSummarize(String summarize) {
        this.summarize = summarize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public ImageInfo getBannerInfo() {
        return bannerInfo;
    }

    public void setBannerInfo(ImageInfo bannerInfo) {
        this.bannerInfo = bannerInfo;
    }

    public ImageInfo getPosterInfo() {
        return posterInfo;
    }

    public void setPosterInfo(ImageInfo posterInfo) {
        this.posterInfo = posterInfo;
    }

    public FileInfo getScormUrl() {
        return scormUrl;
    }

    public void setScormUrl(FileInfo scormUrl) {
        this.scormUrl = scormUrl;
    }

    public List<Title> getTitles() {
        return titles;
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

    public List<CourseClass> getClasses() {
        return classes;
    }

    public void setClasses(List<CourseClass> classes) {
        this.classes = classes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public AIAUser getApprover() {
        return approver;
    }

    public void setApprover(AIAUser approver) {
        this.approver = approver;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public float getRateCount() {
        return rateCount;
    }

    public void setRateCount(float rateCount) {
        this.rateCount = rateCount;
    }

    public float getRatePoint() {
        return ratePoint;
    }

    public void setRatePoint(float ratePoint) {
        this.ratePoint = ratePoint;
    }

    public int getRegistedDate() {
        return registedDate;
    }

    public void setRegistedDate(int registedDate) {
        this.registedDate = registedDate;
    }

    public int getDaysOfReset() {
        return daysOfReset;
    }

    public void setDaysOfReset(int daysOfReset) {
        this.daysOfReset = daysOfReset;
    }

    public int getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(int completedDate) {
        this.completedDate = completedDate;
    }

    public boolean isBanner() {
        return banner;
    }

    public void setBanner(boolean banner) {
        this.banner = banner;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public static Course createDummy() {
        Course course = new Course();
        course.setId(37);
        course.setVersion(4);
        course.setCode("UUZG3812");
        course.setName("Build your first Jetpack app");
        course.setDescription("Jetpack is a set of libraries, tools and architectural guidance to help make it quick and easy to build great Android apps. It provides common infrastructure code so you can focus on what makes your app unique.\n");
        course.setType("ONLINE");
        course.setStartTime("2018-05-22T17:00:00.00");
        course.setActive(true);
        course.setDuration(0);
        course.setMandatory(true);

        ImageInfo bannerInfo = new ImageInfo();
        FileUrl bannerInfoDefault = new FileUrl();
        bannerInfoDefault.setFileUrl("http://d15qj6kty21dv0.cloudfront.net/static-resource/icon-courses-default-banner.png?Expires=1527132084&Signature=Ye0I3fvYcC9RulpKEFHQiO9vdU4CIOKlacU2G0WRGfuxJmbOU94mL~muhzfB6EmwUpzvuFhRKGZyh3kWuBv5A3t5OCGZGPVQsRb46H6pxhczHLwNE3oZPEx8FlxRXU-5LidxdI4MrxfM-qkFbSuFgOLX4HNbJ2MS1gRqj0~7zw1oDvNQZyOgJ7SexvRswsTsqrO0Y6WCxrNH39MAT9SAlz7UPt4MWnEMiK-nGJZkezyx8IGZgQR103-1jjfhJdqFIScna0LIzV6E2OjgKCBGOQXOLgOOZXYPzTF80sZ5hWCKsyybA5rCroD3pX7uHPU4FQMcjL99GZcAjo8QMLUG4Q__&Key-Pair-Id=AKIAJOGSOUQ2YA5J473A");
        bannerInfoDefault.setFilePath("static-resource/icon-courses-default-banner.png");
        bannerInfo.setDefaultUrl(bannerInfoDefault);

        FileUrl bannerInfoSource = new FileUrl();
        bannerInfoSource.setFileUrl("http://d15qj6kty21dv0.cloudfront.net/UUZG3812/BANNER_slider1.jpg?Expires=1527132084&Signature=YQ4ys4tnbswPfcPxoGI1QlBvlIw7Y2txJm2WqFWsxxXYr8cyaCDwHELSlrpz1eJu2g0epLkscrQqvPmuEfcAWCdlCzOgyWYvMOFsMg9~HS1WitAqcYHGVAaT6tFbm-IoJgsPc1ZzsZhuz81nlamyeoBAgwqguuyI8ZnHk6BBPuunayUHjS0dbQTKl~Bs1cmh67viczy~q0lRKS5qSQEctH5qO8YwVtPRSeoin3IVHxU2C7FuVhXueSe0F58JBr62-LOby8ram3o5qAcUfVms63L5fCFPbZDht00o4egSy~rvxNVQYuYt5uhoxQRHScIZwQRBOwo06bwd~2lZubxakQ__&Key-Pair-Id=AKIAJOGSOUQ2YA5J473A");
        bannerInfoSource.setFilePath("UUZG3812/BANNER_slider1.jpg");
        bannerInfo.setSource(bannerInfoSource);

        course.setBannerInfo(bannerInfo);

        ImageInfo posterInfo = new ImageInfo();
        FileUrl posterInfoDefault = new FileUrl();
        posterInfoDefault.setFileUrl("http://d15qj6kty21dv0.cloudfront.net/static-resource/icon-courses-default.png?Expires=1527132083&Signature=hT2cS-uhlIHKOU5JQuycU1aSPXEVu-XvgWv5t8GBCu7By37z3~~tVWMkAWO-SpCK3pQQmHJFlri86LaAhWmqgYGLeec3eIhaib35H6Nz1P1kSYhPMwti1CV3-VnLd-Et~RfqbvvgAjw4IAhMWejZEihaBBRHJZ6mGdVy4IdtbcjuRufUVgqL~2uIaz6GvjoPCUCDZlpXtOnEZLw206cgWYHBEf5QnfIrUscVqLBv6ocUNZSbg4t~AtQereQKzwAjz6B2AChHgP7Hkn0atFJZmCGVIxtA4dP5kJKMHpwKc8IA0-pk3RAC4~gQg2sFAzKyy5BsNdNXKyNbunfTZAdN7Q__&Key-Pair-Id=AKIAJOGSOUQ2YA5J473A");
        posterInfoDefault.setFilePath("static-resource/icon-courses-default.png");
        posterInfo.setDefaultUrl(posterInfoDefault);

        FileUrl posterInfoSource = new FileUrl();
        posterInfoSource.setFileUrl("http://d15qj6kty21dv0.cloudfront.net/UUZG3812/POSTER_Hinh trai cay.jpg?Expires=1527132084&Signature=fHA0f64clvGnP0XtEPYbRmlx7cnyXNnq1Jdww91SCsvJCyAloWk3aepbXNe5Pys78iNFEAj2rmuuhAZK-ho1uqOR1NUxJYf6VlZzc6xH300-rl0x~4pw8iri9t-Sc5A3P1vMN2wdV~ngqj3wceVCc9qZ7VAryvJPBKeubM0pWXpu~RL6H5YU03n0GY6GkJOljEP7589YA~sdttQGLXdgPRmQw7AUpT~4Y5xCQUrEtFf-CoVKW5V7Trk4ZTkEIQTEiuqnJlUpm~fV41NgRMsK5gspi~u4sAejt1teoYjKiPdkllYBP-UjX1tE2xCveFX5IAil03YlmDUUOU2c1N1zJw__&Key-Pair-Id=AKIAJOGSOUQ2YA5J473A");
        posterInfoSource.setFilePath("UUZG3812/POSTER_Hinh trai cay.jpg");
        posterInfo.setSource(posterInfoSource);

        course.setPosterInfo(posterInfo);

        List<Title> titles = new ArrayList<>();
        Title title1 = new Title();
        title1.setId(23);
        title1.setVersion(1);
        title1.setCode("Candidate");
        title1.setName("Candidate");
        title1.setDisplayName("Ứng viên");
        title1.setChannel(new Channel(1, 1, "Agency", "Agency", "Đại lý"));
        titles.add(title1);

        course.setTitles(titles);

        Category category = new Category();
        category.setId(3);
        category.setVersion(1);
        category.setCode("C3");
        category.setName("Android Developer");
        course.setCategory(category);

        course.setApprove("APPROVED");

        course.setRateCount(1);
        course.setRatePoint(3.5f);

        course.setRegistedDate(0);
        course.setDaysOfReset(1);
        course.setCompletedDate(0);
        course.setBanner(true);

        return course;
    }
}
