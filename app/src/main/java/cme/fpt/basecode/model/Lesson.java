
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lesson {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("version")
    @Expose
    private int version;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mainActivityId")
    @Expose
    private String mainActivityId;
    @SerializedName("indexFilePath")
    @Expose
    private String indexFilePath;

    @SerializedName("course")
    @Expose
    private Course course;

    @SerializedName("description")
    @Expose
    private String description;


    /**
     * No args constructor for use in serialization
     */
    public Lesson() {
        // Default constructor
    }

    /**
     * @param id
     * @param indexFilePath
     * @param name
     * @param mainActivityId
     * @param code
     * @param version
     */
    public Lesson(int id, int version, String code, String name, String mainActivityId, String indexFilePath) {
        super();
        this.id = id;
        this.version = version;
        this.code = code;
        this.name = name;
        this.mainActivityId = mainActivityId;
        this.indexFilePath = indexFilePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainActivityId() {
        return mainActivityId;
    }

    public void setMainActivityId(String mainActivityId) {
        this.mainActivityId = mainActivityId;
    }

    public String getIndexFilePath() {
        return indexFilePath;
    }

    public void setIndexFilePath(String indexFilePath) {
        this.indexFilePath = indexFilePath;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
