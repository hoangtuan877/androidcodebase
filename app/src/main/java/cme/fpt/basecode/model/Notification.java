package cme.fpt.basecode.model;

public class Notification {

    private int contentId;
    private String type;
    private String content;
    private boolean seen;
    private String createdDate;
    private int id;
    private int version;

    public Notification() {
        // Default constructor
    }


    public Notification(int contentId, String type, String title, boolean isRead, String time, int id, int version) {
        this.contentId = contentId;
        this.type = type;
        this.content = title;
        this.seen = isRead;
        this.createdDate = time;
        this.id = id;
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String title) {
        this.content = title;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean read) {
        seen = read;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String time) {
        this.createdDate = time;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
