
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("version")
    @Expose
    private int version;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("posterUrl")
    @Expose
    private FileUrl posterUrl;
    @SerializedName("iconMobileUrl")
    @Expose
    private FileUrl iconMobileUrl;
    @SerializedName("numberOfCourses")
    @Expose
    private int numberOfCourses;


    /**
     * No args constructor for use in serialization
     * 
     */
    public Category() {
        // Default constructor
    }

    /**
     * 
     * @param id
     * @param description
     * @param name
     * @param iconMobileUrl
     * @param code
     * @param posterUrl
     * @param version
     */
    public Category(int id, int version, String code, String name, String description, FileUrl posterUrl, FileUrl iconMobileUrl) {
        super();
        this.id = id;
        this.version = version;
        this.code = code;
        this.name = name;
        this.description = description;
        this.posterUrl = posterUrl;
        this.iconMobileUrl = iconMobileUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FileUrl getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(FileUrl posterUrl) {
        this.posterUrl = posterUrl;
    }

    public FileUrl getIconMobileUrl() {
        return iconMobileUrl;
    }

    public void setIconMobileUrl(FileUrl iconMobileUrl) {
        this.iconMobileUrl = iconMobileUrl;
    }

    public int getNumberOfCourses() {
        return numberOfCourses;
    }

    public void setNumberOfCourses(int numberOfCourses) {
        this.numberOfCourses = numberOfCourses;
    }
}
