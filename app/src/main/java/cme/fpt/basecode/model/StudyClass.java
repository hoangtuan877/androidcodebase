package cme.fpt.basecode.model;

public class StudyClass {
    private int courseId;
    private String courseName;
    private int lessonId;
    private String lessonName;
    private int categoryId;
    private String categoryName;
    private String lessonStatus;

    public StudyClass() {
        // Default constructor
    }

    public StudyClass(int courseId, String courseName, int lessonId, String lessonName, int categoryId, String categoryName, String lessonStatus) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.lessonId = lessonId;
        this.lessonName = lessonName;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.lessonStatus = lessonStatus;
    }

    public StudyClass(int courseId, String courseName, String categoryName, String lessonName) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.lessonName = lessonName;
        this.categoryName = categoryName;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getLessonStatus() {
        return lessonStatus;
    }

    public void setLessonStatus(String lessonStatus) {
        this.lessonStatus = lessonStatus;
    }
}
