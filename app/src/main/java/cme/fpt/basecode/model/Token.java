package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Token {
    @SerializedName("token")
    @Expose
    String tokenValue;

    public Token(String token) {
        this.tokenValue = token;
    }

    public Token() {
        // Default constructor
    }

    public String getToken() {
        return tokenValue;
    }

    public void setToken(String token) {
        this.tokenValue = token;
    }
}
