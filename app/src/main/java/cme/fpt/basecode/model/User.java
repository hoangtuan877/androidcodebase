package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("username")
    @Expose
    public String username;

    @SerializedName("identifyNumber")
    @Expose
    public String identifyNumber;

    @SerializedName("dateOfBirth")
    @Expose
    public String dateOfBirth;

    @SerializedName("strDOB")
    @Expose
    public String strDOB;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("password")
    @Expose
    public String password;

    public User() {
        // Default constructor
    }

    public User(String username, String pass) {
        this.username = username;
        this.password = pass;
    }

    public User(String username, String identifyNumber, String dateOfBirth, String strDOB, String email, String phone, String password) {
        this.username = username;
        this.identifyNumber = identifyNumber;
        this.dateOfBirth = dateOfBirth;
        this.strDOB = strDOB;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdentifyNumber() {
        return identifyNumber;
    }

    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getStrDOB() {
        return strDOB;
    }

    public void setStrDOB(String strDOB) {
        this.strDOB = strDOB;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
