package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Discussion {

    @SerializedName("category")
    @Expose
    private KeyValueDTO category;

    @SerializedName("countTopic")
    @Expose
    private int countTopic;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("lectures")
    @Expose
    private String lectures;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("registeredDate")
    @Expose
    private String registeredDate;

    @SerializedName("startTime")
    @Expose
    private String startTime;

    @SerializedName("topics")
    @Expose
    private List<Topic> topics;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("version")
    @Expose
    private int version;

    public Discussion() {
        // Default constructor
    }

    public KeyValueDTO getCategory() {
        return category;
    }

    public void setCategory(KeyValueDTO category) {
        this.category = category;
    }

    public int getCountTopic() {
        return countTopic;
    }

    public void setCountTopic(int countTopic) {
        this.countTopic = countTopic;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLectures() {
        return lectures;
    }

    public void setLectures(String lectures) {
        this.lectures = lectures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(String registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
