
package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileUrl {

    @SerializedName("fileUrl")
    @Expose
    private String fileUrlValue;
    @SerializedName("filePath")
    @Expose
    private String filePath;

    /**
     * No args constructor for use in serialization
     * 
     */
    public FileUrl() {
        // Default constructor
    }

    /**
     * 
     * @param filePath
     * @param fileUrl
     */
    public FileUrl(String fileUrl, String filePath) {
        super();
        this.fileUrlValue = fileUrl;
        this.filePath = filePath;
    }

    public String getFileUrl() {
        return fileUrlValue;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrlValue = fileUrl;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
