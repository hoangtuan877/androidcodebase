package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingCourse {
    @SerializedName("averagePoint")
    @Expose
    private float averagePoint;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("countRating")
    @Expose
    private int countRating;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("starList")
    @Expose
    private List<RatingCount> starList;
    @SerializedName("version")
    @Expose
    private int version;

    public RatingCourse() {
        // Default constructor
    }

    public RatingCourse(String code) {
        this.code = code;
    }

    public float getAveragePoint() {
        return averagePoint;
    }

    public void setAveragePoint(float averagePoint) {
        this.averagePoint = averagePoint;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCountRating() {
        return countRating;
    }

    public void setCountRating(int countRating) {
        this.countRating = countRating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RatingCount> getStarList() {
        return starList;
    }

    public void setStarList(List<RatingCount> starList) {
        this.starList = starList;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
