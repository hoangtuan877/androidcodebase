package cme.fpt.basecode.model;

import java.util.List;

public class UserInfo {
    String username;
    int id;
    boolean accountNonExpired;
    boolean accountNonLocked;
    boolean credentialsNonExpired;
    boolean enabled;
    boolean superAccount;
    boolean adminAccount;
    List<Authority> authorities;

    public UserInfo() {
        // Default constructor
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSuperAccount() {
        return superAccount;
    }

    public void setSuperAccount(boolean superAccount) {
        this.superAccount = superAccount;
    }

    public boolean isAdminAccount() {
        return adminAccount;
    }

    public void setAdminAccount(boolean adminAccount) {
        this.adminAccount = adminAccount;
    }

}
