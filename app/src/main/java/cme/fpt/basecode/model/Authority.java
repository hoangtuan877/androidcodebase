package cme.fpt.basecode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Authority {
    @SerializedName("authority")
    @Expose
    String authorityValue;

    public Authority(String authorityValue) {
        this.authorityValue = authorityValue;
    }

    public Authority() {
        // Default constructor
    }

    public String getAuthorityValue() {
        return authorityValue;
    }

    public void setAuthorityValue(String authorityValue) {
        this.authorityValue = authorityValue;
    }
}
