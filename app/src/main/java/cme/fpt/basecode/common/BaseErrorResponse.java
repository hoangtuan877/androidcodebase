package cme.fpt.basecode.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseErrorResponse {
    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("msg")
    @Expose
    private String msg;

    public BaseErrorResponse() {
    }

    public BaseErrorResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
