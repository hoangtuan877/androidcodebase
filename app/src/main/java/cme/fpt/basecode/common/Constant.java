package cme.fpt.basecode.common;

public class Constant {

    private Constant() {
        // Default constructor
    }
    public static final String DASH = "/";
    public static final String APP_SHAREDPREFERENCES = "AIAiCampus";
    public static final String USER_TOKEN = "USER_TOKEN";
    public static final String SERVER_ERROR = "SERVER_ERROR";
    public static final String DATE_FORMAT_OUTPUT = "dd/MM/yyyy";
    public static final String DATE_FORMAT_OUTPUT2 = "dd/MM/yyyy - HH:mm";
    public static final String DATE_FORMAT_INPUT = "yyyy-MM-dd'T'HH:mm:ss.SS";
    public static final String COMPLETED_STATUS = "COMPLETED";
    public static final String FAILED_STATUS = "FAILED";
    public static final String PASSED_STATUS = "PASSED";
    public static final String IN_PROGRESS_STATUS = "IN_PROGRESS";
    public static final String OPEN_STATUS = "OPEN";
    public static final String NONE_STATUS = "NONE";
    public static final String NOT_STARTED_YET_STATUS = "NOT_STARTED_YET";
    public static final String LRS_ENDPOINT = "LRS_ENDPOINT";
    public static final String LRS_BASIC_AUTH = "LRS_BASIC_AUTH";
    public static final String USERNAME = "USERNAME";
    public static final int PAGE_RELATED_COURSES = 0;
    public static final int SIZE_RELATED_COURSES = 5;
    public static final String TYPE_RELATED_COURSES = "HOT";

    public static final String CURRENT_USER_ID = "CURRENT_USER_ID";
    public static final String CURRENT_USER_DISPLAYNAME = "CURRENT_USER_DISPLAYNAME";
    public static final String AVATAR_URL = "AVATAR_URL";
    public static final String IS_STAFF = "IS_STAFF";

    //Notification Type
    public static final String REGISTER_SUCCESS = "REGISTER_SUCCESS";
    public static final String COMMENT = "COMMENT";
    public static final String REPLY = "REPLY";
    public static final String STARTING_SOON = "STARTING_SOON";
    public static final String COURSE_COMPLETE = "COURSE_COMPLETE";


    public static final String LAST_MODIFIED_DATE_DESC = "lastModifiedDate,desc";
    public static final String CREATED_DATE_DESC = "createdDate,desc";
    public static final String CREATED_DATE_ASC = "createdDate,asc";
    public static final String REGISTERED_DATE_DESC = "registeredDate,desc";

    public static final String SCOM_CONTENT_ACTOR = "actor=%7B%22name%22%3A%22username%22%2C%22account%22%3A%7B%22homePage%22%3A%22http%3A%2F%2Faia.com.vn%22%2C%22name%22%3A%20%22username%22%7D%7D";

    public static final int COURSE_DESCRIPTION_LIMIT_LINE = 5;

    public static final int SIZE_DISCUSSION_COURSES = 10;
    public static final int PAGE_DISCUSSION_COURSES = 0;

    public static final int SIZE_DISCUSSION_TOPICS = 15;
    public static final int PAGE_DISCUSSION_TOPICS = 0;

    public static final int SIZE_DISCUSSION_COMMENT = 10;
    public static final int PAGE_DISCUSSION_COMMENT = 0;

    //Action for discussion page
    public static final String CREATE_COMMENT = "create_comment";
    public static final String UPDATE_COMMENT = "update_comment";
    public static final String CREATE_REPLY = "create_reply";
    public static final String UPDATE_REPLY = "update_reply";
}