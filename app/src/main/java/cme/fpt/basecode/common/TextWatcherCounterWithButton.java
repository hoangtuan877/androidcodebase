package cme.fpt.basecode.common;

import android.text.Editable;
import android.widget.Button;
import android.widget.TextView;

import cme.fpt.basecode.R;

public class TextWatcherCounterWithButton extends TextWatcherCounter {
    Button button;
    int enableCount;
    public TextWatcherCounterWithButton(int counterStringId, TextView textViewCounter, Button button, int enableCount) {
        super(counterStringId,textViewCounter);
        this.button = button;
        this.enableCount = enableCount;
    }

    @Override
    public void afterTextChanged(Editable editable) {
        super.afterTextChanged(editable);
        //Do nothing here
        if(editable.length() >= enableCount){
            button.setEnabled(true);
            button.setBackgroundColor(button.getResources().getColor(R.color.colorBtnActive, button.getContext().getTheme()));
            button.setTextColor(button.getResources().getColor(android.R.color.white, button.getContext().getTheme()));
        } else {
            button.setEnabled(false);
            button.setBackgroundColor(button.getResources().getColor(R.color.colorBtnInactive, button.getContext().getTheme()));
            button.setTextColor(button.getResources().getColor(R.color.colorTextSub4, button.getContext().getTheme()));
        }
    }
}
