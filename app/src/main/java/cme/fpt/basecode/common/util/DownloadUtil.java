package cme.fpt.basecode.common.util;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DownloadUtil {
    private DownloadUtil() {
    }

    public static long downloadData(Context context, String host, String path, String filename, String title, String courseName) {

        long downloadReference = 0l;
        try {
            Uri uri = Uri.parse(host + URLEncoder.encode(path, "UTF-8"));
            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle(title);
            String fileURL = "/AIAiCampus/" + courseName + "/" + filename;

            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileURL);
            String notificationLink = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + fileURL;
            downloadReference = downloadManager.enqueue(request);
            File file = new File(notificationLink);
            downloadManager.addCompletedDownload(filename, filename, true, "application/pdf", file.getAbsolutePath(), file.length(), true);
            return downloadReference;
        } catch (UnsupportedEncodingException e) {
            // Logger will apply later
        }

        return downloadReference;
    }
}
