package cme.fpt.basecode.common;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class BaseSharedViewModel extends ViewModel{
    MutableLiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();

    public MutableLiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(MutableLiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }
}
