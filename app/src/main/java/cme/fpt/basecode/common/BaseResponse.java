package cme.fpt.basecode.common;

public class BaseResponse {
    public static final int UNKNOWN_CODE = 600;
    public static final int SUCCESS_CODE = 200;
    public static final int CODE_400 = 400;
    public static final int CODE_403 = 403;
    public static final int CODE_401 = 401;
    public static final int CODE_500 = 500;
    public static final int CODE_513 = 513;
    public static final int CODE_404 = 404;
    public static final int CODE_201 = 201;

    public static final String BAD_REQUEST = "Bad Request";
    public static final String DEL_BY_OTHER = "resource.deleted.by.other.user";
    public static final String HAS_BEEN_DELETED = "has.been.deleted";

    protected int responseCode;
    protected String errorMessage;

    public BaseResponse() {

    }

    public BaseResponse(int code, String errorMessage) {
        this.responseCode = code;
        this.errorMessage = errorMessage;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int code) {
        this.responseCode = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
}
