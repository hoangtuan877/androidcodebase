package cme.fpt.basecode.common;

public class ApiUrl {
    public static final String BASE_URL = "http://13.251.1.164:8134/elearning/api/";
    public static final String DOC_URL = "https://s3-ap-southeast-1.amazonaws.com/aia-elearning-course-material-on-aws-new/";
    public static final String S3_URL = "http://aia-elearning-course-material-on-aws-new.s3-ap-southeast-1.amazonaws.com";

    private ApiUrl() {
    }
}
