package cme.fpt.basecode.common;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

public class TextWatcherCounter implements TextWatcher {

    int counterStringId;
    TextView textViewCounter;

    public TextWatcherCounter(int counterStringId, TextView textViewCounter) {
        this.counterStringId = counterStringId;
        this.textViewCounter = textViewCounter;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //Do nothing here
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //Do nothing here
    }

    @Override
    public void afterTextChanged(Editable editable) {
        //Do nothing here
        String counterTxt = this.textViewCounter.getResources().getString(counterStringId, editable.length());
        this.textViewCounter.setText(counterTxt);
    }
}
