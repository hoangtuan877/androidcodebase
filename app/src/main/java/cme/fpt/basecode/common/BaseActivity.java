package cme.fpt.basecode.common;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import cme.fpt.basecode.LoginActivity;
import cme.fpt.basecode.R;

public class BaseActivity extends AppCompatActivity {
    protected BaseSharedViewModel errorSharedViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        errorSharedViewModel = ViewModelProviders.of(this).get(BaseSharedViewModel.class);

        errorSharedViewModel.getErrorResponse().observe(this, response -> {
            switch (response.getCode()) {
                case BaseResponse.CODE_401:
                    createRegisterDialog(R.string.enroll_401, true);
                    break;
                case BaseResponse.CODE_201:
                    createRegisterDialog(R.string.enroll_201, false);
                    break;
                case BaseResponse.CODE_400:
                    if (response.getMsg().contains(BaseResponse.BAD_REQUEST)) {
                        createRegisterDialog(R.string.error_unknown, false);
                    } else if (response.getMsg().contains(BaseResponse.DEL_BY_OTHER)) {
                        createRegisterDialog(R.string.error_400_delete, false);
                    } else if (response.getMsg().contains(BaseResponse.HAS_BEEN_DELETED)) {
                        createRegisterDialog(R.string.error_400_has_delete, false);
                    } else {
                        createRegisterDialog(R.string.error_400, false);
                    }
                    break;
                case BaseResponse.CODE_403:
                    createRegisterDialog(R.string.enroll_403, false);
                    break;
                case BaseResponse.CODE_404:
                    createRegisterDialog(R.string.enroll_404, false);
                    break;
                case BaseResponse.CODE_500:
                    createRegisterDialog(R.string.error_500_fail, false);
                    break;
                default:
                    createRegisterDialog(R.string.error_unknown, false);
                    break;

            }
        });
    }


    public void refreshTokenForLogout() {
        SharedPreferences prefFile = getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefFile.edit();
        if (!prefFile.getString(Constant.USER_TOKEN, "").isEmpty()) {
            editor.putString(Constant.USER_TOKEN, "");
            editor.commit();
        }
    }

    public void showLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void createRegisterDialog(int message, boolean isCallLogin) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("OK", (dialog, id) -> {
            if (isCallLogin) {
                refreshTokenForLogout();
                showLoginActivity();
            } else {
                onBackPressed();
            }
        });
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
