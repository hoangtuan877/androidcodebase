package cme.fpt.basecode.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import cme.fpt.basecode.R;
import cme.fpt.basecode.model.Notification;

import static android.content.Context.MODE_PRIVATE;

public class BindingViewAdapter {

    private BindingViewAdapter() {
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.get().load(imageUrl).into(imageView);
        }
    }

    @BindingAdapter({"imageUrlS3"})
    public static void loadImageS3(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            imageUrl = ApiUrl.DOC_URL + imageUrl;
            Picasso.get().load(imageUrl).into(imageView);
        }
    }

    @BindingAdapter({"discussionAvatar"})
    public static void setDiscussionAvatar(ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) {
            Drawable defaultAccount = imageView.getResources().getDrawable(R.drawable.ic_account_circle_black_24dp);
            imageView.setImageDrawable(defaultAccount);
        } else {
            Picasso.get().load(url).into(imageView);
        }
    }

    @BindingAdapter({"loadCertificate"})
    public static void loadCertificate(ImageView imageView, String imageUrl) {
        imageView.setClickable(false);
        if (!TextUtils.isEmpty(imageUrl)) {
            Handler handler = new Handler();
            Runnable runnable = () -> {
                String certificateUrl = ApiUrl.DOC_URL + imageUrl;
                Log.e("loadCertificate", certificateUrl);
                Picasso.get().load(certificateUrl).into(imageView);
                imageView.setClickable(true);
            };
            handler.postDelayed(runnable, 3000);
        }
    }

    @BindingAdapter({"startDate", "endDate"})
    public static void setStartAndEndDate(TextView view, String startDate, String endDate) {
        if (!TextUtils.isEmpty(startDate)) {
            SimpleDateFormat outputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_OUTPUT);
            SimpleDateFormat inputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_INPUT);
            Date sDate;
            Date eDate;
            String date = "";
            try {
                sDate = inputDateFormat.parse(startDate);
                date = outputDateFormat.format(sDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(endDate)) {
                try {
                    eDate = inputDateFormat.parse(endDate);
                    date = date + " - " + outputDateFormat.format(eDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                date = date + " - " + view.getResources().getString(R.string.infinity_time);
            }
            view.setText(date);
        }


    }

    @BindingAdapter({"date"})
    public static void setDateToTextView(TextView view, String date) {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_OUTPUT);
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_INPUT);
        Date sDate;
        if (!TextUtils.isEmpty(date)) {

            try {
                sDate = inputDateFormat.parse(date);
                String displayDate = outputDateFormat.format(sDate);
                view.setText(displayDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @BindingAdapter({"name", "date"})
    public static void setNameAndDateToTextView(TextView view, String name, String date) {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_OUTPUT);
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_INPUT);
        Date sDate;
        if (!TextUtils.isEmpty(date)) {
            try {
                sDate = inputDateFormat.parse(date);
                String displayDate = outputDateFormat.format(sDate);
                view.setText(name + ", " + displayDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @BindingAdapter({"dateTime"})
    public static void setDateTimeToTextView(TextView view, Long dateTime) {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_OUTPUT);
        Date sDate;

        if (dateTime == null) {
            view.setText("-");
            return;
        }
        sDate = new Date(dateTime);
        String displayDate = outputDateFormat.format(sDate);
        view.setText(displayDate);
    }

    @BindingAdapter({"notificationText"})
    public static void setNotificationTitle(TextView view, Notification data) {
        Context context = view.getContext();
        String courseName = '"' + data.getContent() + '"';
        String message = "";
        switch (data.getType()) {
            case Constant.REGISTER_SUCCESS:
                message = context.getString(R.string.dashboard_notification_register_success, courseName);
                break;
            case Constant.COMMENT:
                message = context.getString(R.string.dashboard_notification_comment, courseName);
                break;
            case Constant.REPLY:
                message = context.getString(R.string.dashboard_notification_reply, courseName);
                break;
            case Constant.STARTING_SOON:
                message = context.getString(R.string.dashboard_notification_starting_soon, courseName);
                break;
            case Constant.COURSE_COMPLETE:
                message = context.getString(R.string.dashboard_notification_couse_success, courseName);
                break;
            default:
                message = view.getContext().getString(R.string.dashboard_notification_register_success, courseName);
                break;
        }
        view.setText(message);
    }

    @BindingAdapter({"html"})
    public static void setHtmlToTextView(TextView view, String html) {
        if (!TextUtils.isEmpty(html)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                view.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));

            } else {
                view.setText(Html.fromHtml(html));
            }
        }
    }

    @BindingAdapter({"countRating", "totalRating"})
    public static void setProgressBar(ProgressBar view, int countRating, int totalRating) {
        if (countRating != 0) {
            int result = (int) (((float) totalRating / (float) countRating) * 100);
            view.setProgress(result);
        } else {
            view.setProgress(0);
        }
    }

    @BindingAdapter({"setNotificationDate"})
    public static void setDateToNotification(TextView view, String date) {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_OUTPUT2);
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT_INPUT);
        Date sDate;
        if (!TextUtils.isEmpty(date)) {

            try {
                inputDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                sDate = inputDateFormat.parse(date);
                String displayDate = outputDateFormat.format(sDate);
                view.setText(displayDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @BindingAdapter({"read"})
    public static void setHightLight(TextView view, boolean read) {
        SharedPreferences prefFile = view.getContext().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        int currentTopicOwner = prefFile.getInt("currentTopicOwner", 0);
        int currentUserId = prefFile.getInt(Constant.CURRENT_USER_ID, 0);
        if(currentTopicOwner == currentUserId && !read){
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.INVISIBLE);
        }
    }

    @BindingAdapter({"ownerIdUpdate"})
    public static void showBtnUpdate(ImageView view, int ownerId) {
        SharedPreferences prefFile = view.getContext().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        int currentUserId = prefFile.getInt(Constant.CURRENT_USER_ID, 0);
        boolean isStaff = prefFile.getBoolean(Constant.IS_STAFF, false);

        if(currentUserId == ownerId || isStaff){
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.GONE);
        }
    }
}
