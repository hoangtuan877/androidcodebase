package cme.fpt.basecode.common.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import cme.fpt.basecode.R;
import cme.fpt.basecode.common.BaseResponse;

public class ResponseDialogBuilder extends AlertDialog.Builder {
    public ResponseDialogBuilder(@NonNull Context context) {
        super(context);
    }

    public ResponseDialogBuilder(@NonNull Context context, int responseCode) {
        super(context);
        switch (responseCode) {
            case BaseResponse.SUCCESS_CODE:
                this.setMessage(context.getString(R.string.enroll_success));
                break;

            case BaseResponse.CODE_201:
                this.setMessage(context.getString(R.string.enroll_201));
                break;
            case BaseResponse.CODE_401:
                this.setMessage(context.getString(R.string.enroll_401));
                break;

            case BaseResponse.CODE_403:
                this.setMessage(context.getString(R.string.enroll_403));
                break;

            case BaseResponse.CODE_404:
                this.setMessage(context.getString(R.string.enroll_404));
                break;
            default:
                this.setMessage(context.getString(R.string.error_unknown));
                break;
        }
    }
}
