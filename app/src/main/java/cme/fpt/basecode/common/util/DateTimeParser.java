package cme.fpt.basecode.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeParser {private
        
     DateTimeParser() {
    }

    private static final String DATETIME_OFFSET_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static String getFormattedDateFromString(String time) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(time);
        String formattedDate = parseDateTimeToOffsetFormat(date);
        return formattedDate;

    }
    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds) {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String formattedDate = parseDateTimeToOffsetFormat(date);
        return formattedDate;

    }

    public static String parseDateTimeToOffsetFormat(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_OFFSET_FORMAT);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(date) + "Z";
    }
}
