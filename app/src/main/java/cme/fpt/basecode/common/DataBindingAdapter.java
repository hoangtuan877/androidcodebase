package cme.fpt.basecode.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;

public abstract class DataBindingAdapter extends RecyclerView.Adapter<DataBindingAdapter.DataViewHolder> {

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewId) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewId, parent, false);
        return new DataViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        Object data = getObjectForPosition(position);
        Object presenter = getPresenter();
        holder.bind(data, presenter);
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public DataViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Object data, Object presenter) {
            // You MUST declare your variable name is "itemModel" to use this Adapter
            binding.setVariable(BR.itemModel, data);
            binding.setVariable(BR.presenter, presenter);
            binding.executePendingBindings();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    protected abstract Object getObjectForPosition(int position);

    protected abstract Object getPresenter();

    protected abstract int getLayoutIdForPosition(int position);
}
