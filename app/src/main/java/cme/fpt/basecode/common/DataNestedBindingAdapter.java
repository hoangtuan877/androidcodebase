package cme.fpt.basecode.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;

public abstract class DataNestedBindingAdapter extends RecyclerView.Adapter<DataNestedBindingAdapter.DataViewHolder> {
    int childRecyclerId;

    public DataNestedBindingAdapter(int childRecyclerId) {
        this.childRecyclerId = childRecyclerId;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewId) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewId, parent, false);
        return new DataViewHolder(binding, this.childRecyclerId);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        Object data = getObjectForPosition(position);
        Object presenter = getPresenter();
        holder.bind(data, presenter);
        if (holder.getRecyclerView() != null && getFocusedChild() == position) {
                updateChildList(holder.getRecyclerView().getAdapter(), position);
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;
        private RecyclerView recyclerView;

        public DataViewHolder(ViewDataBinding binding, int recyclerId) {
            super(binding.getRoot());
            this.binding = binding;
            this.recyclerView = this.binding.getRoot().findViewById(recyclerId);
            if (recyclerView != null) {
                recyclerView.setAdapter(provideChildAdapter());
            }
        }

        public void bind(Object data, Object presenter) {
            // You MUST declare your variable name is "itemModel" to use this Adapter
            binding.setVariable(BR.itemModel, data);
            binding.setVariable(BR.presenter, presenter);
            binding.executePendingBindings();
        }

        public RecyclerView getRecyclerView() {
            return recyclerView;
        }

        public void setRecyclerView(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    protected abstract Object getObjectForPosition(int position);

    protected abstract Object getPresenter();

    protected abstract int getFocusedChild();

    protected abstract int getLayoutIdForPosition(int position);

    protected abstract void updateChildList(RecyclerView.Adapter adapter, int position);

    protected abstract DataBindingAdapter provideChildAdapter();
}
