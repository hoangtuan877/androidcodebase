package cme.fpt.basecode.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

public class CustomProgressBar extends ProgressBar{

    public CustomProgressBar(Context context) {
        super(context);
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void showHideLoadingState(boolean isLoading) {
        this.setProgress(0);
        if(isLoading) {
            this.setVisibility(View.VISIBLE);

            final long period = 20;
            Timer timer = new Timer();
            ProgressBar that = this;
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    int progress = that.getProgress();
                    if (progress <= 85) {
                        that.setProgress(progress + 1);
                    }
                }
            }, 0, period);
        } else {
            this.setProgress(100);
            this.setEnabled(true);
        }
    }
}
