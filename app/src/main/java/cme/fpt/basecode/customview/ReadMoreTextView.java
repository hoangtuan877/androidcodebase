package cme.fpt.basecode.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cme.fpt.basecode.R;

public class ReadMoreTextView extends TextView {
    private static final int TRIM_MODE_LINES = 0;
    private static final int TRIM_MODE_LENGTH = 1;
    private static final int DEFAULT_TRIM_LENGTH = 240;
    private static final int DEFAULT_TRIM_LINES = 2;
    private static final int INVALID_END_INDEX = -1;
    private static final boolean DEFAULT_SHOW_TRIM_EXPANDED_TEXT = true;
    private static final String ELLIPSIZE = "... ";

    private CharSequence text;
    private BufferType bufferType;


    private boolean readMore = false;
    private int trimLength;
    private CharSequence trimCollapsedText;
    private CharSequence trimExpandedText;
    private ReadMoreClickableSpan viewMoreSpan;
    private int colorClickableText;
    private boolean showTrimExpandedText;

    private int trimMode;
    private int lineEndIndex;
    private int trimLines;
    private boolean expandByClick = false;
    
    public ReadMoreTextView(Context context) {
        this(context, null);
    }

    public ReadMoreTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ReadMoreTextView);
        this.trimLength = typedArray.getInt(R.styleable.ReadMoreTextView_trimLength, DEFAULT_TRIM_LENGTH);
        int resourceIdTrimCollapsedText =
                typedArray.getResourceId(R.styleable.ReadMoreTextView_trimCollapsedText, R.string.read_more);
        int resourceIdTrimExpandedText =
                typedArray.getResourceId(R.styleable.ReadMoreTextView_trimExpandedText, R.string.read_less);
        this.trimCollapsedText = getResources().getString(resourceIdTrimCollapsedText);
        this.trimExpandedText = getResources().getString(resourceIdTrimExpandedText);
        this.trimLines = typedArray.getInt(R.styleable.ReadMoreTextView_trimLines, DEFAULT_TRIM_LINES);
        this.colorClickableText = typedArray.getColor(R.styleable.ReadMoreTextView_colorClickableText,
                ContextCompat.getColor(context, R.color.colorAccent));
        this.showTrimExpandedText =
                typedArray.getBoolean(R.styleable.ReadMoreTextView_showTrimExpandedText, DEFAULT_SHOW_TRIM_EXPANDED_TEXT);
        this.trimMode = typedArray.getInt(R.styleable.ReadMoreTextView_trimMode, TRIM_MODE_LINES);
        typedArray.recycle();
        viewMoreSpan = new ReadMoreClickableSpan();
        if (trimMode == TRIM_MODE_LENGTH) {
            setTextHandler();
        }
    }

    public void setTextHandler() {
        super.setText(getDisplayableText(), bufferType);
        setMovementMethod(LinkMovementMethod.getInstance());
        setHighlightColor(Color.TRANSPARENT);
    }

    private CharSequence getDisplayableText() {
        return getTrimmedText(text);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        this.text = text;
        bufferType = type;
        setTextHandler();
    }

    private CharSequence getTrimmedText(CharSequence text) {
        if (trimMode == TRIM_MODE_LENGTH && text != null && text.length() > trimLength) {
            if (readMore) {
                return updateCollapsedText();
            } else {
                return updateExpandedText();
            }
        }
        if (trimMode == TRIM_MODE_LINES && text != null && lineEndIndex > 0) {
            if (getLayout() != null && getLayout().getLineCount() > trimLines && !expandByClick) {
                return updateCollapsedText();
            } else {
                return updateExpandedText();
            }
        }
        return text;
    }

    private CharSequence updateCollapsedText() {
        int trimEndIndex = text.length();
        readMore = true;
        switch (trimMode) {
            case TRIM_MODE_LINES:
                trimEndIndex = getTrimEndIndexModeLines(trimEndIndex);
                break;
            case TRIM_MODE_LENGTH:
                trimEndIndex = trimLength + 1;
                break;
            default:
                break;
        }

        if (trimEndIndex > text.length()) {
            trimEndIndex = text.length();
        }

        SpannableStringBuilder s = new SpannableStringBuilder(text, 0, trimEndIndex)
                .append(ELLIPSIZE)
                .append(trimCollapsedText);
        return addClickableSpan(s, trimCollapsedText);
    }

    private int getTrimEndIndexModeLines(int trimEndIndex) {
        if (getLayout()!= null && trimEndIndex > getLayout().getLineEnd(trimLines - 1) -1){
            int endLine = getLayout().getLineEnd(trimLines - 1) - 1;
            if (text.charAt(endLine) == '\n' && text.charAt(endLine - 1) == '\n') {
                trimEndIndex = ((String) text).substring(0, endLine).length();
            } else {
                trimEndIndex = ((String) text).substring(0, endLine).trim().length();
            }
            //Full line case
            int ellipsizeAdding = ELLIPSIZE.length() + trimCollapsedText.length();
            if (trimEndIndex - ellipsizeAdding > getLayout().getLineEnd(trimLines - 2)
                    && trimEndIndex + ellipsizeAdding > endLine + 1) {
                trimEndIndex = trimEndIndex - ellipsizeAdding;
            }
        } else {
            trimEndIndex = lineEndIndex - (ELLIPSIZE.length() + trimCollapsedText.length() + 1);
            if (trimEndIndex < 0) {
                trimEndIndex = trimLength + 1;
            }
        }
        return trimEndIndex;
    }

    private CharSequence updateExpandedText() {
        readMore = false;
        if (showTrimExpandedText) {
            SpannableStringBuilder s = new SpannableStringBuilder(text, 0, text.length()).append(trimExpandedText);
            return addClickableSpan(s, trimExpandedText);
        }
        return text;
    }

    private CharSequence addClickableSpan(SpannableStringBuilder s, CharSequence trimText) {
        if (!TextUtils.isEmpty(trimText)) {
            s.setSpan(viewMoreSpan, s.length() - trimText.length(), s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return s;
    }

    public void setTrimLength(int trimLength) {
        this.trimLength = trimLength;
    }

    public void setColorClickableText(int colorClickableText) {
        this.colorClickableText = colorClickableText;
    }

    public void setTrimCollapsedText(CharSequence trimCollapsedText) {
        this.trimCollapsedText = trimCollapsedText;
    }

    public void setTrimExpandedText(CharSequence trimExpandedText) {
        this.trimExpandedText = trimExpandedText;
    }

    public void setTrimMode(int trimMode) {
        this.trimMode = trimMode;
    }

    public void setTrimLines(int trimLines) {
        this.trimLines = trimLines;
    }

    private class ReadMoreClickableSpan extends ClickableSpan {
        @Override
        public void onClick(View widget) {
            // Implement unused interface method
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(colorClickableText);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (readMore && getLineCount()<= trimLines) return; // prevent to duplicate call on Measure
        refreshLineEndIndex();
        setTextHandler();
        if (readMore) {
            int height = getLineHeight() * trimLines;
            setMeasuredDimension(widthMeasureSpec, getLineHeight() / 4 + height);
        } else {
            getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
    }

    private void refreshLineEndIndex() {
        try {
            if (trimLines == 0) {
                lineEndIndex = getLayout().getLineEnd(0);
            } else if (trimLines > 0 && getLineCount() >= trimLines) {
                lineEndIndex = getLayout().getLineEnd(trimLines - 1);
            } else {
                lineEndIndex = INVALID_END_INDEX;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isReadMore() {
        return readMore;
    }

    public void setReadMore(boolean readMore) {
        this.readMore = readMore;
    }

    public boolean isExpandByClick() {
        return expandByClick;
    }

    public void setExpandByClick(boolean expandByClick) {
        this.expandByClick = expandByClick;
    }

    public void updateState(boolean readMore, boolean textExpandedByClick) {
        this.setReadMore(readMore);
        this.setExpandByClick(textExpandedByClick);
        this.refreshLineEndIndex();
        this.setTextHandler();
    }
}
