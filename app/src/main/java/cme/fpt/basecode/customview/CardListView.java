package cme.fpt.basecode.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cme.fpt.basecode.R;

public class CardListView extends CardView {
    ImageView titleIcon;
    TextView title;
    TextView footerLabel;
    RecyclerView recycler;
    LinearLayout footer;
    TextView emptyLabel;
    int bodyHeight;
    int minItems;
    boolean expandState = false;
    String collapse;
    String footerLabelAttr;

    public CardListView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        addView(inflater.inflate(R.layout.custom_card_list_relative, null));
        titleIcon = findViewById(R.id.card_icon);
        title = findViewById(R.id.card_title);
        footer = findViewById(R.id.card_footer);
        emptyLabel = findViewById(R.id.empty_label);
        footerLabel = findViewById(R.id.card_link);
        recycler = findViewById(R.id.card_recycler);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CardListView, 0, 0);

        try {
            title.setText(typedArray.getString(R.styleable.CardListView_cardTitle));
            titleIcon.setImageDrawable(typedArray.getDrawable(R.styleable.CardListView_cardTitleIcon));

            footerLabelAttr = typedArray.getString(R.styleable.CardListView_cardFooterLabel);
            footerLabelAttr = !TextUtils.isEmpty(footerLabelAttr) ? footerLabelAttr : getResources().getString(R.string.card_list_footer);
            footerLabel.setText(footerLabelAttr);

            boolean scrollable = typedArray.getBoolean(R.styleable.CardListView_cardBodyScrollable, true);
            recycler.setNestedScrollingEnabled(scrollable);

            bodyHeight = typedArray.getDimensionPixelSize(R.styleable.CardListView_cardBodyHeight, 0);
            if (bodyHeight != 0) {
                recycler.getLayoutParams().height = bodyHeight;
            }
            collapse = typedArray.getString(R.styleable.CardListView_cardFooterLabelCollapse);

            minItems = typedArray.getInt(R.styleable.CardListView_cardMinItems, 0);
        } finally {
            typedArray.recycle();
        }

    }

    public void expandBody() {
        if (!expandState) {
            this.recycler.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            this.recycler.setMinimumHeight(bodyHeight);
            footerLabel.setText(collapse);
        } else {
            if (bodyHeight != 0) {
                recycler.getLayoutParams().height = bodyHeight;
                footerLabel.setText(footerLabelAttr);
            }
        }
        this.recycler.requestLayout();
        expandState = !expandState;
    }

    public void setFooterVisibility(int numberOfData) {
        if (numberOfData <= minItems) {
            footer.setVisibility(GONE);
            if (numberOfData < minItems){
                this.recycler.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            }
        } else {
            footer.setVisibility(VISIBLE);

        }
    }

    public RecyclerView getRecycler() {
        return recycler;
    }

    public ImageView getTitleIcon() {
        return titleIcon;
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getFooterLabel() {
        return footerLabel;
    }

    public int getMinItems() {
        return minItems;
    }

    public void setMinItems(int minItems) {
        this.minItems = minItems;
    }

    public LinearLayout getFooter() {
        return footer;
    }

    public void setFooter(LinearLayout footer) {
        this.footer = footer;
    }

    public TextView getEmptyLabel() {
        return emptyLabel;
    }

    public void setEmptyLabel(TextView emptyLabel) {
        this.emptyLabel = emptyLabel;
    }
}
