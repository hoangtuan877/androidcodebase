package cme.fpt.basecode.screens.discussion.adapter;

import android.databinding.BindingAdapter;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.customview.ReadMoreTextView;
import cme.fpt.basecode.model.Comment;
import cme.fpt.basecode.model.Reply;
import cme.fpt.basecode.model.Topic;

public class DiscussionTopicAdapter extends DataBindingAdapter {
    Topic topic;
    List<TopicAdapterItem> listItem = new ArrayList<>();
    private final int layoutTopic;
    private final int layoutAnswer;
    private final int layoutReply;

    AnswerAdapterPresenter presenter;

    public DiscussionTopicAdapter(int layoutTopic, int layoutAnswer, int layoutReply) {
        this.layoutTopic = layoutTopic;
        this.layoutAnswer = layoutAnswer;
        this.layoutReply = layoutReply;
    }

    private void updateTopic(Topic topic) {
        this.topic = topic;
        this.listItem.clear();
        this.listItem = new ArrayList<>();
        this.listItem.addAll(convertDataToListItem(topic.getComments()));
        this.topic.getComments().clear();
    }

    private List convertDataToListItem(List data) {
        List<TopicAdapterItem> items = new ArrayList<>();
        for (Object obj : data) {
            items.add(new TopicAdapterItem(obj, false, false));
        }
        return items;
    }

    @Override
    protected Object getObjectForPosition(int position) {
        if (position == 0) {
            return this.topic;
        }
        return this.listItem.get(position - 1); // Minus one for topic
    }

    @Override
    protected Object getPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        if (position == 0) {
            return layoutTopic;
        } else if (listItem.get(position - 1).getData() instanceof Comment) { // Minus one for topic
            return layoutAnswer;
        } else {
            return layoutReply;
        }
    }

    @Override
    public int getItemCount() {
        return this.listItem.size() + 1; // plus one for topic
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        updateTopic(topic);
        this.notifyDataSetChanged();
    }

    public void addMoreItemList(List<Comment> moreList) {
        List<TopicAdapterItem> addMoreItem = convertDataToListItem(moreList);
        int currentSize = listItem.size();
        listItem.addAll(addMoreItem);
        notifyFilter(1,currentSize + 1, addMoreItem.size()); // new item selected equal clicked index + 1
    }

    public void setPresenter(AnswerAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    public void expandAnswer(List<Reply> listReply) {
        if (listReply.isEmpty()) {
            return;
        }
        int commentId = listReply.get(0).getParentId();
        for (TopicAdapterItem topicAdapterItem : listItem) {
            if (topicAdapterItem.getData() instanceof Comment && ((Comment) topicAdapterItem.getData()).getId().intValue() == commentId) {
                int itemIndex = listItem.indexOf(topicAdapterItem);
                Comment answer = (Comment) listItem.get(itemIndex).getData();
                answer.setReplies(listReply);
                int repliesSize = listReply.size();
                int indexInListItem = itemIndex + 1; // plus one for topic
                if (listItem.get(itemIndex).isExpandedChildren()) {
                    listItem.subList(indexInListItem, indexInListItem + repliesSize).clear();
                    listItem.get(itemIndex).setExpandedChildren(false);
                    notifyFilter(5,indexInListItem + 1, repliesSize);
                } else {
                    listItem.addAll(indexInListItem, convertDataToListItem(listReply));
                    listItem.get(itemIndex).setExpandedChildren(true);
                    notifyFilter(1,indexInListItem + 1, repliesSize); // new item selected equal clicked index + 1
                    this.presenter.scrollToAnswerPosition(indexInListItem + 1);
                }
                break;
            }
        }
    }

    public void notifyFilter(int type, int index, int range) {
        if (listItem.size() < Constant.SIZE_DISCUSSION_COMMENT) {
            this.notifyDataSetChanged();
        } else {
            switch (type) {
                case 0:
                    this.notifyItemInserted(index);
                    break;
                case 1:
                    this.notifyItemRangeInserted(index, range);
                    break;
                case 2:
                    this.notifyItemInserted(index);
                    break;
                case 3:
                    this.notifyItemChanged(index);
                    break;
                case 4:
                    this.notifyItemRemoved(index); // Plus 1 for topic info
                    break;
                case 5:
                    this.notifyItemRangeRemoved(index, range);
                    break;
                default:
                    notifyDataSetChanged();
                    break;
            }
        }
    }

    public void addComment(Comment comment) {
        listItem.add(0, new TopicAdapterItem(comment, false, false));
        notifyFilter(0, 1, 0 ); // Index + 1 for topic info
    }

    public void updateComment(Comment comment) {
        for (TopicAdapterItem topicAdapterItem : listItem) {
            if (topicAdapterItem.getData() instanceof Comment && ((Comment) topicAdapterItem.getData()).getId().intValue() == comment.getId().intValue()) {
                topicAdapterItem.setData(comment);
                notifyFilter(3, listItem.indexOf(topicAdapterItem) + 1, 0 ); // Index + 1 for topic info
                break;
            }
        }
    }

    public void deleteComment(int commentId) {
        for (TopicAdapterItem topicAdapterItem : listItem) {
            if (topicAdapterItem.getData() instanceof Comment && ((Comment) topicAdapterItem.getData()).getId().intValue() == commentId) {
                int index = listItem.indexOf(topicAdapterItem);
                if(topicAdapterItem.isExpandedChildren()){
                    int repliesRange = ((Comment) topicAdapterItem.getData()).getReplies().size();
                    listItem.subList(index, index + repliesRange).clear();
                    listItem.remove(index);
                    notifyFilter(5, index + 1, repliesRange ); // Index + 1 for topic info
                }else {
                    listItem.remove(index);
                    notifyFilter(4, index + 1, 0 ); // Index + 1 for topic info
                }

                break;
            }
        }
    }

    public void updateReply(Reply reply) {
        for (TopicAdapterItem topicAdapterItem : listItem) {
            if (topicAdapterItem.getData() instanceof Reply && ((Reply) topicAdapterItem.getData()).getId().intValue() == reply.getId().intValue()) {
                topicAdapterItem.setData(reply);
                notifyFilter(3, listItem.indexOf(topicAdapterItem) + 1, 0);
                break;
            }
        }
    }

    public void deleteReply(int replyId) {
        for (TopicAdapterItem topicAdapterItem : listItem) {
            if (topicAdapterItem.getData() instanceof Reply && ((Reply) topicAdapterItem.getData()).getId() == replyId) {
                int index = listItem.indexOf(topicAdapterItem);
                Reply reply = ((Reply) topicAdapterItem.getData());
                int parentIndex = reply.getParentId();
                for (TopicAdapterItem itemParent : listItem) {
                    if (itemParent.getData() instanceof Comment && ((Comment) itemParent.getData()).getId() == parentIndex) {
                        Comment parentComment = ((Comment) itemParent.getData());
                        parentComment.getReplies().remove(reply);
                        parentComment.setCountReply(parentComment.getReplies().size());
                        notifyFilter(3, listItem.indexOf(itemParent) + 1, 0 ); // Index + 1 for topic info
                        break;
                    }
                }
                listItem.remove(index);
                notifyFilter(4, index + 1, 0);
                break;
            }
        }
    }


    public void addReply(TopicAdapterItem topicAdapterItem, Reply reply) {
        int itemIndex = listItem.indexOf(topicAdapterItem);
        if (topicAdapterItem.getData() instanceof Comment && itemIndex >= 0) {
            Comment answer = (Comment) listItem.get(itemIndex).getData();
            int indexInListItem = itemIndex + 1;
            List listReplies = answer.getReplies() != null ? answer.getReplies() : new ArrayList();
            listReplies.add(reply);
            if (!topicAdapterItem.isExpandedChildren()){
                int repliesSize = listReplies.size();
                listItem.addAll(indexInListItem, convertDataToListItem(listReplies));
                listItem.get(itemIndex).setExpandedChildren(true);

                notifyFilter(1,indexInListItem + 1,repliesSize);
                this.presenter.scrollToAnswerPosition(indexInListItem);
            } else {
                for (int i = indexInListItem; i < listItem.size(); i++) {
                    if (listItem.get(i).getData() instanceof Comment) {
                        indexInListItem = i;
                        break;
                    }
                }
                listItem.add(indexInListItem, new TopicAdapterItem(reply, false, false));
                notifyFilter(2, indexInListItem + 1, 0);
                this.presenter.scrollToAnswerPosition(indexInListItem + 1);
            }
            answer.setReplies(listReplies);
            answer.setCountReply(listReplies.size());
            notifyFilter(3, itemIndex + 1, 0);
        }
    }

    public void showReadMoreAnswer(ReadMoreTextView readMoreTextView, TopicAdapterItem item) {
        int index = listItem.indexOf(item);
        listItem.get(index).setShowMore(false);
        listItem.get(index).setExpandedByClick(true);
        readMoreTextView.updateState(false, true);
    }

    public interface AnswerAdapterPresenter {
        void onAnswerClick(TopicAdapterItem item);

        void onAnswerIconClick(View view, TopicAdapterItem answer);

        void onHandleTopicIconClick(View view);

        void scrollToAnswerPosition(int position);

        void onAnswerReadMoreClick(View v, TopicAdapterItem item);

        boolean onAnswerLongClick(View view, TopicAdapterItem answer);
    }

    public List<TopicAdapterItem> getListItem() {
        return listItem;
    }

    @BindingAdapter({"textExpanded", "textExpandedByClick"})
    public static void setReadMoreTextState(ReadMoreTextView readMoreTextView, boolean textExpanded, boolean textExpandedByClick) {
        readMoreTextView.updateState(textExpanded, textExpandedByClick);
    }

}
