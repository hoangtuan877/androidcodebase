package cme.fpt.basecode.screens.login.repository;

import java.util.List;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.Authority;
import cme.fpt.basecode.model.Token;
import cme.fpt.basecode.model.UserInfo;

public class LoginResponse extends BaseResponse{
    UserInfo userInfo;
    String apiTimeOut;
    List<Authority> authorities;
    Token accessToken;

    public LoginResponse(UserInfo userInfo, String apiTimeOut, List<Authority> authorities, Token accessToken) {
        this.userInfo = userInfo;
        this.apiTimeOut = apiTimeOut;
        this.authorities = authorities;
        this.accessToken = accessToken;
    }

    public LoginResponse(int code, String errorMessage) {
        super(code, errorMessage);
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getApiTimeOut() {
        return apiTimeOut;
    }

    public void setApiTimeOut(String apiTimeOut) {
        this.apiTimeOut = apiTimeOut;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public Token getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(Token accessToken) {
        this.accessToken = accessToken;
    }
}
