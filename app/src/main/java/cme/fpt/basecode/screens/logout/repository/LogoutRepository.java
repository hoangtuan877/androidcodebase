package cme.fpt.basecode.screens.logout.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.io.IOException;

import javax.inject.Singleton;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.screens.logout.webapi.LogoutWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class LogoutRepository {
    private LogoutWebService logoutWebService;
    private static String TAG = "LogoutRepository";

    public LogoutRepository(LogoutWebService logoutWebService) {
        this.logoutWebService = logoutWebService;
    }

    public LiveData<LogoutResponse> logout() {
        MutableLiveData<LogoutResponse> data = new MutableLiveData<>();
        logoutWebService.logout().enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                Log.e(TAG, response.code() + "");

                    try {
                        if (response.code() != BaseResponse.SUCCESS_CODE) {
                            data.setValue(new LogoutResponse(response.code(), response.errorBody().string().toString()));
                        }else {
                            LogoutResponse result = response.body();
                            result.setResponseCode(response.code());
                            data.setValue(result);
                        }
                    } catch (IOException e) {
                        LogoutResponse logoutResponse = new LogoutResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR);
                        data.setValue(logoutResponse);
                    }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                LogoutResponse logoutResponse = new LogoutResponse(BaseResponse.UNKNOWN_CODE, t.getMessage());
                data.setValue(logoutResponse);
            }
        });
        return data;
    }
}
