package cme.fpt.basecode.screens.login;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.LoginActivity;
import cme.fpt.basecode.R;
import cme.fpt.basecode.RegisterActivity;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentLoginBinding;
import cme.fpt.basecode.screens.login.viewmodel.LoginViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    FragmentLoginBinding loginBinding;
    @Inject
    LoginViewModel.LoginFactory loginViewModelLoginFactory;
    LoginViewModel loginViewModel;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        loginViewModel = ViewModelProviders.of(this, loginViewModelLoginFactory).get(LoginViewModel.class);
        loginBinding.setLoginModel(loginViewModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        loginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        loginBinding.loginButton.setOnClickListener(this);
        loginBinding.loginRegister.setOnClickListener(this);
        loginBinding.loginPassword.setOnFocusChangeListener((view, b) -> {
            if(view.hasFocus()){
                loginBinding.loginPasswordWrapper.setError("");
            }
        });

        return loginBinding.getRoot();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.login_button:
                if (validateUsernameAndPassword()) {
                    callLoginAPI();
                } else {
                    loginBinding.loginUsernameWrapper.setError(" ");
                    loginBinding.loginUsernameWrapper.setErrorEnabled(false);
                    loginBinding.loginUsername.getBackground().setColorFilter(getResources().getColor(R.color.colorTextErrorInput), PorterDuff.Mode.SRC_ATOP);
                    loginBinding.loginPasswordWrapper.setError(getString(R.string.empty_username_password));
                }
                break;
            case R.id.login_register:
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivityForResult(intent, LoginActivity.REGISTER_REQUEST);
                break;
            default:
                break;
        }
    }
    private boolean validateUsernameAndPassword() {
        if(loginBinding.loginPassword.getText().toString().trim().length() <= 0
            || loginBinding.loginUsername.getText().toString().trim().length() <= 0) {
            return false;
        } else {
            return true;
        }
    }

    private void callLoginAPI() {
        loginBinding.progressBar.showHideLoadingState(true);
        String username = loginBinding.loginUsername.getText().toString();
        String password = loginBinding.loginPassword.getText().toString();
        loginViewModel.login(username, password);

        loginViewModel.getLoginData().removeObservers(this);
        loginViewModel.getLoginData().observe(this, loginResponse -> {
            int responseCode = loginResponse.getResponseCode();
            if (responseCode == BaseResponse.SUCCESS_CODE) {
                saveTokenToSharedReference(loginResponse.getAccessToken().getToken(), Constant.USER_TOKEN);
                saveTokenToSharedReference(username, Constant.USERNAME);
                loginViewModel.currentUserData();
                getCurrentUser();
                callGetLRSEndPoint();
            } else {
                loginBinding.loginUsername.getBackground().setColorFilter(getResources().getColor(R.color.colorTextErrorInput), PorterDuff.Mode.SRC_ATOP);
                loginBinding.loginPasswordWrapper.setError(convertErrorMessage(loginResponse.getErrorMessage(), responseCode));
            }
        });
    }

    private void callGetLRSEndPoint() {
        loginViewModel.getLRSEndpoint();
        loginViewModel.getLrsEndPointLiveData().removeObservers(this);
        loginViewModel.getLrsEndPointLiveData().observe(this, lrsEndpoint -> {
            if(!lrsEndpoint.equals(Constant.SERVER_ERROR)) {
                saveTokenToSharedReference(lrsEndpoint, Constant.LRS_ENDPOINT);
                callGetLRSBasicAuthVal();
            } else {
                startMainActivity();
            }

        });
    }

    private void callGetLRSBasicAuthVal() {
        loginViewModel.getLRSAuthVal();
        loginViewModel.getLrsBasicAuthValLiveData().removeObservers(this);
        loginViewModel.getLrsBasicAuthValLiveData().observe(this, basicAuthVal -> {
            if(!basicAuthVal.equals(Constant.SERVER_ERROR)) {
                saveTokenToSharedReference(basicAuthVal, Constant.LRS_BASIC_AUTH);
            }
            startMainActivity();
        });
    }

    private void getCurrentUser(){
        loginViewModel.getCurrentUser().observe(this, currentUserResponse -> {
            if(currentUserResponse.getAvatar().getSource().getFileUrl() == null){
                saveTokenToSharedReference(currentUserResponse.getAvatar().getDefaultUrl().getFileUrl(), Constant.AVATAR_URL);
            }else{
                saveTokenToSharedReference(currentUserResponse.getAvatar().getSource().getFileUrl(), Constant.AVATAR_URL);
            }
            saveTokenToSharedReference(currentUserResponse.getDisplayName(),Constant.CURRENT_USER_DISPLAYNAME);
            saveIdToSharedReference(currentUserResponse.getId(), Constant.CURRENT_USER_ID, currentUserResponse.isStaff(), Constant.IS_STAFF);
        });
    }

    private void startMainActivity() {
        loginBinding.progressBar.showHideLoadingState(false);
        loginBinding.progressBar.setVisibility(View.GONE);
        Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_mainActivity);
        getActivity().finish();
    }
    @NonNull
    private String convertErrorMessage(String responseErrorMsg, int responseCode) {
        String errorMsg;
        switch (responseCode) {
            case BaseResponse.UNKNOWN_CODE:
                errorMsg = getString(R.string.error_unknown);
                break;
            case BaseResponse.CODE_400:
                errorMsg = getString(R.string.error_400_locked);
                break;
            case BaseResponse.CODE_401:
                if (responseErrorMsg.contains("user.not.active")) {
                    errorMsg = getString(R.string.error_401_not_active);
                } else {
                    errorMsg = getString(R.string.error_401_wrong);
                }
                break;
            case BaseResponse.CODE_403:
                errorMsg = getString(R.string.error_403_forbidden);
                break;
            case BaseResponse.CODE_500:
                errorMsg = getString(R.string.error_500_fail);
                break;
            default:
                errorMsg = getString(R.string.error_unknown);
                break;
        }
        return errorMsg;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        //  Handle activity result here
        if (LoginActivity.REGISTER_REQUEST == requestCode && Activity.RESULT_OK == resultCode && data != null) {
            Bundle bundle = data.getBundleExtra(LoginActivity.RESULT_BUNDLE);
            String username = bundle.getString(LoginActivity.USERNAME_INTENT);
            String password = bundle.getString(LoginActivity.PASWD_INTENT);
            loginBinding.loginUsername.setText(username);
            loginBinding.loginPassword.setText(password);
        }
    }
    private void saveTokenToSharedReference(String token, String key) {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, token);
        editor.commit();
    }

    private void saveIdToSharedReference(int id, String keyId, boolean staff, String keyStaff){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(keyId, id);
        editor.putBoolean(keyStaff, staff);
        editor.commit();
    }
}
