package cme.fpt.basecode.screens.userprofile.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import cme.fpt.basecode.model.User;
import cme.fpt.basecode.screens.userprofile.repository.UserRepository;

public class UserProfileViewModel extends ViewModel {
    private LiveData<User> user;

    private UserRepository userRepository;

    public UserProfileViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void init(String userId) {
        if (this.user != null) {
            // ViewModel is created by Fragment, and our app is not change userId
            return;
        }
        this.user = userRepository.getUser(userId);
    }

    public LiveData<User> getUser() {
        return user;
    }

    public static class UserProfileFactory implements ViewModelProvider.Factory {

        private UserRepository userRepository;

        public UserProfileFactory(UserRepository userRepository) {
            this.userRepository = userRepository;
        }

        @NonNull
        @Override
        public UserProfileViewModel create(@NonNull Class modelClass) {
            return new UserProfileViewModel(userRepository);
        }
    }
}
