package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.repository.DiscussionTopicsRepository;

public class DiscussionCreateNewViewModel extends ViewModel {

    MutableLiveData<Topic> newTopic = new MutableLiveData<>();
    MutableLiveData<Boolean> isCreateResult = new MutableLiveData<>();
    LiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();
    DiscussionTopicsRepository topicsRepository;

    public DiscussionCreateNewViewModel(DiscussionTopicsRepository topicsRepository) {
        this.topicsRepository = topicsRepository;
        newTopic.postValue(new Topic());
    }

    public void init(Topic topic) {
        isCreateResult = topicsRepository.getCreateOrUpdateResult();
        errorResponse = topicsRepository.getErrorResponse();
        newTopic.postValue(topic);
    }

    public void createTopic(String title, String content) {
        newTopic.getValue().setTitle(title);
        newTopic.getValue().setContent(content);
        this.topicsRepository.createTopic(newTopic.getValue());
    }

    public void updateTopic(String title, String content) {
        newTopic.getValue().setTitle(title);
        newTopic.getValue().setContent(content);
        this.topicsRepository.updateTopic(newTopic.getValue());
    }

    public MutableLiveData<Boolean> getIsCreateResult() {
        return isCreateResult;
    }

    public void setIsCreateResult(MutableLiveData<Boolean> isCreateResult) {
        this.isCreateResult = isCreateResult;
    }

    public MutableLiveData<Topic> getNewTopic() {
        return newTopic;
    }

    public void setNewTopic(MutableLiveData<Topic> newTopic) {
        this.newTopic = newTopic;
    }

    public LiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(LiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public static class DiscussionCreateNewFactory implements ViewModelProvider.Factory {

        private DiscussionTopicsRepository topicsRepository;

        public DiscussionCreateNewFactory(DiscussionTopicsRepository topicsRepository) {
            this.topicsRepository = topicsRepository;
        }

        @NonNull
        @Override
        public DiscussionCreateNewViewModel create(@NonNull Class modelClass) {
            return new DiscussionCreateNewViewModel(topicsRepository);
        }
    }
}
