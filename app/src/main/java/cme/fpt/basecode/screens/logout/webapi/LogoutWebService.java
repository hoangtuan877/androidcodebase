package cme.fpt.basecode.screens.logout.webapi;

import cme.fpt.basecode.screens.logout.repository.LogoutResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface LogoutWebService {
    @GET("users/invalidateSession")
    Call<LogoutResponse> logout();
}
