package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.repository.DiscussionTopicsRepository;

public class DiscussionTopicsViewModel extends ViewModel {
    MutableLiveData<Integer> currentSection = new MutableLiveData<>();

    MutableLiveData<Boolean> isEmptyTopics = new MutableLiveData<>();

    LiveData<List<Topic>> topics = new MutableLiveData<>();
    LiveData<Discussion> discussion = new MutableLiveData<>();
    MutableLiveData<Boolean> isLastTopicLoaded = new MutableLiveData<>();

    DiscussionTopicsRepository topicsRepository;

    LiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();

    public DiscussionTopicsViewModel(DiscussionTopicsRepository topicsRepository) {
        this.topicsRepository = topicsRepository;
        currentSection.postValue(0);
        isEmptyTopics.postValue(false);
    }

    public void init(int discussionId) {
        topics = topicsRepository.getTopics();
        discussion = topicsRepository.getDiscussion();
        isEmptyTopics = topicsRepository.getEmptyTopics();
        errorResponse = topicsRepository.getErrorResponse();
        isLastTopicLoaded = topicsRepository.getIsLastTopicLoaded();
        this.topicsRepository.init(discussionId);
    }

    public MutableLiveData<Integer> getCurrentSection() {
        return currentSection;
    }

    public void setCurrentSection(int newSection) {
        this.currentSection.postValue(newSection);
    }

//    public DiscussionTopicsRepository getTopicsRepository() {
//        return topicsRepository;
//    }
//
//    public void setTopicsRepository(DiscussionTopicsRepository topicsRepository) {
//        this.topicsRepository = topicsRepository;
//    }

    public LiveData<List<Topic>> getTopics() {
        return topics;
    }

    public void setTopics(LiveData<List<Topic>> topics) {
        this.topics = topics;
    }

    public LiveData<Discussion> getDiscussion() {
        return discussion;
    }

    public void setDiscussion(LiveData<Discussion> discussion) {
        this.discussion = discussion;
    }

    public MutableLiveData<Boolean> getIsEmptyTopis() {
        return isEmptyTopics;
    }

    public LiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(LiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public MutableLiveData<Boolean> getIsLastTopicLoaded() {
        return isLastTopicLoaded;
    }

    public void getListTopicOnAPI(long discussionId, int pageNum, int pageSize) {
        this.topicsRepository.getListTopicOnAPI(discussionId, pageNum, pageSize);
    }

    public static class DiscussionTopicsFactory implements ViewModelProvider.Factory {

        private DiscussionTopicsRepository topicsRepository;

        public DiscussionTopicsFactory(DiscussionTopicsRepository topicsRepository) {
            this.topicsRepository = topicsRepository;
        }

        @NonNull
        @Override
        public DiscussionTopicsViewModel create(@NonNull Class modelClass) {
            return new DiscussionTopicsViewModel(topicsRepository);
        }
    }

}
