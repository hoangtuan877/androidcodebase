package cme.fpt.basecode.screens.dashboard;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.R;
import cme.fpt.basecode.common.BaseSharedViewModel;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentDashboardBinding;
import cme.fpt.basecode.model.Content;
import cme.fpt.basecode.model.Notification;
import cme.fpt.basecode.screens.dashboard.adapter.DashboardContinueAdapter;
import cme.fpt.basecode.screens.dashboard.adapter.DashboardDiscussionAdapter;
import cme.fpt.basecode.screens.dashboard.adapter.DashboardNotificationAdapter;
import cme.fpt.basecode.screens.dashboard.viewmodel.DashboardViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements DashboardNotificationAdapter.NotificationAdapterPresenter, DashboardContinueAdapter.ContinueAdapterPresenter,
        DashboardDiscussionAdapter.DiscussionAdapterPresenter {
    FragmentDashboardBinding dashboardBinding;
    BaseSharedViewModel mErrorViewModel;
    DashboardViewModel viewModel;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Inject
    public DashboardViewModel.DashboardFactory dashboardFactory;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, dashboardFactory).get(DashboardViewModel.class);
        viewModel.init();
        mErrorViewModel = ViewModelProviders.of(getActivity()).get(BaseSharedViewModel.class);

        dashboardBinding.setDashboardViewModel(viewModel);

        // Init Adapter
        dashboardBinding.dashboardContinueCard.getRecycler().setLayoutManager(new LinearLayoutManager(getActivity()));
        dashboardBinding.dashboardNotificationCard.getRecycler().setLayoutManager(new LinearLayoutManager(getActivity()));
        dashboardBinding.dashboardDiscussCard.getRecycler().setLayoutManager(new LinearLayoutManager(getActivity()));

        // Create empty list when waiting view model update data
        DashboardContinueAdapter dashboardContinueAdapter = new DashboardContinueAdapter(R.layout.dashboard_continue_item, new ArrayList<>());
        dashboardBinding.dashboardContinueCard.getRecycler().setAdapter(dashboardContinueAdapter);
        dashboardContinueAdapter.setPresenter(this);
        dashboardBinding.dashboardContinueCard.getFooter().setOnClickListener(v -> Toast.makeText(getActivity(), "OPEN COURSE FILTER SCREEN", Toast.LENGTH_LONG).show());
        // Create empty list when waiting view model update data
        DashboardNotificationAdapter dashboardNotificationAdapter = new DashboardNotificationAdapter(R.layout.dashboard_notification_item, new ArrayList<>());
        dashboardBinding.dashboardNotificationCard.getRecycler().setAdapter(dashboardNotificationAdapter);
        dashboardNotificationAdapter.setPresenter(this);
        dashboardBinding.dashboardContinueCard.getFooter().setOnClickListener(v -> Toast.makeText(getActivity(), "OPEN NOTIFICATION SCREEN", Toast.LENGTH_LONG).show());

        // Create empty list when waiting view model update data
        DashboardDiscussionAdapter dashboardDiscussionAdapter = new DashboardDiscussionAdapter(R.layout.dashboard_dicussion_item, new ArrayList<>());
        dashboardBinding.dashboardDiscussCard.getRecycler().setAdapter(dashboardDiscussionAdapter);
        dashboardDiscussionAdapter.setPresenter(this);
        dashboardBinding.dashboardDiscussCard.getFooter().setOnClickListener(view -> Navigation.findNavController(dashboardBinding.dashboardDiscussCard.getFooter()).navigate(R.id.action_dashboardFragment_to_discussionMainFragment));
        viewModel.getStudyClasses().observe(this, studyClasses -> {
            if (studyClasses == null || studyClasses.isEmpty()) {
                dashboardBinding.dashboardContinueCard.getRecycler().setVisibility(View.GONE);
                dashboardBinding.dashboardContinueCard.getEmptyLabel().setVisibility(View.VISIBLE);
                dashboardBinding.dashboardContinueCard.getEmptyLabel().setText(R.string.dashboard_empty_courses);
            } else {
                dashboardBinding.dashboardContinueCard.setFooterVisibility(studyClasses.size());
                dashboardBinding.dashboardContinueCard.getRecycler().setVisibility(View.VISIBLE);
                dashboardBinding.dashboardContinueCard.getEmptyLabel().setVisibility(View.GONE);
                dashboardContinueAdapter.setStudyClasses(studyClasses);
            }
        });

        viewModel.getNotifications().observe(this, notifications -> {
            if (notifications == null || notifications.isEmpty()) {
                dashboardBinding.dashboardNotificationCard.getRecycler().setVisibility(View.GONE);
                dashboardBinding.dashboardNotificationCard.getEmptyLabel().setVisibility(View.VISIBLE);
                dashboardBinding.dashboardNotificationCard.getEmptyLabel().setText(R.string.dashboard_empty_notification);
            } else {
                dashboardBinding.dashboardNotificationCard.setFooterVisibility(notifications.size());
                dashboardBinding.dashboardNotificationCard.getRecycler().setVisibility(View.VISIBLE);
                dashboardBinding.dashboardNotificationCard.getEmptyLabel().setVisibility(View.GONE);
                dashboardNotificationAdapter.setNotifications(notifications);
            }
        });

        viewModel.getListContent().observe(this, discussions -> {
            if (discussions == null || discussions.isEmpty()) {
                dashboardBinding.dashboardDiscussCard.getRecycler().setVisibility(View.GONE);
                dashboardBinding.dashboardDiscussCard.getEmptyLabel().setVisibility(View.VISIBLE);
                dashboardBinding.dashboardDiscussCard.getEmptyLabel().setText(R.string.dashboard_empty_discussion);
            } else {
                dashboardBinding.dashboardDiscussCard.setFooterVisibility(discussions.size());
                dashboardBinding.dashboardDiscussCard.getRecycler().setVisibility(View.VISIBLE);
                dashboardBinding.dashboardDiscussCard.getEmptyLabel().setVisibility(View.GONE);
                dashboardDiscussionAdapter.setDiscussions(discussions);
            }
        });

        dashboardBinding.btnGoToCourse.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(dashboardBinding.edtCourseId.getText())) {
                int courseId = Integer.parseInt(dashboardBinding.edtCourseId.getText().toString());
                onCourseClick(v, courseId);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        });

        viewModel.getErrorResponse().observe(this, mErrorViewModel.getErrorResponse()::postValue);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        dashboardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        dashboardBinding.setLifecycleOwner(this);

        getActivity().setTitle(R.string.dashboard_title);

        return dashboardBinding.getRoot();
    }

    @Override
    public void onNotificationClick(View view, Notification notification) {
        if (!notification.isSeen()) {
            int[] ids = new int[]{notification.getId()};
            viewModel.seenNotification(ids);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("courseId", notification.getContentId());
        Navigation.findNavController(view).navigate(R.id.action_dashboardFragment_to_courseDetailFragment, bundle);
    }

    @Override
    public void onCourseClick(View view, int courseId) {
        Bundle bundle = new Bundle();
        bundle.putInt("courseId", courseId);
        Navigation.findNavController(view).navigate(R.id.action_dashboardFragment_to_courseDetailFragment, bundle);
    }

    @Override
    public void onDiscussionClick(View view, Content topic) {
        saveIdCurrentTopic(topic.getOwner().getUserId(), "currentTopicOwner");
        Bundle bundle = new Bundle();
        bundle.putLong("topicId", topic.getId());
        Navigation.findNavController(view).navigate(R.id.action_dashboardFragment_to_topicAnswerFragment, bundle);
    }

    private void saveIdCurrentTopic(int id, String keyId){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(keyId, id);
        editor.commit();
    }
}
