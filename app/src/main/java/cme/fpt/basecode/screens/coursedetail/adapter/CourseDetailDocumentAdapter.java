package cme.fpt.basecode.screens.coursedetail.adapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.Document;

public class CourseDetailDocumentAdapter extends DataBindingAdapter {
    private int layoutId;
    private List<Document> mListDocuments;
    private DocumentAdapterPresenter presenter;


    public CourseDetailDocumentAdapter(int layoutId, List<Document> documentList) {
        this.layoutId = layoutId;
        this.mListDocuments = documentList;
    }

    @Override
    protected Object getObjectForPosition(int position) {
        return mListDocuments.get(position);
    }

    @Override
    protected Object getPresenter() {
        return this.presenter;
    }

    public void setPresenter(DocumentAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return this.layoutId;
    }

    @Override
    public int getItemCount() {
        return mListDocuments.size();
    }

    public void setData(List<Document> data) {
        this.mListDocuments = data;
        notifyDataSetChanged();
    }

    @BindingAdapter("documentSize")
    public static void setLessonStatus(TextView view, int fileSize) {
        if(fileSize < 1024) {
            view.setText(fileSize + "Kb");
        } else {
            view.setText(fileSize/1024 + "Mb");
        }
    }

    public interface DocumentAdapterPresenter {
        void onDownloadDocument(View view, Document document);
    }
}
