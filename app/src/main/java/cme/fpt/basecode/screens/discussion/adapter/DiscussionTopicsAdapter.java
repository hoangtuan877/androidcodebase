package cme.fpt.basecode.screens.discussion.adapter;

import android.view.View;

import java.util.List;

import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.Topic;

public class DiscussionTopicsAdapter extends DataBindingAdapter {

    private int layoutId;
    private List<Topic> topics;
    private DiscussionTopicsAdapter.DiscussionTopicsAdapterPresenter presenter;

    public DiscussionTopicsAdapter(int layoutId, List<Topic> topics) {
        this.layoutId = layoutId;
        this.topics = topics;
    }

    @Override
    protected Object getObjectForPosition(int position) {
        if (this.topics == null){
            return null;
        }
        return topics.get(position);
    }

    @Override
    public DiscussionTopicsAdapterPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(DiscussionTopicsAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return this.layoutId;
    }

    @Override
    public int getItemCount() {
        if (this.topics == null){
            return 0;
        }
        return this.topics.size();
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        if (this.topics != null) {
            this.topics.clear();
            this.topics.addAll(topics);
        } else {
            this.topics = topics;
        }
        notifyDataSetChanged();
    }

    public interface DiscussionTopicsAdapterPresenter {
        void onTopicClick(View view, Topic topic);
    }
}
