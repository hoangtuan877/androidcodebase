package cme.fpt.basecode.screens.coursedetail.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.model.Course;
import cme.fpt.basecode.model.LessonResult;
import cme.fpt.basecode.model.RatingCourse;
import cme.fpt.basecode.model.Registration;
import cme.fpt.basecode.model.RelatedCourseDTO;
import cme.fpt.basecode.screens.coursedetail.repository.CourseDetailRepository;
import retrofit2.Callback;

public class CourseDetailViewModel extends ViewModel {
    CourseDetailRepository courseDetailRepository;

    LiveData<Course> course = new MutableLiveData<>();
    LiveData<List<LessonResult>> lessonResult = new MutableLiveData<>();

    LiveData<String> categoryName = new MutableLiveData<>();
    LiveData<RatingCourse> ratingCourse = new MutableLiveData<>();
    MutableLiveData<String> traineeStatus = new MutableLiveData<>();
    LiveData<Registration> registration = new MutableLiveData<>();
    LiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();
    LiveData<List<RelatedCourseDTO>> relatedCourses = new MutableLiveData<>();

    public CourseDetailViewModel(CourseDetailRepository courseDetailRepository) {
        this.courseDetailRepository = courseDetailRepository;
    }

    public void init(String courseId) {
        // Set data to livedata of repository
        course = this.courseDetailRepository.getCourseLiveData();
        lessonResult = this.courseDetailRepository.getLessonResultLiveData();
        ratingCourse = this.courseDetailRepository.getRatingCourseLiveData();
        traineeStatus = this.courseDetailRepository.getTraineeStatus();
        registration = this.courseDetailRepository.getRegistrationLiveData();
        errorResponse = this.courseDetailRepository.getErrorResponseLiveData();
        relatedCourses = this.courseDetailRepository.getRelatedCoursesLiveData();
        this.courseDetailRepository.init(courseId);
    }

    public void submitRating(String point, String courseId) {
        this.courseDetailRepository.submitRating(point, courseId);
    }

    public void enrollCourse(int courseId, Callback callback) {
        this.courseDetailRepository.enrollCourse(courseId, callback);
    }

    public LiveData<Course> getCourse() {
        return course;
    }

    public void setCourse(MutableLiveData<Course> course) {
        this.course = course;
    }

    public LiveData<RatingCourse> getRatingCourse() {
        return ratingCourse;
    }

    public void setRatingCourse(LiveData<RatingCourse> ratingCourse) {
        this.ratingCourse = ratingCourse;
    }

    public LiveData<List<LessonResult>> getLessonResult() {
        return lessonResult;
    }

    public void setLessonResult(LiveData<List<LessonResult>> lessonResult) {
        this.lessonResult = lessonResult;
    }

    public LiveData<String> getTraineeStatus() {
        return traineeStatus;
    }

    public void setTraineeStatus(MutableLiveData<String> traineeStatus) {
        this.traineeStatus = traineeStatus;
    }

    public LiveData<Registration> getRegistration() {
        return registration;
    }

    public void setRegistration(LiveData<Registration> registration) {
        this.registration = registration;
    }

    public void updateTraineeStatus(String status) {
        this.traineeStatus.postValue(status);
    }

    public LiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(LiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public void updateSelectedLesson(LessonResult lessonResult) {
        this.courseDetailRepository.updateSelectedLesson(lessonResult);
    }

    public LiveData<List<RelatedCourseDTO>> getRelatedCourses() {
        return relatedCourses;
    }

    public void setRelatedCourses(LiveData<List<RelatedCourseDTO>> relatedCourses) {
        this.relatedCourses = relatedCourses;
    }

    public static class CourseDetailFactory implements ViewModelProvider.Factory {

        private CourseDetailRepository courseDetailRepository;

        public CourseDetailFactory(CourseDetailRepository courseDetailRepository) {
            this.courseDetailRepository = courseDetailRepository;
        }

        @NonNull
        @Override
        public CourseDetailViewModel create(@NonNull Class modelClass) {
            return new CourseDetailViewModel(courseDetailRepository);
        }
    }

}
