package cme.fpt.basecode.screens.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

public class MainSharedViewModel extends ViewModel {
    MutableLiveData<Integer> menuItem = new MutableLiveData<>();
    MutableLiveData<Boolean> isNeedHideToolbar = new MutableLiveData<>();
    MutableLiveData<Boolean> isWebviewShow = new MutableLiveData<>();

    MutableLiveData<List> buttonActionState = new MutableLiveData<>();
    MutableLiveData<Boolean> isActionButtonClick = new MutableLiveData<>();

    public MainSharedViewModel() {
        isWebviewShow.setValue(false);
    }

    public LiveData<Integer> getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(int menuItem) {
        this.menuItem.postValue(menuItem);
    }

    public LiveData<Boolean> getHideToolbar() {
        return isNeedHideToolbar;
    }

    public void setHideToolbar(boolean isHideToolbar) {
        this.isNeedHideToolbar.postValue(isHideToolbar);
    }

    public MutableLiveData<Boolean> getIsWebviewShow() {
        return isWebviewShow;
    }

    public void setIsWebviewShow(boolean isWebviewShow) {
        this.isWebviewShow.postValue(isWebviewShow);
    }

    public MutableLiveData<List> getButtonActionState() {
        return buttonActionState;
    }

    public void setButtonActionState(MutableLiveData<List> buttonActionState) {
        this.buttonActionState = buttonActionState;
    }

    public MutableLiveData<Boolean> getIsActionButtonClick() {
        return isActionButtonClick;
    }

    public void setIsActionButtonClick(MutableLiveData<Boolean> isActionButtonClick) {
        this.isActionButtonClick = isActionButtonClick;
    }
}