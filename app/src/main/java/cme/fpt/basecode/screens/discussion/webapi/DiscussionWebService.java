package cme.fpt.basecode.screens.discussion.webapi;

import cme.fpt.basecode.model.Comment;
import cme.fpt.basecode.model.Reply;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.repository.CommentResponse;
import cme.fpt.basecode.screens.discussion.repository.DiscussionResponse;
import cme.fpt.basecode.screens.discussion.repository.ReplyResponse;
import cme.fpt.basecode.screens.discussion.repository.TopicResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DiscussionWebService {

    @GET("trainee/discussions")
    Call<DiscussionResponse> getDiscussions(@Query("page") int page, @Query("size") int size, @Query("sort") String sort);

    @GET("trainee/topics")
    Call<TopicResponse> getTopics(@Query("discussionId") long discussionId, @Query("page") int page, @Query("size") int size, @Query("sort") String sort);

    @GET("trainee/topics/{id}")
    Call<Topic> getTopicById(@Path("id") long id);

    @GET("trainee/comments")
    Call<CommentResponse> getComments(@Query("topicId") long topicId, @Query("page") int page, @Query("size") int size);

    @GET("trainee/replies")
    Call<ReplyResponse> getReplies(@Query("commentId") int commentId);

    @POST("trainee/topics")
    Call<Topic> createTopic(@Body DiscussionCreateTopicRequest topic);

    @POST("trainee/topics/{topicId}/edit")
    Call<Topic> updateTopic(@Path("topicId") long topicId, @Body DiscussionCreateTopicRequest topic);

    @POST("trainee/topics/{topicId}/delete")
    Call<Void> deleteTopic(@Path("topicId") long topicId);

    @POST("trainee/comments")
    Call<Comment> createComment(@Body Comment comment);

    @POST("trainee/comments/edit")
    Call<Comment> updateComment(@Body Comment comment);

    @POST("trainee/comments/{commentId}/delete")
    Call<Void> deleteComment(@Path("commentId") long commentId);

    @POST("trainee/replies")
    Call<Reply> createReply(@Body Reply reply);

    @POST("trainee/replies/edit")
    Call<Reply> updateReply(@Body Reply reply);

    @POST("trainee/replies/{replyId}/delete")
    Call<Void> deleteReply(@Path("replyId") long replyId);

}
