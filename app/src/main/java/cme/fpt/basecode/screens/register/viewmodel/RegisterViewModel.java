package cme.fpt.basecode.screens.register.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.User;
import cme.fpt.basecode.screens.register.respository.RegisterRepository;

public class RegisterViewModel extends ViewModel {


    private RegisterRepository registerRepository;

    public RegisterViewModel(RegisterRepository registerRepository) {
        this.registerRepository = registerRepository;
    }


    public LiveData<BaseResponse> registerUser(User user) {
        return registerRepository.registerUser(user);
    }

    public static class RegisterFactory implements ViewModelProvider.Factory {

        private RegisterRepository registerRepository;

        public RegisterFactory(RegisterRepository userRepository) {
            this.registerRepository = userRepository;
        }

        @NonNull
        @Override
        public RegisterViewModel create(@NonNull Class modelClass) {
            return new RegisterViewModel(registerRepository);
        }
    }
}
