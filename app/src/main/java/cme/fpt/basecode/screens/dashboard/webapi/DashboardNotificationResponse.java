package cme.fpt.basecode.screens.dashboard.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.Notification;

public class DashboardNotificationResponse extends BaseResponse {
    @SerializedName("content")
    @Expose
    private List<Notification> listNotifications;


    public List<Notification> getListNotifications() {
        return listNotifications;
    }
}
