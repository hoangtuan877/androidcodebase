package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.repository.DiscussionRepository;

public class DiscussionViewModel extends ViewModel {
    DiscussionRepository discussionRepository;
    MutableLiveData<Topic> topic = new MutableLiveData<>();

    public DiscussionViewModel(DiscussionRepository discussionRepository) {
        this.discussionRepository = discussionRepository;
    }

    public void init() {
        topic = this.discussionRepository.getTopic();
    }

    public static class DiscussionFactory implements ViewModelProvider.Factory {

        private DiscussionRepository discussionRepository;

        public DiscussionFactory(DiscussionRepository discussionRepository) {
            this.discussionRepository = discussionRepository;
        }

        @NonNull
        @Override
        public DiscussionViewModel create(@NonNull Class modelClass) {
            return new DiscussionViewModel(discussionRepository);
        }
    }

    public DiscussionRepository getDiscussionRepository() {
        return discussionRepository;
    }

    public void setDiscussionRepository(DiscussionRepository discussionRepository) {
        this.discussionRepository = discussionRepository;
    }

    public MutableLiveData<Topic> getTopic() {
        return topic;
    }

    public void setTopic(MutableLiveData<Topic> topic) {
        this.topic = topic;
    }
}

