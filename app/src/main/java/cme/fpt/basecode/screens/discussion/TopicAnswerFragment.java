package cme.fpt.basecode.screens.discussion;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.util.List;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.R;
import cme.fpt.basecode.common.BaseSharedViewModel;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.customview.ReadMoreTextView;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentTopicAnswerBinding;
import cme.fpt.basecode.model.Comment;
import cme.fpt.basecode.model.Owner;
import cme.fpt.basecode.model.Reply;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.adapter.DiscussionTopicAdapter;
import cme.fpt.basecode.screens.discussion.adapter.TopicAdapterItem;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionCommentViewModel;
import cme.fpt.basecode.screens.main.MainSharedViewModel;

import static android.content.Context.MODE_PRIVATE;

public class TopicAnswerFragment extends Fragment implements DiscussionTopicAdapter.AnswerAdapterPresenter,
        PopupMenu.OnMenuItemClickListener {

    FragmentTopicAnswerBinding topicBinding;
    DiscussionCommentViewModel discussionCommentViewModel;
    DiscussionTopicAdapter topicAnswerBaseAdapter;
    BaseSharedViewModel mErrorViewModel;

    //Handle for load more
    private boolean isLoadingDiscussionList = false;
    private boolean isLastPage = false;
    private LinearLayoutManager mLinearLayoutManager;

    @Inject
    DiscussionCommentViewModel.DiscussionCommentFactory discussionCommentFactory;

    long topicId;
    TopicAdapterItem topicAdapterItem;

    String typeAction = Constant.CREATE_COMMENT;
    Owner owner = new Owner();
    SharedPreferences prefFile;
    boolean isStaff;

    public TopicAnswerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        topicId = getArguments().getLong("topicId", 0);
        getCurrentUserData();

        MainSharedViewModel uiViewModel = ViewModelProviders.of(getActivity()).get(MainSharedViewModel.class);
        uiViewModel.setMenuItem(4);

        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        discussionCommentViewModel = ViewModelProviders.of(this, discussionCommentFactory).get(DiscussionCommentViewModel.class);
        if(discussionCommentViewModel.getTopic().getValue() != null){
            discussionCommentViewModel.clean();
        }
        discussionCommentViewModel.setOwner(owner);
        discussionCommentViewModel.init(topicId);
        topicBinding.setDiscussionCommentModel(discussionCommentViewModel);

        mErrorViewModel = ViewModelProviders.of(getActivity()).get(BaseSharedViewModel.class);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        topicBinding.recycleAnswers.setLayoutManager(mLinearLayoutManager);
        topicBinding.recycleAnswers.setHasFixedSize(true);

        topicAnswerBaseAdapter = new DiscussionTopicAdapter(R.layout.custom_card_topic_discussion,
                R.layout.discussion_answer_item, R.layout.discussion_reply_item);

        topicAnswerBaseAdapter.setPresenter(this);
        topicBinding.recycleAnswers.setAdapter(topicAnswerBaseAdapter);
        topicBinding.recycleAnswers.addOnScrollListener(onListTolicScrollListener);
        topicBinding.sendInputReply.setOnClickListener(v -> handleComment());
        topicBinding.contentAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Do nothing to beforeTextChanged
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Do nothing to onTextChanged
            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean enable = !editable.toString().trim().isEmpty();
                topicBinding.sendInputReply.setEnabled(enable);
                topicBinding.sendInputReply.setClickable(enable);
                topicBinding.btnSendInputReply.setEnabled(enable);
            }
        });
        topicBinding.clearLayoutReplyTo.setOnClickListener(view -> clearLayoutReplyTo());


        discussionCommentViewModel.getTopic().observe(this, topic -> topicAnswerBaseAdapter.setTopic(topic));

        discussionCommentViewModel.getAddMoreComments().observe(this, addMoreComments -> {
            topicAnswerBaseAdapter.addMoreItemList(addMoreComments);
            stopLoadingState();
        });

        discussionCommentViewModel.getErrorResponse().observe(this, mErrorViewModel.getErrorResponse()::postValue);

        discussionCommentViewModel.getDeleteTopicResult().observe(this, deleteResult -> {
            if (deleteResult) {
                getActivity().onBackPressed();
            }
        });

        discussionCommentViewModel.getNewComment().observe(this, commentItem -> {
            if (commentItem.getVersion() == 0){
                topicAnswerBaseAdapter.addComment(commentItem);
                if (((LinearLayoutManager) topicBinding.recycleAnswers.getLayoutManager()).findFirstCompletelyVisibleItemPosition() > 0) {
                    topicBinding.recycleAnswers.smoothScrollToPosition(0);
                }
            } else if (commentItem.getVersion() > 0){
                topicAnswerBaseAdapter.updateComment(commentItem);
            }
        });

        discussionCommentViewModel.getDeletedCommentId().observe(this, topicAnswerBaseAdapter::deleteComment);

        discussionCommentViewModel.getNewReply().observe(this, replyItem -> {
            if (replyItem.getVersion() == 0){
                topicAnswerBaseAdapter.addReply(topicAdapterItem, replyItem);
            } else if (replyItem.getVersion() > 0){
                topicAnswerBaseAdapter.updateReply(replyItem);
            }

        });

        discussionCommentViewModel.getDeletedReplyId().observe(this, topicAnswerBaseAdapter::deleteReply);

        discussionCommentViewModel.getReplies().observe(this, this::setDataListReply);

        discussionCommentViewModel.getIsLastCommentLoaded().observe(this, isLastPageScrolled -> this.isLastPage = isLastPageScrolled);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        topicBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_topic_answer, container, false);
        topicBinding.setLifecycleOwner(this);
        return topicBinding.getRoot();
    }

    private void getCurrentUserData() {
        prefFile = this.getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        String avatar = prefFile.getString(Constant.AVATAR_URL, "");
        String currentDisplayName = prefFile.getString(Constant.CURRENT_USER_DISPLAYNAME, "");
        int currentUserId = prefFile.getInt(Constant.CURRENT_USER_ID, 0);
        isStaff = prefFile.getBoolean(Constant.IS_STAFF, false);
        Log.d("testne", "getCurrentUserData: avatar = "+avatar);
        owner.setAvatarUrl(avatar);
        owner.setDisplayName(currentDisplayName);
        owner.setUserId(currentUserId);
    }

    private void clearLayoutReplyTo() {
        topicBinding.contentAnswer.setText("");
        topicBinding.txtReplyToName.setText("");
        topicBinding.layoutReplyToName.setVisibility(View.GONE);
        typeAction = Constant.CREATE_COMMENT;
    }

    private void showReplyToNameLayout(TopicAdapterItem item) {
        topicBinding.contentAnswer.setText("");
        typeAction = Constant.CREATE_REPLY;
        if (item.getData() instanceof Comment) {
            topicBinding.txtReplyToName.setText(((Comment) item.getData()).getOwner().getDisplayName());
        } else {
            topicBinding.txtReplyToName.setText(((Reply) item.getData()).getOwner().getDisplayName());
        }

        topicBinding.layoutReplyToName.setVisibility(topicBinding.layoutReplyToName.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    private void handleComment() {
        if (!topicBinding.contentAnswer.getText().toString().isEmpty()) {
            switch (typeAction) {
                case Constant.CREATE_COMMENT:
                    addComment();
                    break;
                case Constant.UPDATE_COMMENT:
                    updateComment();
                    break;
                case Constant.CREATE_REPLY:
                    addReply();
                    break;
                case Constant.UPDATE_REPLY:
                    updateReply();
                    break;
                default:
                    break;
            }
        }
        // Reset action type
        View view = getActivity().getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        typeAction = Constant.CREATE_COMMENT;
    }

    private void addComment() {
        Comment comment = new Comment();
        comment.setContent(topicBinding.contentAnswer.getText().toString().trim());
        comment.setOwner(owner);
        comment.setParentId(discussionCommentViewModel.getTopic().getValue().getId());
        discussionCommentViewModel.createComment(comment);
        topicBinding.contentAnswer.setText("");
    }

    private void getDataForUpdate() {
        if (topicAdapterItem.getData() instanceof Comment) {
            typeAction = Constant.UPDATE_COMMENT;
            String answerContent = ((Comment) topicAdapterItem.getData()).getContent();
            topicBinding.contentAnswer.setText(answerContent);
            topicBinding.contentAnswer.setSelection(answerContent.length());
        } else {
            typeAction = Constant.UPDATE_REPLY;
            String replyContent = ((Reply) topicAdapterItem.getData()).getContent();
            topicBinding.contentAnswer.setText(replyContent);
            topicBinding.contentAnswer.setSelection(replyContent.length());
        }
    }

    private void updateComment() {
        Comment comment = (Comment) topicAdapterItem.getData();
        comment.setContent(topicBinding.contentAnswer.getText().toString().trim());
        discussionCommentViewModel.updateComment(comment);
        topicBinding.contentAnswer.setText("");
    }

    private void removeComment() {
        discussionCommentViewModel.deleteComment(((Comment) topicAdapterItem.getData()).getId());
    }

    private void addReply() {
        Reply reply = new Reply();
        reply.setContent(topicBinding.contentAnswer.getText().toString().trim());
        reply.setOwner(owner);
        if (topicAdapterItem.getData() instanceof Comment) {
            reply.setParentId(((Comment) topicAdapterItem.getData()).getId());
        } else {
            reply.setParentId(((Reply) topicAdapterItem.getData()).getParentId());
        }
        discussionCommentViewModel.createReply(reply);
        clearLayoutReplyTo();
    }

    private void updateReply() {
        Reply reply = (Reply) topicAdapterItem.getData();
        reply.setContent(topicBinding.contentAnswer.getText().toString().trim());

        discussionCommentViewModel.updateReply(reply);
        topicBinding.contentAnswer.setText("");
    }

    private void removeReply() {
        discussionCommentViewModel.deleteReply(((Reply) topicAdapterItem.getData()).getId());
    }

    @Override
    public void onAnswerClick(TopicAdapterItem item) {
        if (((Comment) item.getData()).getCountReply() != 0) {
            List<Reply> replies = ((Comment) item.getData()).getReplies();
            if (replies!= null && !replies.isEmpty()) {
                topicAnswerBaseAdapter.expandAnswer(replies);
            } else {
                discussionCommentViewModel.getReplyByComment(((Comment) item.getData()).getId());
            }
        }
    }

    public void setDataListReply(List<Reply> listReply) {
        topicAnswerBaseAdapter.expandAnswer(listReply);
    }

    @Override
    public void onAnswerIconClick(View view, TopicAdapterItem item) {
        topicAdapterItem = item;
        if(!item.isExpandedChildren()){
            onAnswerClick(item);
        }
        showReplyToNameLayout(item);
    }

    @Override
    public void onHandleTopicIconClick(View view) {
        PopupMenu popup = new PopupMenu(this.getContext(), view, Gravity.RIGHT);
        MenuInflater inflater = popup.getMenuInflater();
        if(isStaff){
            inflater.inflate(R.menu.menu_topic_staff, popup.getMenu());
        }else{
            inflater.inflate(R.menu.menu_topic, popup.getMenu());
        }
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_topic_menu:
                    Bundle bundle = new Bundle();
                    Topic topic = new Topic();
                    topic.setParentId(discussionCommentViewModel.getTopic().getValue().getParentId());
                    topic.setId(discussionCommentViewModel.getTopic().getValue().getId());
                    topic.setTitle(discussionCommentViewModel.getTopic().getValue().getTitle());
                    topic.setContent(discussionCommentViewModel.getTopic().getValue().getContent());
                    bundle.putSerializable("topic", topic);
                    Navigation.findNavController(TopicAnswerFragment.this.getView()).navigate(R.id.action_topicAnswerFragment_to_discussionCreateNewFragment, bundle);
                return true;
            case R.id.remove_topic_menu:
            case R.id.remove_topic_staff_menu:
                createDeleteTopicDialog();
                return true;
            case R.id.edit_comment_menu:
                getDataForUpdate();
                return true;
            case R.id.remove_comment_menu:
            case R.id.remove_comment_staff_menu:
                if (topicAdapterItem.getData() instanceof Comment) {
                    removeComment();
                } else {
                    removeReply();
                }
                return true;
            default:
                return false;
        }
    }

    @Override
    public void scrollToAnswerPosition(int position) {
        topicBinding.recycleAnswers.smoothScrollToPosition(position);
    }

    @Override
    public void onAnswerReadMoreClick(View v, TopicAdapterItem item) {
        topicAnswerBaseAdapter.showReadMoreAnswer((ReadMoreTextView) v, item);
    }

    @Override
    public boolean onAnswerLongClick(View view, TopicAdapterItem item) {
        boolean isShowMenu = false;
        if(item.getData() instanceof Comment){
            isShowMenu = ((Comment) item.getData()).getOwner().getUserId().intValue() == owner.getUserId().intValue() || isStaff;
        }else{
            isShowMenu = ((Reply) item.getData()).getOwner().getUserId().intValue() == owner.getUserId().intValue() || isStaff;
        }

        if(isShowMenu){
            topicAdapterItem = item;
            PopupMenu popup = new PopupMenu(this.getContext(), view, Gravity.CENTER, R.attr.actionOverflowMenuStyle, 0);
            MenuInflater inflater = popup.getMenuInflater();

            if(isStaff){
                inflater.inflate(R.menu.menu_comment_staff, popup.getMenu());
            }else{
                inflater.inflate(R.menu.menu_comment, popup.getMenu());
            }

            popup.setOnMenuItemClickListener(this);
            popup.show();
        }

        return true;
    }

    private void startLoadingState () {
        topicBinding.loadingMoreLayout.setVisibility(View.VISIBLE);
        isLoadingDiscussionList = true;
    }
    private void stopLoadingState () {
        topicBinding.loadingMoreLayout.setVisibility(View.INVISIBLE);
        isLoadingDiscussionList = false;
    }


    @Override
    public void onStop() {
        super.onStop();
        discussionCommentViewModel.getTopic().removeObservers(this);
        discussionCommentViewModel.getAddMoreComments().removeObservers(this);
        discussionCommentViewModel.getErrorResponse().removeObservers(this);
        discussionCommentViewModel.getDeleteTopicResult().removeObservers(this);
        discussionCommentViewModel.getNewComment().removeObservers(this);
        discussionCommentViewModel.getDeletedCommentId().removeObservers(this);
        discussionCommentViewModel.getNewReply().removeObservers(this);
        discussionCommentViewModel.getDeletedReplyId().removeObservers(this);
        discussionCommentViewModel.getReplies().removeObservers(this);
        discussionCommentViewModel.getIsLastCommentLoaded().removeObservers(this);
    }

    private void createDeleteTopicDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.delete_topic_confirm));
        builder.setPositiveButton(getString(R.string.remove), (dialogPos, id) -> {
            discussionCommentViewModel.deleteTopic();
            dialogPos.dismiss();
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialogPos, which) -> dialogPos.dismiss());
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private RecyclerView.OnScrollListener onListTolicScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if(newState != 0) return;
            int lastCompleteItem = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
            if(!isLastPage && !isLoadingDiscussionList && lastCompleteItem == topicAnswerBaseAdapter.getListItem().size()) {
                new Handler().postDelayed(() -> {
                    int pageNum = topicAnswerBaseAdapter.getListItem().size()/ Constant.SIZE_DISCUSSION_COURSES;
                    discussionCommentViewModel.callUpdateCommentAPI(topicId, pageNum, Constant.SIZE_DISCUSSION_COURSES);
                }, 1000);
                startLoadingState();
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            // Do nothing with onScrolled
        }
    };
}
