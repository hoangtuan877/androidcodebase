package cme.fpt.basecode.screens.dashboard.repository;

import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import javax.inject.Singleton;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.model.Content;
import cme.fpt.basecode.model.DashboardDiscussionResponse;
import cme.fpt.basecode.model.Notification;
import cme.fpt.basecode.model.StudyClass;
import cme.fpt.basecode.screens.dashboard.webapi.DashboardNotificationResponse;
import cme.fpt.basecode.screens.dashboard.webapi.DashboardWebService;
import cme.fpt.basecode.screens.dashboard.webapi.StudyClassResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class DashboardRepository {

    DashboardWebService dashboardWebService;
    private static String TAG = "DashboardRepository";

    //ERROR HANDLER
    MutableLiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();
    MutableLiveData<List<StudyClass>> listStudyClass = new MutableLiveData<>();
    MutableLiveData<List<Notification>> listNotification = new MutableLiveData<>();
    MutableLiveData<List<Content>> mListContent = new MutableLiveData<>();

    public DashboardRepository(DashboardWebService dashboardWebService) {
        this.dashboardWebService = dashboardWebService;
    }

    public void init() {
        this.getTopStudyClass();
        this.getTopNotifications();
        this.getTopDiscussions();
    }

    public void getTopStudyClass() {
        dashboardWebService.getTopStudyClasses(0, 5, Constant.IN_PROGRESS_STATUS, "desc").enqueue(new Callback<StudyClassResponse>() {
            @Override
            public void onResponse(Call<StudyClassResponse> call, Response<StudyClassResponse> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                } else {
                    listStudyClass.postValue(response.body().getListStudyClass());
                }
            }

            @Override
            public void onFailure(Call<StudyClassResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });

    }

    public void getTopNotifications() {
        dashboardWebService.getTopNotifications(0, 5, Constant.CREATED_DATE_DESC).enqueue(new Callback<DashboardNotificationResponse>() {
            @Override
            public void onResponse(Call<DashboardNotificationResponse> call, Response<DashboardNotificationResponse> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                } else {
                    listNotification.postValue(response.body().getListNotifications());
                }
            }

            @Override
            public void onFailure(Call<DashboardNotificationResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void getTopDiscussions() {
        dashboardWebService.getTopDiscussions(0, 5, Constant.LAST_MODIFIED_DATE_DESC).enqueue(new Callback<DashboardDiscussionResponse>() {
            @Override
            public void onResponse(Call<DashboardDiscussionResponse> call, Response<DashboardDiscussionResponse> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                } else {
                    mListContent.postValue(response.body().getContent());
                }
            }

            @Override
            public void onFailure(Call<DashboardDiscussionResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void seenNotification(int[] ids) {
        dashboardWebService.postSeenNotification(ids).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                } else {
                    getTopNotifications();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public MutableLiveData<List<StudyClass>> getListStudyClass() {
        return listStudyClass;
    }

    public void setListStudyClass(MutableLiveData<List<StudyClass>> listStudyClass) {
        this.listStudyClass = listStudyClass;
    }

    public MutableLiveData<List<Notification>> getListNotification() {
        return listNotification;
    }

    public void setListNotification(MutableLiveData<List<Notification>> listNotification) {
        this.listNotification = listNotification;
    }

    public MutableLiveData<List<Content>> getmListContent() {
        return mListContent;
    }

    public void setmListContent(MutableLiveData<List<Content>> mListContent) {
        this.mListContent = mListContent;
    }

    public MutableLiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(MutableLiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    private void postErrorResponse(BaseErrorResponse response) {
        errorResponse.postValue(response);
    }
}
