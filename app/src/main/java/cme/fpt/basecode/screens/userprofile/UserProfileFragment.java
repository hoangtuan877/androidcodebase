package cme.fpt.basecode.screens.userprofile;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.R;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentUserProfileBinding;
import cme.fpt.basecode.screens.userprofile.viewmodel.UserProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener {

    FragmentUserProfileBinding userProfileBinding;

    @Inject
    UserProfileViewModel.UserProfileFactory userProfileFactory;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Inject data
        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        UserProfileViewModel  viewModel = ViewModelProviders.of(this, userProfileFactory).get(UserProfileViewModel.class);
        viewModel.init("1");
        userProfileBinding.setUserProfile(viewModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        userProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_profile, container, false);
        userProfileBinding.setLifecycleOwner(this);
        // Set click event for element using data binding
        userProfileBinding.userProfileAvatar.setOnClickListener(this);
        getActivity().setTitle(R.string.profile_title);

        return userProfileBinding.getRoot();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.user_profile_avatar) {
            Navigation.findNavController(view).navigate(R.id.action_userProfileFragment_to_blankFragment);
        }
    }
}
