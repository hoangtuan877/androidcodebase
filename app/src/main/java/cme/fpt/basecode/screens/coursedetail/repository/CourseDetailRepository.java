package cme.fpt.basecode.screens.coursedetail.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.text.TextUtils;

import java.io.IOException;
import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.model.Course;
import cme.fpt.basecode.model.LessonResult;
import cme.fpt.basecode.model.RatingCourse;
import cme.fpt.basecode.model.Registration;
import cme.fpt.basecode.model.RelatedCourseDTO;
import cme.fpt.basecode.model.RelatedCourses;
import cme.fpt.basecode.screens.coursedetail.webapi.CourseDetailWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CourseDetailRepository {

    private CourseDetailWebService webService;
    private static final String TAG = "CourseDetailRepository";

    MutableLiveData<Course> courseLiveData = new MutableLiveData<>();
    MutableLiveData<RatingCourse> ratingCourseLiveData = new MutableLiveData<>();
    MutableLiveData<List<LessonResult>> lessonResult = new MutableLiveData<>();
    MutableLiveData<String> traineeStatus = new MutableLiveData<>();
    MutableLiveData<Registration> registrationLiveData = new MutableLiveData<>();

    //ERROR HANDLER
    MutableLiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();
    MutableLiveData<List<RelatedCourseDTO>> relatedCoursesLiveData = new MutableLiveData<>();

    public CourseDetailRepository() {
    }

    public CourseDetailRepository(CourseDetailWebService webService) {
        this.webService = webService;
    }

    public void init(String courseId) {

        webService.getCourseById(courseId).enqueue(new Callback<CourseDetailResponse>() {
            @Override
            public void onResponse(Call<CourseDetailResponse> call, retrofit2.Response<CourseDetailResponse> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                } else {
                    handleInitResponse(response);
                }
            }

            @Override
            public void onFailure(Call<CourseDetailResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });

        getRelatedCourses();
    }

    private void handleInitResponse(Response<CourseDetailResponse> response) {
        if (response.body() == null) return;
        if (response.body().getCourse() != null) {
            courseLiveData.postValue(response.body().getCourse());
        }
        if (response.body().getRatingCourse() != null) {
            ratingCourseLiveData.postValue(response.body().getRatingCourse());
        }
        if (response.body().getLessonResults() != null) {
            lessonResult.postValue(response.body().getLessonResults());
        }
        if (response.body().getRegistration() != null) {
            registrationLiveData.postValue(response.body().getRegistration());
        }
        if (!TextUtils.isEmpty(response.body().getTraineeStatus())) {
            traineeStatus.postValue(response.body().getTraineeStatus());
        }
    }

    public void submitRating(String point, String courseId) {
        webService.submitRating(point, courseId).enqueue(new Callback<RatingCourse>() {
            @Override
            public void onResponse(Call<RatingCourse> call, retrofit2.Response<RatingCourse> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    ratingCourseLiveData.postValue(response.body());
                    courseLiveData.getValue().setRateCount(response.body().getCountRating());
                    courseLiveData.getValue().setRatePoint(response.body().getAveragePoint());
                } else {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                }
            }

            @Override
            public void onFailure(Call<RatingCourse> call, Throwable t) {
                String error = "Error submit rating";
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, error));
            }
        });
    }

    public void enrollCourse(int courseId, Callback callback) {
        webService.enrollCourseById(String.valueOf(courseId)).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                callback.onFailure(call, t);
            }
        });
    }

    private void postErrorResponse(BaseErrorResponse response) {
        errorResponse.postValue(response);
    }

    public void getRelatedCourses() {
        MutableLiveData<BaseResponse> data = new MutableLiveData<>();
        webService.relatedCourses(Constant.PAGE_RELATED_COURSES, Constant.SIZE_RELATED_COURSES, Constant.TYPE_RELATED_COURSES).enqueue(new Callback<RelatedCourses>() {
            @Override
            public void onResponse(Call<RelatedCourses> call, Response<RelatedCourses> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    if (response.body().getContent() != null)
                        relatedCoursesLiveData.setValue(response.body().getContent());
                } else {
                    try {
                        data.postValue(new BaseResponse(response.code(), response.errorBody().string().toString()));
                    } catch (IOException e) {
                        postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
                    }
                }
            }

            @Override
            public void onFailure(Call<RelatedCourses> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public LiveData<Course> getCourseDetail() {
        courseLiveData.postValue(Course.createDummy());
        return courseLiveData;
    }

    public MutableLiveData<Course> getCourseLiveData() {
        return courseLiveData;
    }

    public void updateSelectedLesson(LessonResult lesson) {
        if (lesson == null) {
            return;
        }
        webService.getLessonById("" + lesson.getRegistration().getId(), lesson.getLesson().getId()).enqueue(new Callback<LessonResult>() {
            @Override
            public void onResponse(Call<LessonResult> call, Response<LessonResult> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    List<LessonResult> lessonResults = lessonResult.getValue();
                    int index = lessonResults.indexOf(lesson);
                    if(index != -1) {
                        lessonResults.set(index, response.body());
                        lessonResult.postValue(lessonResults);
                    }
                } else {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                }
            }

            @Override
            public void onFailure(Call<LessonResult> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }


    public MutableLiveData<List<LessonResult>> getLessonResultLiveData() {
        return lessonResult;
    }

    public void setCourseLiveData(MutableLiveData<Course> courseLiveData) {
        this.courseLiveData = courseLiveData;
    }

    public MutableLiveData<RatingCourse> getRatingCourseLiveData() {
        return ratingCourseLiveData;
    }

    public void setRatingCourseData(MutableLiveData<RatingCourse> ratingCourseLiveData) {
        this.ratingCourseLiveData = ratingCourseLiveData;
    }

    public MutableLiveData<String> getTraineeStatus() {
        return traineeStatus;
    }

    public void setTraineeStatus(MutableLiveData<String> traineeStatus) {
        this.traineeStatus = traineeStatus;
    }

    public LiveData<Registration> getRegistrationLiveData() {
        return registrationLiveData;
    }

    public void setRegistrationLiveData(MutableLiveData<Registration> registrationLiveData) {
        this.registrationLiveData = registrationLiveData;
    }

    public MutableLiveData<BaseErrorResponse> getErrorResponseLiveData() {
        return errorResponse;
    }

    public void setErrorResponseLiveData(MutableLiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public LiveData<List<RelatedCourseDTO>> getRelatedCoursesLiveData() {
        return relatedCoursesLiveData;
    }

    public void setRelatedCoursesData(MutableLiveData<List<RelatedCourseDTO>> relatedCoursesLiveData) {
        this.relatedCoursesLiveData = relatedCoursesLiveData;
    }
}
