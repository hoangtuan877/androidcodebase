package cme.fpt.basecode.screens.dashboard.adapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import cme.fpt.basecode.R;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.Notification;

public class DashboardNotificationAdapter extends DataBindingAdapter {

    List<Notification> notifications;
    private final int layoutId;
    private NotificationAdapterPresenter presenter;

    public DashboardNotificationAdapter(int layoutId, List<Notification> notifications) {
        this.layoutId = layoutId;
        this.notifications = notifications;
    }

    public void setPresenter(NotificationAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected Object getObjectForPosition(int position) {
        return this.notifications.get(position);
    }

    @Override
    protected Object getPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return this.notifications.size();
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        if (this.notifications != null) {
            this.notifications.clear();
            this.notifications.addAll(notifications);
        } else {
            this.notifications = notifications;
        }

        this.notifyDataSetChanged();
    }

    @BindingAdapter({"notifyIcon"})
    public static void setNotifyIcon(ImageView imageView, String data) {
        switch (data) {
            case Constant.REGISTER_SUCCESS:
                imageView.setImageResource(R.drawable.ic_notification_success);
                break;
            case Constant.COMMENT:
                imageView.setImageResource(R.drawable.ic_error_black_24dp);
                break;
            case Constant.REPLY:
                imageView.setImageResource(R.drawable.ic_error_black_24dp);
                break;
            case Constant.STARTING_SOON:
                imageView.setImageResource(R.drawable.ic_error_black_24dp);
                break;
            case Constant.COURSE_COMPLETE:
                imageView.setImageResource(R.drawable.ic_notification_success);
                break;
            default:
                imageView.setImageResource(R.drawable.ic_notification_success);
                break;
        }
    }

    public interface NotificationAdapterPresenter {
        void onNotificationClick(View v, Notification data);
    }
}
