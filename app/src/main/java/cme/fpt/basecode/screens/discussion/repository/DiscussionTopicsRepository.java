package cme.fpt.basecode.screens.discussion.repository;

import android.arch.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionCreateTopicRequest;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscussionTopicsRepository {
    DiscussionWebService discussionWebService;
    MutableLiveData<List<Topic>> topics = new MutableLiveData<>();
    MutableLiveData<Boolean> isLastTopicLoaded = new MutableLiveData<>();
    MutableLiveData<Discussion> discussion = new MutableLiveData<>();

    //ERROR HANDLER
    MutableLiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();
    MutableLiveData<Boolean> createOrUpdateResult = new MutableLiveData<>();
    MutableLiveData<Boolean> emptyTopics = new MutableLiveData<>();

    public DiscussionTopicsRepository(DiscussionWebService discussionWebService) {
        this.discussionWebService = discussionWebService;
        this.topics.postValue(new ArrayList<>());
        this.createOrUpdateResult.postValue(false);
        emptyTopics.postValue(false);
    }

    public void init(long discussionId) {
        getListTopicOnAPI(discussionId, Constant.PAGE_DISCUSSION_TOPICS, Constant.SIZE_DISCUSSION_TOPICS);
    }

    public void getListTopicOnAPI(long discussionId, int pageNum, int pageSize) {
        this.discussionWebService.getTopics(discussionId, pageNum, pageSize, Constant.LAST_MODIFIED_DATE_DESC).enqueue(new Callback<TopicResponse>() {
            @Override
            public void onResponse(Call<TopicResponse> call, Response<TopicResponse> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    emptyTopics.postValue(true);
                } else {
                    if (response.body().getContent() != null) {
                        List<Topic> listNew = topics.getValue();
                        listNew.addAll(response.body().getContent());
                        topics.postValue(listNew);
                        emptyTopics.postValue(false);
                    } else {
                        topics.postValue(new ArrayList<>());
                        if(topics.getValue().isEmpty()) {
                            emptyTopics.postValue(true);
                        }
                    }
                    if (response.body().getDiscussion() != null) {
                        discussion.postValue(response.body().getDiscussion());
                    }
                    isLastTopicLoaded.postValue(response.body().isLast());
                }
            }

            @Override
            public void onFailure(Call<TopicResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void createTopic(Topic topic) {
        DiscussionCreateTopicRequest topicRequest = new DiscussionCreateTopicRequest(topic.getParentId(), topic.getTitle(), topic.getContent());
        this.discussionWebService.createTopic(topicRequest).enqueue(new Callback<Topic>() {
            @Override
            public void onResponse(Call<Topic> call, Response<Topic> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    createOrUpdateResult.postValue(false);
                } else {
                    createOrUpdateResult.postValue(true);
                    emptyTopics.postValue(false);
                }
            }

            @Override
            public void onFailure(Call<Topic> call, Throwable t) {
                createOrUpdateResult.postValue(false);
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void updateTopic(Topic topic) {
        DiscussionCreateTopicRequest topicRequest = new DiscussionCreateTopicRequest(topic.getParentId(), topic.getTitle(), topic.getContent());
        this.discussionWebService.updateTopic(topic.getId(), topicRequest).enqueue(new Callback<Topic>() {
            @Override
            public void onResponse(Call<Topic> call, Response<Topic> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    try {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.errorBody() != null ? response.errorBody().string().toString() : response.message()));
                    } catch (IOException e) {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    }
                    createOrUpdateResult.postValue(false);
                } else {
                    createOrUpdateResult.postValue(true);
                    emptyTopics.postValue(false);
                }
            }

            @Override
            public void onFailure(Call<Topic> call, Throwable t) {
                createOrUpdateResult.postValue(false);
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    private void postErrorResponse(BaseErrorResponse response) {
        errorResponse.postValue(response);
    }

    public MutableLiveData<List<Topic>> getTopics() {
        return topics;
    }

    public void setTopics(MutableLiveData<List<Topic>> topics) {
        this.topics = topics;
    }

    public MutableLiveData<Discussion> getDiscussion() {
        return discussion;
    }

    public void setDiscussion(MutableLiveData<Discussion> discussion) {
        this.discussion = discussion;
    }

    public MutableLiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(MutableLiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public DiscussionWebService getDiscussionWebService() {
        return discussionWebService;
    }

    public void setDiscussionWebService(DiscussionWebService discussionWebService) {
        this.discussionWebService = discussionWebService;
    }

    public MutableLiveData<Boolean> getCreateOrUpdateResult() {
        return createOrUpdateResult;
    }

    public void setCreateOrUpdateResult(MutableLiveData<Boolean> createOrUpdateResult) {
        this.createOrUpdateResult = createOrUpdateResult;
    }

    public MutableLiveData<Boolean> getEmptyTopics() {
        return emptyTopics;
    }

    public void setEmptyTopics(MutableLiveData<Boolean> emptyTopics) {
        this.emptyTopics = emptyTopics;
    }

    public MutableLiveData<Boolean> getIsLastTopicLoaded() {
        return isLastTopicLoaded;
    }
}
