package cme.fpt.basecode.screens.login.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import cme.fpt.basecode.screens.login.repository.CurrentUserResponse;
import cme.fpt.basecode.screens.login.repository.LoginRepository;
import cme.fpt.basecode.screens.login.repository.LoginResponse;

public class LoginViewModel extends ViewModel {

    LoginRepository loginRepository;
    LiveData<LoginResponse> loginData = new MutableLiveData<>();
    LiveData<String> lrsBasicAuthVal = new MutableLiveData<>();
    LiveData<String> lrsEndPoint = new MutableLiveData<>();
    LiveData<CurrentUserResponse> currentUser = new MutableLiveData<>();

    public LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public void getLRSAuthVal() {
        lrsBasicAuthVal = loginRepository.getLRSBasicAuthValue();
    }

    public void getLRSEndpoint() {
        lrsEndPoint = loginRepository.getLRSEndpoint();
    }

    public void login(String username, String password) {
        loginData = loginRepository.login(username, password);
    }

    public void currentUserData(){
        currentUser = loginRepository.getCurrentUser();
    }

    public LiveData<LoginResponse> getLoginData() {
        return loginData;
    }

    public static class LoginFactory implements ViewModelProvider.Factory {

        private LoginRepository loginRepository;

        public LoginFactory(LoginRepository loginRepository) {
            this.loginRepository = loginRepository;
        }

        @NonNull
        @Override
        public LoginViewModel create(@NonNull Class modelClass) {
            return new LoginViewModel(loginRepository);
        }
    }

    public LiveData<String> getLrsBasicAuthValLiveData() {
        return lrsBasicAuthVal;
    }

    public void setLrsBasicAuthVal(LiveData<String> lrsBasicAuthVal) {
        this.lrsBasicAuthVal = lrsBasicAuthVal;
    }

    public LiveData<String> getLrsEndPointLiveData() {
        return lrsEndPoint;
    }

    public void setLrsEndPoint(LiveData<String> lrsEndPoint) {
        this.lrsEndPoint = lrsEndPoint;
    }

    public LiveData<CurrentUserResponse> getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(LiveData<CurrentUserResponse> currentUser) {
        this.currentUser = currentUser;
    }
}
