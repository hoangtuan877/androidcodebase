package cme.fpt.basecode.screens.coursedetail.webapi;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.LessonResult;
import cme.fpt.basecode.model.RatingCourse;
import cme.fpt.basecode.model.RelatedCourses;
import cme.fpt.basecode.screens.coursedetail.repository.CourseDetailResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CourseDetailWebService {
    @POST("courses/rating")
    Call<RatingCourse> submitRating(@Query("point") String point, @Query("courseId") String courseId);

    @GET("courses/{id}")
    Call<CourseDetailResponse> getCourseById(@Path("id") String id);

    @POST("registration/courses/{id}")
    Call<BaseResponse> enrollCourseById(@Path("id") String id);

    @GET("registration/{id}/result")
    Call<LessonResult> getLessonById(@Path("id") String id, @Query("lessonId") int lessonId);

    @GET("courses")
    Call<RelatedCourses> relatedCourses(@Query("page") int page, @Query("size") int size, @Query("type") String type);
}
