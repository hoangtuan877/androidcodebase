package cme.fpt.basecode.screens.login.webapi;

import cme.fpt.basecode.screens.login.repository.CurrentUserResponse;
import cme.fpt.basecode.screens.login.repository.LoginRequest;
import cme.fpt.basecode.screens.login.repository.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface LoginWebService {
    @POST("auth/trainee/token")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET("meta-data/lrs/endpoint")
    Call<String> getLRSEndpoint();

    @GET("meta-data/lrs/basicAuthVal")
    Call<String> getLRSBasicAuthVal();

    @GET("users/current-user-info")
    Call<CurrentUserResponse> getCurrentUser();
}
