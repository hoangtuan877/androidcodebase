package cme.fpt.basecode.screens.logout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import cme.fpt.basecode.LoginActivity;
import cme.fpt.basecode.R;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.screens.logout.repository.LogoutRepository;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by PhucNT17 on 6/12/2018.
 */

public class LogoutFragment extends Fragment {

    @Inject
    LogoutRepository logoutRepository;

    SharedPreferences prefFile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        prefFile = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        return inflater.inflate(R.layout.fragment_logout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logout();
    }

    private void logout() {
        refreshTokenForLogout();
        showLoginActivity();
    }

    private void refreshTokenForLogout() {
        SharedPreferences.Editor editor = prefFile.edit();
        if (!prefFile.getString(Constant.USER_TOKEN, "").isEmpty()) {
            editor.putString(Constant.USER_TOKEN, "");
            editor.commit();
        }

        if(!prefFile.getString(Constant.AVATAR_URL, "").isEmpty()){
            editor.putString(Constant.AVATAR_URL, "");
            editor.commit();
        }

        if(!prefFile.getString(Constant.CURRENT_USER_DISPLAYNAME, "").isEmpty()){
            editor.putString(Constant.CURRENT_USER_DISPLAYNAME, "");
            editor.commit();
        }

        if(prefFile.getInt(Constant.CURRENT_USER_ID, 0) != 0){
            editor.putInt(Constant.CURRENT_USER_ID, 0);
            editor.commit();
        }
    }

    private void showLoginActivity() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
