package cme.fpt.basecode.screens.coursedetail.webview;

import android.webkit.JavascriptInterface;

public class WebAppInterface {

    FragmentInteractionHelper helper;

    /**
     * Instantiate the interface and set the context
     */
    public WebAppInterface() {
        // Default constructor
    }

    @JavascriptInterface
    public void alertMessage(String message) {
        if (helper != null) {
            helper.alertMessage(message);
        }
    }

    @JavascriptInterface
    public void closeWebApp() {
        if (helper != null) {
            helper.closeWebApp();
        }
    }

    public void setHelper(FragmentInteractionHelper helper) {
        this.helper = helper;
    }

    public interface FragmentInteractionHelper {
        void alertMessage(String message);

        void closeWebApp();

    }
}