package cme.fpt.basecode.screens.userprofile.webapi;


import cme.fpt.basecode.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UserWebService {
    @GET("user/{id}")
    Call<User> getUserProfile(@Path("id") String id);
}
