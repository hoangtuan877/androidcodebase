package cme.fpt.basecode.screens.discussion.repository;

import android.arch.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.model.Comment;
import cme.fpt.basecode.model.Reply;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscussionCommentRepository {

    DiscussionWebService discussionWebService;
    MutableLiveData<Topic> topic = new MutableLiveData<>();
    Topic currentTopic;

    MutableLiveData<List<Comment>> addMoreComments = new MutableLiveData<>();
    MutableLiveData<Boolean> isLastCommentLoaded = new MutableLiveData<>();
    MutableLiveData<List<Reply>> replies = new MutableLiveData<>();

    MutableLiveData<Comment> newComment = new MutableLiveData<>();
    MutableLiveData<Reply> newReply = new MutableLiveData<>();

    MutableLiveData<Boolean> callApiResult = new MutableLiveData<>();

    MutableLiveData<Integer> deletedCommentId = new MutableLiveData<>();
    MutableLiveData<Integer> deletedReplyId = new MutableLiveData<>();

    //ERROR HANDLER
    MutableLiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();

    MutableLiveData<Boolean> deleteTopicResult = new MutableLiveData<>();

    public DiscussionCommentRepository(DiscussionWebService discussionWebService) {
        this.discussionWebService = discussionWebService;

    }

    public void init(long topicId) {
        this.discussionWebService.getTopicById(topicId).enqueue(new Callback<Topic>() {

            @Override
            public void onResponse(Call<Topic> call, Response<Topic> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    response.body().setComments(new ArrayList<>());
                    currentTopic = response.body();
                    getCommentsByTopic(topicId);
                } else {
                    try {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.errorBody() != null ? response.errorBody().string().toString() : response.message()));
                    } catch (IOException e) {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    }
                }
            }

            @Override
            public void onFailure(Call<Topic> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });

    }

    public void getCommentsByTopic(long topicId) {
        this.discussionWebService.getComments(topicId, Constant.PAGE_DISCUSSION_COMMENT, Constant.SIZE_DISCUSSION_COMMENT).enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    if (response.body().getContent() != null ) {
                        currentTopic.setComments(response.body().getContent());
                    } else {
                        currentTopic.setComments(new ArrayList<>());
                    }
                    isLastCommentLoaded.postValue(response.body().isLast());
                    topic.setValue(currentTopic);
                } else {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void updateCommentList(long topicId, int pageNum, int pageSize) {
        this.discussionWebService.getComments(topicId, pageNum, pageSize).enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    if (response.body().getContent() != null) {
                        if (addMoreComments.getValue() != null &&
                                addMoreComments.getValue().containsAll(response.body().getContent())){
                            addMoreComments.postValue(new ArrayList<>());
                        } else {
                            addMoreComments.postValue(response.body().getContent());
                        }
                        isLastCommentLoaded.postValue(response.body().isLast());
                    }
                } else {
                    // Finish loading when error
                    addMoreComments.postValue(new ArrayList<>());
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                // Finish loading
                addMoreComments.postValue(new ArrayList<>());
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void getReplyByComment(int commentId) {
        this.discussionWebService.getReplies(commentId).enqueue(new Callback<ReplyResponse>() {
            @Override
            public void onResponse(Call<ReplyResponse> call, Response<ReplyResponse> response) {
                if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                    if (response.body().getReplies() != null) {
                        replies.postValue(response.body().getReplies());
                    }
                } else {
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                }
            }

            @Override
            public void onFailure(Call<ReplyResponse> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void createComment(Comment comment) {
        this.discussionWebService.createComment(comment).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    callApiResult.postValue(false);
                } else {
                    newComment.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
                callApiResult.postValue(false);
            }
        });
    }

    public void updateComment(Comment comment) {
        this.discussionWebService.updateComment(comment).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    callApiResult.postValue(false);
                    postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                } else {
                    newComment.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
                callApiResult.postValue(false);
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void deleteComment(int commentId) {
        this.discussionWebService.deleteComment(commentId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    try {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.errorBody() != null ? response.errorBody().string().toString() : response.message()));
                    } catch (IOException e) {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    }
                } else {
                    deletedCommentId.postValue(commentId);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void createReply(Reply reply) {
        this.discussionWebService.createReply(reply).enqueue(new Callback<Reply>() {
            @Override
            public void onResponse(Call<Reply> call, Response<Reply> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    callApiResult.postValue(false);
                } else {
                    newReply.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Reply> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void updateReply(Reply reply) {
        this.discussionWebService.updateReply(reply).enqueue(new Callback<Reply>() {
            @Override
            public void onResponse(Call<Reply> call, Response<Reply> response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    callApiResult.postValue(false);
                } else {
                    newReply.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Reply> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void deleteReply(int replyId) {
        this.discussionWebService.deleteReply(replyId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    try {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.errorBody() != null ? response.errorBody().string().toString() : response.message()));
                    } catch (IOException e) {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    }
                } else {
                    deletedReplyId.postValue(replyId);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public void deleteTopic() {
        this.discussionWebService.deleteTopic(topic.getValue().getId()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != BaseResponse.SUCCESS_CODE) {
                    try {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.errorBody() != null ? response.errorBody().string().toString() : response.message()));
                    } catch (IOException e) {
                        postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                    }
                } else {
                    deleteTopicResult.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
            }
        });
    }

    public MutableLiveData<Topic> getTopic() {
        return topic;
    }

    public void setTopic(MutableLiveData<Topic> topic) {
        this.topic = topic;
    }

    public MutableLiveData<List<Reply>> getReplies() {
        return replies;
    }

    public void setReplies(MutableLiveData<List<Reply>> replies) {
        this.replies = replies;
    }

    public MutableLiveData<Comment> getNewComment() {
        return newComment;
    }

    public void setNewComment(MutableLiveData<Comment> newComment) {
        this.newComment = newComment;
    }

    public MutableLiveData<Boolean> getCallApiResult() {
        return callApiResult;
    }

    public void setCallApiResult(MutableLiveData<Boolean> callApiResult) {
        this.callApiResult = callApiResult;
    }

    public MutableLiveData<Reply> getNewReply() {
        return newReply;
    }

    public void setNewReply(MutableLiveData<Reply> newReply) {
        this.newReply = newReply;
    }

    private void postErrorResponse(BaseErrorResponse response) {
        errorResponse.postValue(response);
    }

    public MutableLiveData<Boolean> getIsLastCommentLoaded() {
        return isLastCommentLoaded;
    }

    public MutableLiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public MutableLiveData<Boolean> getDeleteTopicResult() {
        return deleteTopicResult;
    }

    public void setDeleteTopicResult(MutableLiveData<Boolean> deleteTopicResult) {
        this.deleteTopicResult = deleteTopicResult;
    }

    public MutableLiveData<Integer> getDeletedCommentId() {
        return deletedCommentId;
    }

    public void setDeletedCommentId(MutableLiveData<Integer> deletedCommentId) {
        this.deletedCommentId = deletedCommentId;
    }

    public MutableLiveData<Integer> getDeletedReplyId() {
        return deletedReplyId;
    }

    public void setDeletedReplyId(MutableLiveData<Integer> deletedReplyId) {
        this.deletedReplyId = deletedReplyId;
    }

    public MutableLiveData<List<Comment>> getAddMoreComments() {
        return addMoreComments;
    }

    public void setAddMoreComments(MutableLiveData<List<Comment>> addMoreComments) {
        this.addMoreComments = addMoreComments;
    }
}
