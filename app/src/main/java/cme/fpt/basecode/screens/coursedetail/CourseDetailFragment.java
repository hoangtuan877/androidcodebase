package cme.fpt.basecode.screens.coursedetail;


import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cme.fpt.basecode.R;
import cme.fpt.basecode.common.ApiUrl;
import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.BaseSharedViewModel;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.common.util.DownloadUtil;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentCourseDetailBinding;
import cme.fpt.basecode.model.Document;
import cme.fpt.basecode.model.LessonResult;
import cme.fpt.basecode.screens.coursedetail.adapter.CourseDetailDocumentAdapter;
import cme.fpt.basecode.screens.coursedetail.adapter.CourseDetailLessonAdapter;
import cme.fpt.basecode.screens.coursedetail.adapter.RelatedCoursesAdapter;
import cme.fpt.basecode.screens.coursedetail.viewmodel.CourseDetailViewModel;
import cme.fpt.basecode.screens.coursedetail.webview.LessonWebview;
import cme.fpt.basecode.screens.main.MainSharedViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseDetailFragment extends Fragment implements View.OnClickListener,
        CourseDetailLessonAdapter.LessonAdapterPresenter, CourseDetailDocumentAdapter.DocumentAdapterPresenter {
    FragmentCourseDetailBinding detailBinding;
    CourseDetailViewModel courseDetailViewModel;
    boolean descriptionExpand = false;
    MainSharedViewModel uiViewModel;
    BaseSharedViewModel mErrorViewModel;
    LessonWebview mLessonWebview;
    CourseDetailLessonAdapter mCourseDetailLessonAdapter;
    CourseDetailDocumentAdapter mCourseDetailDocumentAdapter;
    String token;
    int courseId;
    String courseStatus;

    @Inject
    CourseDetailViewModel.CourseDetailFactory courseDetailFactory;

    public CourseDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        courseId = getArguments().getInt("courseId", 1);

        uiViewModel = ViewModelProviders.of(getActivity()).get(MainSharedViewModel.class);
        uiViewModel.setMenuItem(1);
        mErrorViewModel = ViewModelProviders.of(getActivity()).get(BaseSharedViewModel.class);

        token = getToken();

        courseDetailViewModel = ViewModelProviders.of(this, courseDetailFactory).get(CourseDetailViewModel.class);
        courseDetailViewModel.init(String.valueOf(courseId));
        detailBinding.setCourseDetailModel(courseDetailViewModel);
        CourseFragmentOnClickHandler handler = new CourseFragmentOnClickHandler();
        detailBinding.setPresenter(handler);


        // Course Lesson Handle
        detailBinding.courseDetailLessonCard.getRecycler().setLayoutManager(new LinearLayoutManager(getActivity()));
        mCourseDetailLessonAdapter = new CourseDetailLessonAdapter(R.layout.course_detail_lesson_item, new ArrayList<>());
        mCourseDetailLessonAdapter.setPresenter(this);
        detailBinding.courseDetailLessonCard.getRecycler().setAdapter(mCourseDetailLessonAdapter);
        detailBinding.courseDetailLessonCard.getFooter().setOnClickListener(view -> detailBinding.courseDetailLessonCard.expandBody());

        // Course document handle
        detailBinding.courseDetailDocumentsCard.getRecycler().setLayoutManager(new LinearLayoutManager(getActivity()));
        mCourseDetailDocumentAdapter = new CourseDetailDocumentAdapter(R.layout.course_detail_document_item, new ArrayList<>());
        mCourseDetailDocumentAdapter.setPresenter(this);
        detailBinding.courseDetailDocumentsCard.getRecycler().setAdapter(mCourseDetailDocumentAdapter);
        detailBinding.courseDetailDocumentsCard.getFooter().setOnClickListener(view -> detailBinding.courseDetailDocumentsCard.expandBody());
        detailBinding.btnEndLesson.setOnClickListener(v -> getActivity().onBackPressed());
        //Course Ratting Handle

        mLessonWebview = new LessonWebview(detailBinding.mainWebview);
        uiViewModel.getIsWebviewShow().observe(this, isShowWebview -> {
            if (isShowWebview) {
                showWebview();
            } else {
                hideWebview();
            }
        });

        courseDetailViewModel.getErrorResponse().observe(this, baseErrorResponse -> mErrorViewModel.getErrorResponse().postValue(baseErrorResponse));

        courseDetailViewModel.getLessonResult().observe(this, listData -> {
            mCourseDetailLessonAdapter.setData(listData);
            detailBinding.courseDetailLessonCard.setFooterVisibility(listData.size());
            String courseStatusValue = courseDetailViewModel.getTraineeStatus().getValue();
            if (isAllLessonFinished(listData) && courseStatusValue != null && !courseStatusValue.equals(Constant.COMPLETED_STATUS)) {
                courseDetailViewModel.updateTraineeStatus(Constant.COMPLETED_STATUS);
                createFinishCourseDialog();
            }
        });
        detailBinding.relatedCoursesRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        RelatedCoursesAdapter relatedCoursesAdapter = new RelatedCoursesAdapter(R.layout.custom_card_related_course, new ArrayList<>());
        detailBinding.relatedCoursesRecycler.setAdapter(relatedCoursesAdapter);

        courseDetailViewModel.getRelatedCourses().observe(this, listData -> relatedCoursesAdapter.setData(listData));


        courseDetailViewModel.getCourse().observe(this, course -> {
            if (course.getDocuments() != null) {
                detailBinding.courseDetailDocumentsCard.setVisibility(View.VISIBLE);
                detailBinding.courseDetailDocumentsCard.setFooterVisibility(course.getDocuments().size());
                mCourseDetailDocumentAdapter.setData(course.getDocuments());
            } else {
                detailBinding.courseDetailDocumentsCard.setVisibility(View.GONE);
                detailBinding.courseDetailDocumentsCard.setFooterVisibility(0);
            }
        });
        detailBinding.courseDetailRatingCardDetail.btnRateThisCourse.setOnClickListener(v -> showRatingDialog("", false));
        detailBinding.courseDetailCertificateThump.setOnClickListener(v -> onDownloadCertificate());

        ViewTreeObserver vto = detailBinding.courseDetailDescriptionCardDetail.descriptionContent.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(() -> {
            int line = detailBinding.courseDetailDescriptionCardDetail.descriptionContent.getLineCount();
            if (line <= Constant.COURSE_DESCRIPTION_LIMIT_LINE) {
                detailBinding.courseDetailDescriptionCardDetail.descriptionFooter.setVisibility(View.GONE);
            } else {
                detailBinding.courseDetailDescriptionCardDetail.descriptionFooter.setVisibility(View.VISIBLE);
            }
        });
    }

    private boolean isAllLessonFinished(List<LessonResult> listData) {
        int numberOfCompletedLesson = 0;
        for (int i = 0; i < listData.size(); i++) {
            LessonResult lesson = listData.get(i);
            if (lesson.getStatus().equals(Constant.COMPLETED_STATUS) || lesson.getStatus().equals(Constant.PASSED_STATUS)) {
                numberOfCompletedLesson++;
            }
        }
        return numberOfCompletedLesson >= listData.size();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        detailBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_course_detail, container, false);
        detailBinding.setLifecycleOwner(this);
        getActivity().setTitle(R.string.course_detail);
        detailBinding.courseDetailDescriptionCardDetail.descriptionFooter.setOnClickListener(this);
        return detailBinding.getRoot();
    }


    @Override
    public void onClick(View view) {

        int id = view.getId();
        switch (id) {
            case R.id.btnRateThisCourse:
                break;
            case R.id.description_footer:
                View descriptionContentWrapper = detailBinding.courseDetailDescriptionCardDetail.descriptionContentWrapper;
                if (!descriptionExpand) {
                    descriptionContentWrapper.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    detailBinding.courseDetailDescriptionCardDetail.descriptionFooterLabel.setText(R.string.show_less);
                } else {
                    descriptionContentWrapper.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.description_content_height);
                    detailBinding.courseDetailDescriptionCardDetail.descriptionFooterLabel.setText(R.string.show_more);
                }
                descriptionContentWrapper.requestLayout();
                descriptionExpand = !descriptionExpand;
                break;
            default:
                break;
        }
    }

    private void showRatingDialog(String message, boolean isRefreshCourseNeeded) {
        RatingBar ratingBarDialog;
        Button btnSubmitRating;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewDialog = this.getLayoutInflater().inflate(R.layout.rating_dialog, null);
        ratingBarDialog = viewDialog.findViewById(R.id.ratingBarDialog);
        btnSubmitRating = viewDialog.findViewById(R.id.btnSubmitRating);
        btnSubmitRating.setEnabled(false);
        btnSubmitRating.setTextColor(getResources().getColor(R.color.colorTextSub3, getActivity().getTheme()));
        Button btnCancelRating = viewDialog.findViewById(R.id.btnCancelRating);

        TextView txtMessage = viewDialog.findViewById(R.id.txtMessage);
        if (!TextUtils.isEmpty(message)) {
            txtMessage.setText(message);
            txtMessage.setVisibility(View.VISIBLE);
        } else {
            txtMessage.setVisibility(View.GONE);
        }

        builder.setView(viewDialog);
        if (isRefreshCourseNeeded) {
            builder.setOnDismissListener(dialogInterface -> courseDetailViewModel.init(String.valueOf(courseId)));
        }
        AlertDialog dialog = builder.create();
        addEventButtonDialog(dialog, ratingBarDialog, btnSubmitRating, btnCancelRating);
        dialog.show();
    }

    private void addEventButtonDialog(AlertDialog dialog, RatingBar ratingBarDialog,
                                      Button btnSubmit, Button btnCancel) {
        ratingBarDialog.setOnRatingBarChangeListener((ratingBar, v, b) -> {
            if (ratingBar.getRating() < 1.0f) {
                ratingBar.setRating(1.0f);
            }
            btnSubmit.setEnabled(true);
            btnSubmit.setTextColor(getResources().getColor(R.color.colorTxtActive, getActivity().getTheme()));
        });
        btnSubmit.setOnClickListener(v -> {
            courseDetailViewModel.submitRating(String.valueOf(ratingBarDialog.getRating()), String.valueOf(courseId));
            dialog.cancel();
        });
        btnCancel.setOnClickListener(v -> dialog.cancel());
    }

    @Override
    public void onLessonClick(View view, LessonResult lesson) {
        uiViewModel.setIsWebviewShow(true);
        mLessonWebview.init(lesson);
        mLessonWebview.loadUrl(bindingScormUrl(lesson));
    }

    private void showWebview() {
        detailBinding.webviewWrapper.setVisibility(View.VISIBLE);
        Animation slideIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_slide_in_left);
        detailBinding.webviewWrapper.startAnimation(slideIn);
        uiViewModel.setHideToolbar(true);

    }

    private void hideWebview() {
        Animation slideOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_slide_out_right);
        detailBinding.webviewWrapper.startAnimation(slideOut);
        detailBinding.webviewWrapper.setVisibility(View.GONE);
        uiViewModel.setHideToolbar(false);
        courseDetailViewModel.updateSelectedLesson(mLessonWebview.getLessonResult());
        mLessonWebview.loadUrl("");
    }

    @BindingAdapter({"courseStatus"})
    public static void updateCourseStatus(Button button, String courseStatus) {
        if (TextUtils.isEmpty(courseStatus)) {
            return;
        }

        String buttonText;
        int colorResource = R.color.colorBtnActive;
        switch (courseStatus) {
            case Constant.COMPLETED_STATUS:
                buttonText = button.getResources().getString(R.string.course_status_completed);
                button.setEnabled(false);
                break;
            case Constant.IN_PROGRESS_STATUS:
                buttonText = button.getResources().getString(R.string.course_status_inprogress);
                button.setEnabled(true);
                colorResource = R.color.colorButtonInProgress;
                break;
            case Constant.NONE_STATUS:
                buttonText = button.getResources().getString(R.string.course_status_open);
                button.setEnabled(false);
                colorResource = R.color.colorBtnInactive;
                break;
            case Constant.OPEN_STATUS:
            default:
                buttonText = button.getResources().getString(R.string.course_status_open);
                button.setEnabled(true);
                break;
        }
        button.setText(buttonText);
        button.setBackgroundColor(button.getContext().getColor(colorResource));
    }

    @Override
    public void onDownloadDocument(View view, Document document) {
        DownloadUtil.downloadData(getContext(), ApiUrl.DOC_URL, document.getPath(), document.getName(), document.getName(), courseDetailViewModel.getCourse().getValue().getName());
    }

    public void onDownloadCertificate() {
        String courseName = courseDetailViewModel.getCourse().getValue().getName();
        String certUrl = courseDetailViewModel.getRegistration().getValue().getCertificatePath();
        int index = certUrl.lastIndexOf(Constant.DASH);
        String fileName = certUrl.substring(index + 1);
        DownloadUtil.downloadData(getContext(), ApiUrl.DOC_URL, certUrl, fileName, courseName, courseName);
    }

    public class CourseFragmentOnClickHandler {
        public CourseFragmentOnClickHandler() {
            // Default constructor
        }

        private void disableEnrollButton() {
            detailBinding.button.setEnabled(false);
            detailBinding.button.setBackgroundColor(getActivity().getColor(R.color.coolGrey));
        }

        private void callEnrollAPI(CourseDetailViewModel course) {
            course.enrollCourse(course.getCourse().getValue().getId(), new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.code() == BaseResponse.SUCCESS_CODE || response.code() == BaseResponse.CODE_201) {
                        courseDetailViewModel.init(String.valueOf(courseId));
                        disableEnrollButton();
                    } else {
                        mErrorViewModel.getErrorResponse().postValue(new BaseErrorResponse(response.code(), response.message()));
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    mErrorViewModel.getErrorResponse().postValue(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
                }
            });
        }

        public void enrollButtonClick(CourseDetailViewModel courseVM) {
            String status = courseVM.getTraineeStatus().getValue();
            if (status == null) {
                return;
            }
            switch (status) {
                case Constant.OPEN_STATUS:
                    callEnrollAPI(courseVM);
                    break;
                case Constant.COMPLETED_STATUS:
                    callOpenFirstLesson(courseVM.getLessonResult().getValue());
                    break;
                case Constant.IN_PROGRESS_STATUS:
                    callOpenNextLesson(courseVM.getLessonResult().getValue());
                    break;
                default:
                    callEnrollAPI(courseVM);
                    break;
            }
        }

        private void callOpenFirstLesson(List<LessonResult> value) {
            onLessonClick(CourseDetailFragment.this.getView(), value.get(0));
        }

        private void callOpenNextLesson(List<LessonResult> value) {
            for (int i = 0; i < value.size(); i++) {
                String status = value.get(i).getStatus();
                if (!Constant.COMPLETED_STATUS.equals(status) && !Constant.PASSED_STATUS.equals(status) ) {
                    onLessonClick(CourseDetailFragment.this.getView(), value.get(i));
                    break;
                }
            }
        }
    }

    private void createFinishCourseDialog() {
        String courseName = courseDetailViewModel.getCourse().getValue().getName();
        String message = getString(R.string.course_completed_congrat) + " " + courseName;
        showRatingDialog(message, true);
    }

    private String getToken() {
        SharedPreferences prefFile = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        return prefFile.getString(Constant.USER_TOKEN, "");
    }

    public String bindingScormUrl(LessonResult lessonResult) {
        if (lessonResult == null) {
            return "";
        }
        SharedPreferences preferences = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        String lrsEndpoint = preferences.getString(Constant.LRS_ENDPOINT, "");
        String lrsBasicAuth = preferences.getString(Constant.LRS_BASIC_AUTH, "");
        String userName = preferences.getString(Constant.USERNAME, "");
        if (lrsEndpoint.isEmpty() || userName.isEmpty() || lrsBasicAuth.isEmpty()) {
            return "";
        }
        String actor = Constant.SCOM_CONTENT_ACTOR.replace("username", userName);
        return ApiUrl.S3_URL + "/" + lessonResult.getLesson().getIndexFilePath()
                + "?endpoint=" + lrsEndpoint + "&auth=Basic%20" + lrsBasicAuth +
                "&tincan=true&" + actor + "&activity_id=" + lessonResult.getLesson().getMainActivityId()
                + "&registration=" + lessonResult.getRegistration().getCode();
    }

}
