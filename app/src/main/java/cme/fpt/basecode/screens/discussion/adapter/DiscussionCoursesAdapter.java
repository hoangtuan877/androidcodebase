package cme.fpt.basecode.screens.discussion.adapter;


import android.view.View;

import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.R;
import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.Discussion;

public class DiscussionCoursesAdapter extends DataBindingAdapter {

    private List<Discussion> mListData = new ArrayList<>();
    private int layoutId;
    private DiscussionAdapterPresenter presenter;

    public DiscussionCoursesAdapter(int layoutId, List<Discussion> data) {
        this.layoutId = layoutId;
        this.mListData.addAll(data);
    }


    @Override
    protected Object getObjectForPosition(int position) {
        return mListData.get(position);
    }

    @Override
    protected Object getPresenter() {
        return this.presenter;
    }

    public void setPresenter(DiscussionAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        if (position == 0) {
            return R.layout.discussion_course_item_blank;
        } else if (position == 1) {
            return R.layout.discussion_course_item_filer;
        } else {
            return layoutId;
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public void setData(List<Discussion> data) {
        this.mListData = data;
        notifyDataSetChanged();
    }

    public interface DiscussionAdapterPresenter {
        void onCourseClick(View view, int courseId);
    }
}
