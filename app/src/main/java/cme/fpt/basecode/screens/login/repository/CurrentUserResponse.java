package cme.fpt.basecode.screens.login.repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import cme.fpt.basecode.model.ImageInfo;

public class CurrentUserResponse {
    @SerializedName("avatar")
    @Expose
    private ImageInfo avatar;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("staff")
    @Expose
    private boolean staff;

    public CurrentUserResponse() {
    }

    public CurrentUserResponse(ImageInfo avatar, String displayName, int id) {
        this.avatar = avatar;
        this.displayName = displayName;
        this.id = id;
    }

    public ImageInfo getAvatar() {
        return avatar;
    }

    public void setAvatar(ImageInfo avatar) {
        this.avatar = avatar;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStaff() {
        return staff;
    }

    public void setStaff(boolean staff) {
        this.staff = staff;
    }
}
