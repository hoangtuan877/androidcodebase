package cme.fpt.basecode.screens.discussion.repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.model.Topic;

public class TopicResponse {

    @SerializedName("content")
    @Expose
    private List<Topic> content;

    @SerializedName("discussion")
    @Expose
    private Discussion discussion;

    @SerializedName("first")
    @Expose
    private boolean first;

    @SerializedName("last")
    @Expose
    private boolean last;

    @SerializedName("number")
    @Expose
    private int number;

    @SerializedName("numberOfElements")
    @Expose
    private int numberOfElements;

    @SerializedName("size")
    @Expose
    private int size;

    @SerializedName("totalElements")
    @Expose
    private int totalElements;

    @SerializedName("totalPages")
    @Expose
    private int totalPages;

    public TopicResponse() {
    }

    public List<Topic> getContent() {
        return content;
    }

    public void setContent(List<Topic> content) {
        this.content = content;
    }

    public Discussion getDiscussion() {
        return discussion;
    }

    public void setDiscussion(Discussion discussion) {
        this.discussion = discussion;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
