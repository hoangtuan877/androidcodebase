package cme.fpt.basecode.screens.login.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.io.IOException;

import javax.inject.Singleton;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.screens.login.webapi.LoginWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class LoginRepository {
    private LoginWebService loginWebService;

    public LoginRepository(LoginWebService loginWebService) {
        this.loginWebService = loginWebService;
    }

    public LiveData<LoginResponse> login(String username, String password) {
        MutableLiveData<LoginResponse> data = new MutableLiveData<>();
        LoginRequest request = new LoginRequest(username, password);
        loginWebService.login(request).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                try {
                    if (response.code() != BaseResponse.SUCCESS_CODE) {
                        data.setValue(new LoginResponse(response.code(), response.errorBody().string().toString()));
                    } else {
                        LoginResponse result = response.body();
                        result.setResponseCode(response.code());
                        data.setValue(result);
                    }
                } catch (IOException e) {
                    LoginResponse loginResponse = new LoginResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR);
                    data.setValue(loginResponse);
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                LoginResponse loginResponse = new LoginResponse(BaseResponse.UNKNOWN_CODE, t.getMessage());
                data.setValue(loginResponse);
            }
        });
        return data;
    }
    public LiveData<String> getLRSEndpoint() {
        MutableLiveData<String> data = new MutableLiveData<>();
        loginWebService.getLRSEndpoint().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.code() == BaseResponse.SUCCESS_CODE) {
                    data.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Do nothing with this
            }
        });
        return data;
    }

    public LiveData<String> getLRSBasicAuthValue() {
        MutableLiveData<String> data = new MutableLiveData<>();
        loginWebService.getLRSBasicAuthVal().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                data.postValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Do nothing in this case.
            }
        });
        return data;
    }

    public LiveData<CurrentUserResponse> getCurrentUser() {
        MutableLiveData<CurrentUserResponse> data = new MutableLiveData<>();
        loginWebService.getCurrentUser().enqueue(new Callback<CurrentUserResponse>() {
            @Override
            public void onResponse(Call<CurrentUserResponse> call, Response<CurrentUserResponse> response) {
                if(response.code() == BaseResponse.SUCCESS_CODE) {
                    data.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<CurrentUserResponse> call, Throwable t) {
                // Do nothing in this case.
            }
        });
        return data;
    }
}
