package cme.fpt.basecode.screens.logout.repository;

import cme.fpt.basecode.common.BaseResponse;

public class LogoutResponse extends BaseResponse {

    public LogoutResponse(int code, String errorMessage) {
        super(code, errorMessage);
    }
}
