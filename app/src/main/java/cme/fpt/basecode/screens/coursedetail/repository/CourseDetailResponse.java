package cme.fpt.basecode.screens.coursedetail.repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.Course;
import cme.fpt.basecode.model.LessonResult;
import cme.fpt.basecode.model.RatingCourse;
import cme.fpt.basecode.model.Registration;

public class CourseDetailResponse extends BaseResponse {
    @SerializedName("beginLearnDate")
    @Expose
    private int beginLearnDate;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("completedDate")
    @Expose
    private int completedDate;

    @SerializedName("courseDTO")
    @Expose
    Course course;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("lessonResults")
    @Expose
    private List<LessonResult> lessonResults;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("ratingCourseDTOS")
    @Expose
    private RatingCourse ratingCourse;

    @SerializedName("registrationDTOS")
    @Expose
    private Registration registration;

    @SerializedName("traineeStatus")
    @Expose
    private String traineeStatus;

    @SerializedName("version")
    @Expose
    private int version;

    public CourseDetailResponse(){

    }
    public CourseDetailResponse(int code, String resultMessage){
        this.responseCode = code;
        this.errorMessage = resultMessage;
    }

    public int getBeginLearnDate() {
        return beginLearnDate;
    }

    public void setBeginLearnDate(int beginLearnDate) {
        this.beginLearnDate = beginLearnDate;
    }

    public String getCode(){
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(int completedDate) {
        this.completedDate = completedDate;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<LessonResult> getLessonResults() {
        return lessonResults;
    }

    public void setLessonResults(List<LessonResult> lessonResults) {
        this.lessonResults = lessonResults;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RatingCourse getRatingCourse() {
        return ratingCourse;
    }

    public void setRatingCourse(RatingCourse ratingCourse) {
        this.ratingCourse = ratingCourse;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public String getTraineeStatus() {
        return traineeStatus;
    }

    public void setTraineeStatus(String traineeStatus) {
        this.traineeStatus = traineeStatus;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
