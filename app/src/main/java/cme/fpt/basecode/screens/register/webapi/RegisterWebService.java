package cme.fpt.basecode.screens.register.webapi;


import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RegisterWebService {
    @Headers({
            "Content-Type: application/json",
            "JWT-TOKEN: authorized",
            "Request-type: mobile"
    })
    @POST("trainees/candidates/unsecured")
    Call<BaseResponse> registerUser(@Body User user);

}
