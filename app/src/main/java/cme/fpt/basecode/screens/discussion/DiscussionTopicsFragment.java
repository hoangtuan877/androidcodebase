package cme.fpt.basecode.screens.discussion;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.R;
import cme.fpt.basecode.common.BaseSharedViewModel;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentDiscussionTopicsBinding;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.adapter.DiscussionTopicsAdapter;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionTopicsViewModel;
import cme.fpt.basecode.screens.main.MainSharedViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscussionTopicsFragment extends Fragment implements ViewPager.OnPageChangeListener, DiscussionTopicsAdapter.DiscussionTopicsAdapterPresenter {
    FragmentDiscussionTopicsBinding discussionTopicsBinding;
    DiscussionTopicsViewModel topicsViewModel;
    MainSharedViewModel uiViewModel;
    BaseSharedViewModel mErrorViewModel;
    LinearLayoutManager mLinearLayoutManager;
    private boolean isLastPage = false;
    private boolean isLoadingDiscussionList = true;
    private int mDiscussionId;
    @Inject
    public DiscussionTopicsViewModel.DiscussionTopicsFactory discussionTopicsFactory;

    public DiscussionTopicsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDiscussionId = getArguments().getInt("discussionId", 0);

        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        uiViewModel = ViewModelProviders.of(getActivity()).get(MainSharedViewModel.class);
        uiViewModel.setMenuItem(4);
        mErrorViewModel = ViewModelProviders.of(getActivity()).get(BaseSharedViewModel.class);

        topicsViewModel = ViewModelProviders.of(this, discussionTopicsFactory).get(DiscussionTopicsViewModel.class);
        if (topicsViewModel.getTopics().getValue() != null){
            topicsViewModel.getTopics().getValue().clear();
        }
        topicsViewModel.init(mDiscussionId);

        discussionTopicsBinding.setTopicsViewModel(topicsViewModel);

        ViewPager viewPager = discussionTopicsBinding.discussionTopicsViewpager;
        List<Integer> pagesId = new ArrayList<>();
        pagesId.add(R.id.discussion_topic_list);
        pagesId.add(R.id.discussion_topic_info);

        DiscussionTopicsPageAdapter adapter = new DiscussionTopicsPageAdapter(getActivity(), pagesId);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);

        DiscussionTopicsAdapter topicsAdapter = new DiscussionTopicsAdapter(R.layout.discussion_course_topic_item, topicsViewModel.getTopics().getValue());
        topicsAdapter.setPresenter(this);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        discussionTopicsBinding.recycleCourseTopic.setLayoutManager(mLinearLayoutManager);
        discussionTopicsBinding.recycleCourseTopic.setHasFixedSize(true);
        discussionTopicsBinding.recycleCourseTopic.setAdapter(topicsAdapter);
        discussionTopicsBinding.recycleCourseTopic.addOnScrollListener(topicListOnScroll);
        /*Observer*/
        uiViewModel.getIsActionButtonClick().observe(this, isClick -> {
            if (isClick) {
                Bundle bundle = new Bundle();
                Topic topic = new Topic();
                topic.setParentId(mDiscussionId);
                bundle.putSerializable("topic", topic);
                Navigation.findNavController(DiscussionTopicsFragment.this.getView()).navigate(R.id.action_discussionTopicsFragment_to_discussionCreateNewFragment, bundle);
            }
        });

        topicsViewModel.getCurrentSection().observe(this, integer -> {
            if (integer != viewPager.getCurrentItem()) {
                viewPager.setCurrentItem(integer, false);
            }
        });

        topicsViewModel.getTopics().observe(this, topics -> {
            if(topics != null && !topics.isEmpty()) {
                ArrayList<Topic> listData = new ArrayList<>();
                listData.addAll(topics);
                topicsAdapter.setTopics(listData);
            }
            stopLoadingState();
        });

        topicsViewModel.getErrorResponse().observe(this, mErrorViewModel.getErrorResponse()::postValue);

        topicsViewModel.getIsLastTopicLoaded().observe(this, aBoolean -> this.isLastPage = aBoolean);

        topicsViewModel.getDiscussion().observe(this, discussion -> getActivity().setTitle(discussion.getName()));
        updateActionButtonState(true, getResources().getString(R.string.new_topic));
    }

    private void updateActionButtonState(boolean isShow, String btnText) {
        List<Object> actionButtonState = new ArrayList<>();
        actionButtonState.add(isShow);
        actionButtonState.add(btnText);
        uiViewModel.getButtonActionState().postValue(actionButtonState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        discussionTopicsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_discussion_topics, container, false);
        discussionTopicsBinding.setLifecycleOwner(this);
        getActivity().setTitle(R.string.discussion_screen_header);
        return discussionTopicsBinding.getRoot();
    }

    @Override
    public void onStop() {
        super.onStop();
        updateActionButtonState(false, "");
        uiViewModel.getIsActionButtonClick().postValue(false);
        topicsViewModel.getTopics().removeObservers(this);
    }

    @Override
    public void onTopicClick(View view, Topic topic) {
        saveIdCurrentTopic(topic.getOwner().getUserId(), "currentTopicOwner");
        Bundle bundle = new Bundle();
        bundle.putLong("topicId", topic.getId());
        Navigation.findNavController(view).navigate(R.id.action_discussionTopicsFragment_to_topicAnswerFragment, bundle);
    }

    private void startLoadingState () {
        discussionTopicsBinding.loadingMoreLayout.setVisibility(View.VISIBLE);
        isLoadingDiscussionList = true;
    }
    private void stopLoadingState () {
        discussionTopicsBinding.loadingMoreLayout.setVisibility(View.GONE);
        isLoadingDiscussionList = false;
    }
    class DiscussionTopicsPageAdapter extends PagerAdapter {

        List<Integer> resourceIds;
        Context context;

        public DiscussionTopicsPageAdapter(Context context, List<Integer> resourceIds) {
            this.resourceIds = resourceIds;
            this.context = context;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            return container.findViewById(resourceIds.get(position));
        }

        @Override
        public int getCount() {
            return resourceIds.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // Do nothing here
    }

    @Override
    public void onPageSelected(int position) {
        topicsViewModel.setCurrentSection(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // Do nothing here
    }

    private RecyclerView.OnScrollListener topicListOnScroll = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if(newState != 0) return;
            int lastCompleteItem = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
            if(!isLastPage && !isLoadingDiscussionList && lastCompleteItem == (topicsViewModel.getTopics().getValue().size()-1)) {
                new Handler().postDelayed(() -> {
                    int pageNum = topicsViewModel.getTopics().getValue().size()/ Constant.SIZE_DISCUSSION_COURSES;
                    topicsViewModel.getListTopicOnAPI(mDiscussionId, pageNum, Constant.SIZE_DISCUSSION_COURSES);
                }, 1000);
                startLoadingState();
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            // Method of OnScrollListener interface
        }
    };

    private void saveIdCurrentTopic(int id, String keyId){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Constant.APP_SHAREDPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(keyId, id);
        editor.commit();
    }
}
