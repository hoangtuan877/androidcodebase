package cme.fpt.basecode.screens.discussion.adapter;

import java.util.List;

import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.Reply;

public class DiscussionReplyAdapter extends DataBindingAdapter {

    List<Reply> replies;
    private final int layoutId;

    public DiscussionReplyAdapter(int layoutId, List<Reply> replies) {
        this.replies = replies;
        this.layoutId = layoutId;
    }

    @Override
    protected Object getObjectForPosition(int position) {
        return null;
    }

    @Override
    protected Object getPresenter() {
        return null;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public List<Reply> getReplies() {
        return replies;
    }

    public void setReplies(List<Reply> replies) {
        if (this.replies != null) {
            this.replies.clear();
            this.replies.addAll(replies);
        } else {
            this.replies = replies;
        }
        this.notifyDataSetChanged();
    }

    public int getLayoutId() {
        return layoutId;
    }
}
