package cme.fpt.basecode.screens.dashboard.webapi;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.model.DashboardDiscussionResponse;
import cme.fpt.basecode.model.StudyClass;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DashboardWebService {

    @GET("class/{id}")
    Call<StudyClass> getClassList(@Path("id") String id);

    @GET("lessons/status")
    Call<StudyClassResponse> getTopStudyClasses(@Query("page") int page, @Query("size") int size, @Query("status") String status, @Query("sort") String sort);

    @GET("notifications/me")
    Call<DashboardNotificationResponse> getTopNotifications(@Query("page") int page, @Query("size") int size, @Query("sort") String sort);

    @POST("notifications/seen")
    Call<BaseResponse> postSeenNotification(@Query("ids") int[] ids);

    @GET("trainee/topics/latest")
    Call<DashboardDiscussionResponse> getTopDiscussions(@Query("page") int page, @Query("size") int size, @Query("sort") String sort);
}
