package cme.fpt.basecode.screens.discussion.repository;

import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscussionRepository {

    private DiscussionWebService webService;
    MutableLiveData<List<Discussion>> listDiscussion = new MutableLiveData<>();
    MutableLiveData<Boolean> isLastDiscussionLoaded = new MutableLiveData<>();
    MutableLiveData<Topic> topic = new MutableLiveData<>();

    //ERROR HANDLER
    MutableLiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();

    public DiscussionRepository() {
    }

    public DiscussionRepository(DiscussionWebService webService) {
        this.webService = webService;
    }

    public void init(){
        getListDiscussionOnAPI(Constant.PAGE_DISCUSSION_COURSES, Constant.SIZE_DISCUSSION_COURSES);
    }

    public void getListDiscussionOnAPI(int pageNum, int itemCount) {
        webService.getDiscussions(pageNum, itemCount, null)
                .enqueue(new Callback<DiscussionResponse>() {
                    @Override
                    public void onResponse(Call<DiscussionResponse> call, Response<DiscussionResponse> response) {
                        if (response.code() == BaseResponse.SUCCESS_CODE && response.body() != null) {
                            if (response.body().getContent() != null) {
                                listDiscussion.postValue(response.body().getContent());
                            } else {
                                listDiscussion.postValue(new ArrayList<>());
                            }
                            isLastDiscussionLoaded.postValue(response.body().isLast());
                        } else {
                            postErrorResponse(new BaseErrorResponse(response.code(), response.message()));
                        }
                    }

                    @Override
                    public void onFailure(Call<DiscussionResponse> call, Throwable t) {
                        postErrorResponse(new BaseErrorResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
                    }
                });
    }

    public MutableLiveData<List<Discussion>> getListDiscussion() {
        return listDiscussion;
    }

    public MutableLiveData<Boolean> getIsLastDiscussionLoaded() {
        return isLastDiscussionLoaded;
    }

    public void setListDiscussion(MutableLiveData<List<Discussion>> listDiscussion) {
        this.listDiscussion = listDiscussion;
    }

    public void postErrorResponse(BaseErrorResponse response) {
        errorResponse.postValue(response);
    }

    public MutableLiveData<Topic> getTopic() {
        return topic;
    }

    public void setTopic(MutableLiveData<Topic> topic) {
        this.topic = topic;
    }
}
