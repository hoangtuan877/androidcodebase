package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.screens.discussion.repository.DiscussionRepository;

public class DiscussionCoursesViewModel extends ViewModel{

    DiscussionRepository discussionRepository;
    LiveData<List<Discussion>> listDiscussion = new MutableLiveData<>();
    LiveData<Boolean> isLastDiscussionLoaded = new MutableLiveData<>();

    public DiscussionCoursesViewModel(DiscussionRepository discussionRepository) {
        this.discussionRepository = discussionRepository;
    }

    public void init(){
        listDiscussion = this.discussionRepository.getListDiscussion();
        isLastDiscussionLoaded = this.discussionRepository.getIsLastDiscussionLoaded();
        this.discussionRepository.init();
    }
    public void getListDiscusstionOnAPI(int pageNum, int itemCount) {
        this.discussionRepository.getListDiscussionOnAPI(pageNum, itemCount);
    }
    public LiveData<List<Discussion>> getListDiscussion() {
        return listDiscussion;
    }

    public void setListDiscussion(LiveData<List<Discussion>> listDiscussion) {
        this.listDiscussion = listDiscussion;
    }

    public LiveData<Boolean> getIsLastDiscussionLoaded() {
        return isLastDiscussionLoaded;
    }

    public static class DiscussionCoursesFactory implements ViewModelProvider.Factory {

        private DiscussionRepository discussionRepository;

        public DiscussionCoursesFactory(DiscussionRepository discussionRepository) {
            this.discussionRepository = discussionRepository;
        }

        @NonNull
        @Override
        public DiscussionCoursesViewModel create(@NonNull Class modelClass) {
            return new DiscussionCoursesViewModel(discussionRepository);
        }
    }
}
