package cme.fpt.basecode.screens.userprofile.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import javax.inject.Singleton;

import cme.fpt.basecode.model.User;
import cme.fpt.basecode.screens.userprofile.webapi.UserWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class UserRepository {
    private UserWebService userWebService;

    public UserRepository(UserWebService userWebService) {
        this.userWebService = userWebService;
    }

    public LiveData<User> getUser(String userId) {
        MutableLiveData<User> data = new MutableLiveData<>();
        userWebService.getUserProfile(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                data.postValue(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // Do nothing in this case.
            }
        });
        return data;
    }


}
