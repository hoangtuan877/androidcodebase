package cme.fpt.basecode.screens.dashboard.adapter;


import android.view.View;

import java.util.List;

import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.StudyClass;

public class DashboardContinueAdapter extends DataBindingAdapter {

    List<StudyClass> studyClasses;
    private final int layoutId;
    private ContinueAdapterPresenter presenter;

    public DashboardContinueAdapter(int layoutId, List<StudyClass> studyClasses) {
        this.layoutId = layoutId;
        this.studyClasses = studyClasses;
    }


    @Override
    protected Object getObjectForPosition(int position) {
        return this.studyClasses.get(position);
    }

    @Override
    protected Object getPresenter() {
        return presenter;
    }

    public void setPresenter(ContinueAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return this.studyClasses.size();
    }

    public List<StudyClass> getStudyClasses() {
        return studyClasses;
    }

    public void setStudyClasses(List<StudyClass> studyClasses) {
        if (this.studyClasses != null) {
            this.studyClasses.clear();
            this.studyClasses.addAll(studyClasses);
        } else {
            this.studyClasses = studyClasses;
        }

        this.notifyDataSetChanged();
    }

    public interface ContinueAdapterPresenter {
        void onCourseClick(View view, int courseId);
    }
}
