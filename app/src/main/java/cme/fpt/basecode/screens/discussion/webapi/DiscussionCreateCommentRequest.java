package cme.fpt.basecode.screens.discussion.webapi;

public class DiscussionCreateCommentRequest {
    private String content;
    private int parentId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public DiscussionCreateCommentRequest() {
        // Default constructor
    }
}
