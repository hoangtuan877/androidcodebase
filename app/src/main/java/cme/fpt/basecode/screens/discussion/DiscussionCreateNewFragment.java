package cme.fpt.basecode.screens.discussion;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.R;
import cme.fpt.basecode.common.BaseSharedViewModel;
import cme.fpt.basecode.common.TextWatcherCounter;
import cme.fpt.basecode.common.TextWatcherCounterWithButton;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentDiscussionCreateNewBinding;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionCreateNewViewModel;
import cme.fpt.basecode.screens.main.MainSharedViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscussionCreateNewFragment extends Fragment {

    FragmentDiscussionCreateNewBinding discussionCreateNewBinding;
    MainSharedViewModel uiViewModel;
    DiscussionCreateNewViewModel newTopicViewModel;
    BaseSharedViewModel mErrorViewModel;

    @Inject
    public DiscussionCreateNewViewModel.DiscussionCreateNewFactory discussionCreateNewFactory;

    public DiscussionCreateNewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        uiViewModel = ViewModelProviders.of(getActivity()).get(MainSharedViewModel.class);
        uiViewModel.setMenuItem(4);
        mErrorViewModel = ViewModelProviders.of(getActivity()).get(BaseSharedViewModel.class);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        Topic topic = (Topic) getArguments().getSerializable("topic");
        if (topic == null) {
            topic = new Topic();
        }

        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        newTopicViewModel = ViewModelProviders.of(this, discussionCreateNewFactory).get(DiscussionCreateNewViewModel.class);
        newTopicViewModel.init(topic);
        discussionCreateNewBinding.setNewTopicViewModel(newTopicViewModel);

        TextWatcherCounter descriptionWatcher = new TextWatcherCounter(R.string.discussion_description_limit, discussionCreateNewBinding.discussionNewTopicDescriptionCounter);
        discussionCreateNewBinding.discussionNewTopicDescriptionCounter.setText(getResources().getString(R.string.discussion_description_limit, 0));

        TextWatcherCounterWithButton watcherCounterWithButton = new TextWatcherCounterWithButton(R.string.discussion_title_limit,
                discussionCreateNewBinding.discussionNewTopicNameCounter,
                discussionCreateNewBinding.discussionNewTopicConfirm, 10);
        discussionCreateNewBinding.discussionNewTopicNameCounter.setText(getResources().getString(R.string.discussion_title_limit, 0));

        discussionCreateNewBinding.discussionNewTopicDescription.addTextChangedListener(descriptionWatcher);
        discussionCreateNewBinding.discussionNewTopicName.addTextChangedListener(watcherCounterWithButton);

        discussionCreateNewBinding.discussionNewTopicConfirm.setOnClickListener(view -> {
            String title = discussionCreateNewBinding.discussionNewTopicName.getText().toString().trim();
            String description = discussionCreateNewBinding.discussionNewTopicDescription.getText().toString().trim();
            if (newTopicViewModel.getNewTopic().getValue().getId() == 0) {
                newTopicViewModel.createTopic(title, description);
            } else {
                newTopicViewModel.updateTopic(title, description);
            }
        });

        discussionCreateNewBinding.discussionNewTopicCancel.setOnClickListener(view -> Navigation.findNavController(view).popBackStack());

        newTopicViewModel.getIsCreateResult().observe(this, createResult -> {
            if (createResult) {
               Navigation.findNavController(getView()).popBackStack();
            }
        });

        newTopicViewModel.getErrorResponse().observe(this, baseErrorResponse -> mErrorViewModel.getErrorResponse().postValue(baseErrorResponse));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        discussionCreateNewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_discussion_create_new, container, false);
        discussionCreateNewBinding.setLifecycleOwner(this);
        getActivity().setTitle(R.string.create_new_topic_title);
        return discussionCreateNewBinding.getRoot();
    }

    @BindingAdapter({"updateButton"})
    public static void updateButtonText(Button button, int topicId){
        if (topicId == 0){
            button.setText(button.getResources().getString(R.string.create_new_topic));
        }else{
            button.setText(button.getResources().getString(R.string.update_topic));
        }
    }

}
