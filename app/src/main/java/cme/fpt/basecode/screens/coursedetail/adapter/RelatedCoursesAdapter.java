package cme.fpt.basecode.screens.coursedetail.adapter;

import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.RelatedCourseDTO;

public class RelatedCoursesAdapter extends DataBindingAdapter {

    List<RelatedCourseDTO> listCourse = new ArrayList<>();
    private final int layoutId;

    public RelatedCoursesAdapter(int layoutId, List<RelatedCourseDTO> listCourse) {
        this.layoutId = layoutId;
        this.listCourse.addAll(listCourse);
    }

    @Override
    protected Object getObjectForPosition(int position) {
        return  this.listCourse.get(position);
    }

    @Override
    protected Object getPresenter() {
        return null;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return listCourse.size();
    }

    public void setData(List<RelatedCourseDTO> data) {
        this.listCourse = data;
        notifyDataSetChanged();
    }
}
