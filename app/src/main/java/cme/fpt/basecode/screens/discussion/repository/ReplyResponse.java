package cme.fpt.basecode.screens.discussion.repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import cme.fpt.basecode.model.Reply;

public class ReplyResponse {

    @SerializedName("replies")
    @Expose
    private List<Reply> replies;

    @SerializedName("topicOwner")
    @Expose
    private int topicOwner;

    public ReplyResponse() {
    }

    public ReplyResponse(List<Reply> replies, int topicOwner) {
        this.replies = replies;
        this.topicOwner = topicOwner;
    }

    public List<Reply> getReplies() {
        return replies;
    }

    public void setReplies(List<Reply> replies) {
        this.replies = replies;
    }

    public int getTopicOwner() {
        return topicOwner;
    }

    public void setTopicOwner(int topicOwner) {
        this.topicOwner = topicOwner;
    }
}
