package cme.fpt.basecode.screens.coursedetail.adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cme.fpt.basecode.R;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.LessonResult;

public class CourseDetailLessonAdapter extends DataBindingAdapter {

    private List<LessonResult> mLessonResultList = new ArrayList<>();
    private int layoutId;
    private LessonAdapterPresenter presenter;

    public CourseDetailLessonAdapter(int layoutId, List<LessonResult> data) {
        this.layoutId = layoutId;
        this.mLessonResultList.addAll(data);

    }


    @Override
    protected Object getObjectForPosition(int position) {
        return mLessonResultList.get(position);
    }

    @Override
    protected Object getPresenter() {
        return this.presenter;
    }

    public void setPresenter(LessonAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return mLessonResultList.size();
    }

    public void setData(List<LessonResult> data) {
        this.mLessonResultList = data;
        notifyDataSetChanged();
    }
    @BindingAdapter("lessonStatus")
    public static void setLessonStatus(TextView view, String status) {
        Context context = view.getContext();
        switch (status) {
            case Constant.PASSED_STATUS:
                view.setText(context.getString(R.string.lesson_completed));
                break;
            case Constant.COMPLETED_STATUS:
                view.setText(context.getString(R.string.lesson_completed));
                break;
            case Constant.FAILED_STATUS:
                view.setText(context.getString(R.string.lesson_not_yet_started));
                break;
            case Constant.IN_PROGRESS_STATUS:
                view.setText(context.getString(R.string.lesson_not_yet_started));
                break;
            case Constant.NOT_STARTED_YET_STATUS:
                view.setText(context.getString(R.string.lesson_not_yet_started));
                break;
            default:
                view.setText(context.getString(R.string.lesson_not_yet_started));
                break;
        }
    }

    public interface LessonAdapterPresenter {
        void onLessonClick(View view, LessonResult lesson);
    }
}
