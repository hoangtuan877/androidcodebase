package cme.fpt.basecode.screens.discussion;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import cme.fpt.basecode.R;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.databinding.FragmentDiscussionMainBinding;
import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.screens.discussion.adapter.DiscussionCoursesAdapter;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionCoursesViewModel;

public class DiscussionMainFragment extends Fragment implements DiscussionCoursesAdapter.DiscussionAdapterPresenter {

    FragmentDiscussionMainBinding discussionMainBinding;
    DiscussionCoursesAdapter mDiscussionCoursesAdapter;
    DiscussionCoursesViewModel discussionCoursesViewModel;
    private float mCurrentTranslation = 0;
    @Inject
    DiscussionCoursesViewModel.DiscussionCoursesFactory discussionCoursesFactory;
    ArrayList<Discussion> mListDiscussion = new ArrayList<>();
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isLastPage = false;
    private boolean isLoadingDiscussionList = true;

    public DiscussionMainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        discussionMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_discussion_main, container, false);
        discussionMainBinding.setLifecycleOwner(this);
        getActivity().setTitle(R.string.discussion_screen_header);
        return discussionMainBinding.getRoot();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);

        discussionCoursesViewModel = ViewModelProviders.of(this, discussionCoursesFactory).get(DiscussionCoursesViewModel.class);
        if (mListDiscussion.isEmpty()) {
            //Empty item for banner and filter view
            mListDiscussion.add(new Discussion());
            mListDiscussion.add(new Discussion());
            discussionCoursesViewModel.init();
        } else {
            discussionMainBinding.headerWrapper.setTranslationY(mCurrentTranslation);
        }

        mDiscussionCoursesAdapter = new DiscussionCoursesAdapter(R.layout.discussion_course_item, mListDiscussion);
        mDiscussionCoursesAdapter.setPresenter(this);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        discussionMainBinding.coursesList.setLayoutManager(mLinearLayoutManager);
        discussionMainBinding.coursesList.setAdapter(mDiscussionCoursesAdapter);
        discussionMainBinding.coursesList.setHasFixedSize(true);

        discussionMainBinding.coursesList.addOnScrollListener(onHideShowBannerScroll);

        //get list discussion
        discussionCoursesViewModel.getListDiscussion().observe(this, listData -> {
            if (!mListDiscussion.containsAll(listData)) {
                mListDiscussion.addAll(listData);
                mDiscussionCoursesAdapter.setData(mListDiscussion);
            }
            if (listData != null && !listData.isEmpty()) {
                discussionMainBinding.textNull.setVisibility(View.GONE);
            } else {
                discussionMainBinding.textNull.setVisibility(View.VISIBLE);
            }
            stopLoadingState();
        });

        discussionCoursesViewModel.getIsLastDiscussionLoaded().observe(this, isLastPageScrolled -> this.isLastPage = isLastPageScrolled);
    }

    @Override
    public void onStop() {
        super.onStop();
        discussionCoursesViewModel.getIsLastDiscussionLoaded().removeObservers(this);
    }

    private RecyclerView.OnScrollListener onHideShowBannerScroll = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState != 0) return;
            int lastCompleteItem = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
            if (!isLastPage && !isLoadingDiscussionList && lastCompleteItem == (mListDiscussion.size() - 1)) {
                new Handler().postDelayed(() -> {
                    int pageNum = mListDiscussion.size() / Constant.SIZE_DISCUSSION_COURSES;
                    discussionCoursesViewModel.getListDiscusstionOnAPI(pageNum, Constant.SIZE_DISCUSSION_COURSES);
                }, 1000);
                startLoadingState();
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            float translation = discussionMainBinding.headerWrapper.getTranslationY();
            int bannerHeight = discussionMainBinding.discussionBanner.getHeight();
            int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

            if (dy > 0 && translation > -bannerHeight) {
                if (translation - dy < -bannerHeight) {
                    mCurrentTranslation = -bannerHeight;
                } else {
                    mCurrentTranslation = translation - dy;
                }
                discussionMainBinding.headerWrapper.setTranslationY(mCurrentTranslation);
            } else if (dy < 0 && translation < 0 && firstVisibleItem == 0) {
                if (translation - dy > 0) {
                    mCurrentTranslation = 0;
                } else {
                    mCurrentTranslation = translation - dy;
                }
                discussionMainBinding.headerWrapper.setTranslationY(mCurrentTranslation);
            }
        }
    };

    private void startLoadingState() {
        discussionMainBinding.loadingMoreLayout.setVisibility(View.VISIBLE);
        isLoadingDiscussionList = true;
    }

    private void stopLoadingState() {
        discussionMainBinding.loadingMoreLayout.setVisibility(View.GONE);
        isLoadingDiscussionList = false;
    }

    @Override
    public void onCourseClick(View view, int discussionId) {
        Bundle bundle = new Bundle();
        bundle.putInt("discussionId", discussionId);
        Navigation.findNavController(view).navigate(R.id.action_discussionMainFragment_to_discussionTopicsFragment, bundle);
    }
}
