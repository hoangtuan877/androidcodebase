package cme.fpt.basecode.screens.discussion.webapi;

public class DiscussionCreateTopicRequest {
    String content;
    long parentId;
    String title;

    public DiscussionCreateTopicRequest() {
    }

    public DiscussionCreateTopicRequest( long parentId, String title, String content) {
        this.content = content;
        this.parentId = parentId;
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
