package cme.fpt.basecode.screens.discussion.adapter;

public class TopicAdapterItem {
    Object data;
    boolean isExpandedChildren;
    boolean isShowMore;
    boolean isExpandedByClick;

    public TopicAdapterItem(Object data, boolean isExpandedChildren, boolean isShowMore) {
        this.data = data;
        this.isExpandedChildren = isExpandedChildren;
        this.isShowMore = isShowMore;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isExpandedChildren() {
        return isExpandedChildren;
    }

    public void setExpandedChildren(boolean expandedChildren) {
        isExpandedChildren = expandedChildren;
    }

    public boolean isShowMore() {
        return this.isShowMore;
    }

    public void setShowMore(boolean isShowMore) {
        this.isShowMore = isShowMore;
    }

    public boolean isExpandedByClick() {
        return isExpandedByClick;
    }

    public void setExpandedByClick(boolean expabdedByClick) {
        this.isExpandedByClick = expabdedByClick;
    }
}
