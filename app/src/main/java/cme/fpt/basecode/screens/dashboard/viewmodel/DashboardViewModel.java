package cme.fpt.basecode.screens.dashboard.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.model.Content;
import cme.fpt.basecode.model.Notification;
import cme.fpt.basecode.model.StudyClass;
import cme.fpt.basecode.screens.dashboard.repository.DashboardRepository;

public class DashboardViewModel extends ViewModel {

    LiveData<List<StudyClass>> studyClasses = new MutableLiveData<>();
    LiveData<List<Notification>> notifications = new MutableLiveData<>();
    LiveData<List<Content>> listContent = new MutableLiveData<>();


    LiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();

    private DashboardRepository dashboardRepository;

    public DashboardViewModel(DashboardRepository dashboardRepository) {
        this.dashboardRepository = dashboardRepository;
    }

    public void init() {
        studyClasses = dashboardRepository.getListStudyClass();
        notifications = dashboardRepository.getListNotification();
        errorResponse = dashboardRepository.getErrorResponse();
        listContent = dashboardRepository.getmListContent();
        dashboardRepository.init();
    }

    public LiveData<List<StudyClass>> getStudyClasses() {
        return studyClasses;
    }

    public LiveData<List<Notification>> getNotifications() {
        return notifications;
    }

    public LiveData<List<Content>> getListContent() {
        return listContent;
    }

    public void setListContent(LiveData<List<Content>> listContent) {
        this.listContent = listContent;
    }

    public LiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(LiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public void seenNotification(int[] ids) {
        this.dashboardRepository.seenNotification(ids);
    }

    public static class DashboardFactory implements ViewModelProvider.Factory {

        private DashboardRepository dashboardRepository;

        public DashboardFactory(DashboardRepository dashboardRepository) {
            this.dashboardRepository = dashboardRepository;
        }

        @NonNull
        @Override
        public DashboardViewModel create(@NonNull Class modelClass) {
            return new DashboardViewModel(dashboardRepository);
        }
    }
}
