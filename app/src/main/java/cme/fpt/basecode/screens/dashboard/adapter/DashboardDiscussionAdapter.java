package cme.fpt.basecode.screens.dashboard.adapter;


import android.view.View;

import java.util.List;

import cme.fpt.basecode.common.DataBindingAdapter;
import cme.fpt.basecode.model.Content;

public class DashboardDiscussionAdapter extends DataBindingAdapter {

    List<Content> discussions;
    private final int layoutId;
    private DiscussionAdapterPresenter presenter;

    public DashboardDiscussionAdapter(int layoutId, List<Content> discussions) {
        this.layoutId = layoutId;
        this.discussions = discussions;
    }

    @Override
    protected Object getObjectForPosition(int position) {
        return this.discussions.get(position);
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    protected Object getPresenter() {
        return presenter;
    }

    public void setPresenter(DiscussionAdapterPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public int getItemCount() {
        return this.discussions.size();
    }

    public List<Content> getDiscussions() {
        return discussions;
    }

    public void setDiscussions(List<Content> discussions) {
        if (this.discussions != null) {
            this.discussions.clear();
            this.discussions.addAll(discussions);
        } else {
            this.discussions = discussions;
        }

        this.notifyDataSetChanged();
    }

    public interface DiscussionAdapterPresenter {
        void onDiscussionClick(View view, Content content);
    }
}
