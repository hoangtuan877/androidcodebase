package cme.fpt.basecode.screens.register.respository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.io.IOException;

import javax.inject.Singleton;

import cme.fpt.basecode.common.BaseResponse;
import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.model.User;
import cme.fpt.basecode.screens.register.webapi.RegisterWebService;
import retrofit2.Call;
import retrofit2.Callback;

@Singleton
public class RegisterRepository {
    private RegisterWebService webService;
    static String TAG = "RegisterRepository";

    public RegisterRepository(RegisterWebService userWebService) {
        this.webService = userWebService;
    }

    public LiveData<BaseResponse> registerUser(User user) {
        MutableLiveData<BaseResponse> data = new MutableLiveData<>();
        webService.registerUser(user).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                if(response.code() != BaseResponse.SUCCESS_CODE) {
                    try {
                        data.postValue(new BaseResponse(response.code(), response.errorBody().string().toString()));
                    } catch (IOException e) {
                        data.postValue(new BaseResponse(BaseResponse.UNKNOWN_CODE, Constant.SERVER_ERROR));
                    }
                } else {
                    data.postValue(new BaseResponse(response.code(), response.message()));
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e(TAG, "Error: " + t.getMessage());
                data.postValue(new BaseResponse(BaseResponse.UNKNOWN_CODE, t.getMessage()));
            }
        });
        return data;
    }
}
