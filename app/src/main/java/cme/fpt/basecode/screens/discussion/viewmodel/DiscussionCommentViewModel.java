package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import cme.fpt.basecode.common.BaseErrorResponse;
import cme.fpt.basecode.model.Comment;
import cme.fpt.basecode.model.Owner;
import cme.fpt.basecode.model.Reply;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.repository.DiscussionCommentRepository;

public class DiscussionCommentViewModel extends ViewModel {
    DiscussionCommentRepository discussionCommentRepository;
    MutableLiveData<Topic> topic = new MutableLiveData<>();

    MutableLiveData<Comment> newComment = new MutableLiveData<>();
    MutableLiveData<Reply> newReply = new MutableLiveData<>();

    MutableLiveData<Boolean> callAPIResult = new MutableLiveData<>();
    MutableLiveData<Boolean> deleteTopicResult = new MutableLiveData<>();
    MutableLiveData<Integer> deletedCommentId = new MutableLiveData<>();
    MutableLiveData<Integer> deletedReplyId = new MutableLiveData<>();

    MutableLiveData<List<Comment>> addMoreComments = new MutableLiveData<>();
    MutableLiveData<Boolean> isLastCommentLoaded = new MutableLiveData<>();

    MutableLiveData<List<Reply>> replies = new MutableLiveData<>();

    LiveData<BaseErrorResponse> errorResponse = new MutableLiveData<>();

    MutableLiveData<Owner> owner = new MutableLiveData<>();

    public DiscussionCommentViewModel(DiscussionCommentRepository discussionCommentRepository) {
        this.discussionCommentRepository = discussionCommentRepository;
        newComment.postValue(new Comment());
    }

    public void init(long topicId) {
        this.discussionCommentRepository.init(topicId);
        topic = this.discussionCommentRepository.getTopic();
        addMoreComments = this.discussionCommentRepository.getAddMoreComments();
        errorResponse = this.discussionCommentRepository.getErrorResponse();
        isLastCommentLoaded = this.discussionCommentRepository.getIsLastCommentLoaded();
        callAPIResult = this.discussionCommentRepository.getCallApiResult();
        newComment = this.discussionCommentRepository.getNewComment();
        newReply = this.discussionCommentRepository.getNewReply();
        replies = this.discussionCommentRepository.getReplies();
        deletedCommentId = this.discussionCommentRepository.getDeletedCommentId();
        deletedReplyId = this.discussionCommentRepository.getDeletedReplyId();
        deleteTopicResult = this.discussionCommentRepository.getDeleteTopicResult();
    }

    public void clean(){
        this.discussionCommentRepository.getIsLastCommentLoaded().setValue(false);
        this.discussionCommentRepository.getCallApiResult().setValue(false);
        this.discussionCommentRepository.getDeletedCommentId().setValue(0);
        this.discussionCommentRepository.getDeletedReplyId().setValue(0);
        this.discussionCommentRepository.getDeleteTopicResult().setValue(false);
    }

    public void getReplyByComment(int commentId) {
        this.discussionCommentRepository.getReplyByComment(commentId);
    }

    public void createComment(Comment comment) {
        this.discussionCommentRepository.createComment(comment);
    }

    public void updateComment(Comment comment){
        this.discussionCommentRepository.updateComment(comment);
    }

    public void deleteComment(int commentId){
        this.discussionCommentRepository.deleteComment(commentId);
    }

    public void callUpdateCommentAPI(long topicId, int pageNum, int pageSize) {
        this.discussionCommentRepository.updateCommentList(topicId, pageNum, pageSize);
    }

    public void createReply(Reply reply){
        this.discussionCommentRepository.createReply(reply);
    }

    public void updateReply(Reply reply){
        this.discussionCommentRepository.updateReply(reply);
    }

    public void deleteReply(int replyId){
        this.discussionCommentRepository.deleteReply(replyId);
    }

    public void deleteTopic(){
        this.discussionCommentRepository.deleteTopic();
    }

    public static class DiscussionCommentFactory implements ViewModelProvider.Factory {

        private DiscussionCommentRepository discussionCommentRepository;

        public DiscussionCommentFactory(DiscussionCommentRepository discussionCommentRepository) {
            this.discussionCommentRepository = discussionCommentRepository;
        }

        @NonNull
        @Override
        public DiscussionCommentViewModel create(@NonNull Class modelClass) {
            return new DiscussionCommentViewModel(discussionCommentRepository);
        }
    }

    public MutableLiveData<Topic> getTopic() {
        return topic;
    }

    public void setTopic(MutableLiveData<Topic> topic) {
        this.topic = topic;
    }

    public MutableLiveData<List<Comment>> getAddMoreComments() {
        return addMoreComments;
    }

    public void setAddMoreComments(MutableLiveData<List<Comment>> addMoreComments) {
        this.addMoreComments = addMoreComments;
    }

    public MutableLiveData<List<Reply>> getReplies() {
        return replies;
    }

    public void setReplies(MutableLiveData<List<Reply>> replies) {
        this.replies = replies;
    }

    public MutableLiveData<Comment> getNewComment() {
        return newComment;
    }

    public void setNewComment(MutableLiveData<Comment> newComment) {
        this.newComment = newComment;
    }

    public MutableLiveData<Boolean> getCallAPIResult() {
        return callAPIResult;
    }

    public MutableLiveData<Reply> getNewReply() {
        return newReply;
    }

    public void setNewReply(MutableLiveData<Reply> newReply) {
        this.newReply = newReply;
    }

    public void setCallAPIResult(MutableLiveData<Boolean> callAPIResult) {
        this.callAPIResult = callAPIResult;
    }

    public LiveData<BaseErrorResponse> getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(LiveData<BaseErrorResponse> errorResponse) {
        this.errorResponse = errorResponse;
    }

    public MutableLiveData<Owner> getOwner() {
        return owner;
    }

    public void setOwner(Owner ownerData) {
        this.owner.postValue(ownerData);
    }

    public MutableLiveData<Integer> getDeletedCommentId() {
        return deletedCommentId;
    }

    public void setDeletedCommentId(MutableLiveData<Integer> deletedCommentId) {
        this.deletedCommentId = deletedCommentId;
    }

    public MutableLiveData<Integer> getDeletedReplyId() {
        return deletedReplyId;
    }

    public void setDeletedReplyId(MutableLiveData<Integer> deletedReplyId) {
        this.deletedReplyId = deletedReplyId;
    }

    public MutableLiveData<Boolean> getIsLastCommentLoaded() {
        return isLastCommentLoaded;
    }

    public MutableLiveData<Boolean> getDeleteTopicResult() {
        return deleteTopicResult;
    }

    public void setDeleteTopicResult(MutableLiveData<Boolean> deleteTopicResult) {
        this.deleteTopicResult = deleteTopicResult;
    }
}
