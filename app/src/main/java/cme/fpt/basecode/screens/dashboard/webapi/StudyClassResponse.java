package cme.fpt.basecode.screens.dashboard.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import cme.fpt.basecode.model.StudyClass;

public class StudyClassResponse {
    @SerializedName("content")
    @Expose
    private List<StudyClass> listStudyClass;


    public List<StudyClass> getListStudyClass() {
        return listStudyClass;
    }

}
