package cme.fpt.basecode;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import androidx.navigation.NavController;
import androidx.navigation.NavHost;
import androidx.navigation.Navigation;

public class LoginActivity extends AppCompatActivity implements NavHost {

    public static final int REGISTER_REQUEST = 100;
    public static final String RESULT_BUNDLE = "RESULT_BUNDLE";
    public static final String USERNAME_INTENT = "username";
    public static final String PASWD_INTENT = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @NonNull
    @Override
    public NavController getNavController() {
        return Navigation.findNavController(this, R.id.nav_login_fragment);
    }
}
