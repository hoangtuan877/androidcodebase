package cme.fpt.basecode.dagger.component;

import javax.inject.Singleton;

import cme.fpt.basecode.MainActivity;
import cme.fpt.basecode.RegisterActivity;
import cme.fpt.basecode.dagger.DaggerApplication;
import cme.fpt.basecode.dagger.module.ApplicationModule;
import cme.fpt.basecode.dagger.module.NetModule;
import cme.fpt.basecode.screens.coursedetail.CourseDetailFragment;
import cme.fpt.basecode.screens.dashboard.DashboardFragment;
import cme.fpt.basecode.screens.discussion.DiscussionCreateNewFragment;
import cme.fpt.basecode.screens.discussion.DiscussionMainFragment;
import cme.fpt.basecode.screens.discussion.DiscussionTopicsFragment;
import cme.fpt.basecode.screens.discussion.TopicAnswerFragment;
import cme.fpt.basecode.screens.login.LoginFragment;
import cme.fpt.basecode.screens.logout.LogoutFragment;
import cme.fpt.basecode.screens.userprofile.UserProfileFragment;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface AppComponent {

    void inject(DaggerApplication daggerApplication);

    void inject(MainActivity activity);

    void inject(UserProfileFragment userProfileFragment);

    void inject(DashboardFragment dashboardFragment);

    void inject(LoginFragment loginFragment);

    void inject(LogoutFragment logoutFragment);

    void inject(CourseDetailFragment courseDetailFragment);

    void inject(RegisterActivity registerActivity);

    void inject(DiscussionTopicsFragment discussionTopicsFragment);

    void inject(DiscussionMainFragment discussionMainFragment);

    void inject(TopicAnswerFragment topicAnswerFragment);

    void inject(DiscussionCreateNewFragment discussionCreateNewFragment);
}
