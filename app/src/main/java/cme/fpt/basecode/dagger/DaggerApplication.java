package cme.fpt.basecode.dagger;

import android.app.Application;

import cme.fpt.basecode.common.ApiUrl;
import cme.fpt.basecode.dagger.component.AppComponent;
import cme.fpt.basecode.dagger.component.DaggerAppComponent;
import cme.fpt.basecode.dagger.module.ApplicationModule;
import cme.fpt.basecode.dagger.module.NetModule;

public class DaggerApplication extends Application{

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initNetComponent();
    }

    private void initNetComponent() {
        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .netModule(new NetModule(ApiUrl.BASE_URL))
                .build();
        appComponent.inject(this);

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
