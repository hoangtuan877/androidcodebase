package cme.fpt.basecode.dagger.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;

import javax.inject.Singleton;

import cme.fpt.basecode.common.Constant;
import cme.fpt.basecode.screens.coursedetail.repository.CourseDetailRepository;
import cme.fpt.basecode.screens.coursedetail.viewmodel.CourseDetailViewModel;
import cme.fpt.basecode.screens.coursedetail.webapi.CourseDetailWebService;
import cme.fpt.basecode.screens.dashboard.repository.DashboardRepository;
import cme.fpt.basecode.screens.dashboard.viewmodel.DashboardViewModel;
import cme.fpt.basecode.screens.dashboard.webapi.DashboardWebService;
import cme.fpt.basecode.screens.discussion.repository.DiscussionCommentRepository;
import cme.fpt.basecode.screens.discussion.repository.DiscussionRepository;
import cme.fpt.basecode.screens.discussion.repository.DiscussionTopicsRepository;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionCommentViewModel;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionCoursesViewModel;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionCreateNewViewModel;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionTopicsViewModel;
import cme.fpt.basecode.screens.discussion.viewmodel.DiscussionViewModel;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import cme.fpt.basecode.screens.login.repository.LoginRepository;
import cme.fpt.basecode.screens.login.viewmodel.LoginViewModel;
import cme.fpt.basecode.screens.login.webapi.LoginWebService;
import cme.fpt.basecode.screens.logout.repository.LogoutRepository;
import cme.fpt.basecode.screens.logout.webapi.LogoutWebService;
import cme.fpt.basecode.screens.register.respository.RegisterRepository;
import cme.fpt.basecode.screens.register.viewmodel.RegisterViewModel;
import cme.fpt.basecode.screens.register.webapi.RegisterWebService;
import cme.fpt.basecode.screens.userprofile.repository.UserRepository;
import cme.fpt.basecode.screens.userprofile.viewmodel.UserProfileViewModel;
import cme.fpt.basecode.screens.userprofile.webapi.UserWebService;
import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/***
 * To register dagger2 for an repository we need to provide Retrofit and Web Service.
 * So, you have to implement provideXXXWebService and provideXXXRepository method.
 */
@Module
public class NetModule {
    String mBaseUrl;

    public NetModule(String mBaseUrl) {
        this.mBaseUrl = mBaseUrl;
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient(Application application) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        SharedPreferences preferences = application.getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                String token = preferences.getString(Constant.USER_TOKEN, "");
                Log.e("TOKEN", token);
                Request.Builder reqBuilder = chain.request().newBuilder();
                reqBuilder.addHeader("Content-Type", "application/json").addHeader("Request-Type", "mobile");
                Request request;
                if (!TextUtils.isEmpty(token)) {
                    request = reqBuilder.addHeader("JWT-TOKEN", token).build();
                } else {
                    request = reqBuilder.build();
                }

                Response response = chain.proceed(request);
                String newToken = response.headers().get("JWT-TOKEN");
                if (!TextUtils.isEmpty(newToken)) {
                    preferences.edit().putString(Constant.USER_TOKEN, newToken).commit();
                }

                return response;
            }
        });
        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    public UserWebService provideUserWebService(Retrofit retrofit) {
        return retrofit.create(UserWebService.class);
    }

    @Provides
    public UserRepository provideUserRepository(UserWebService userWebService) {
        return new UserRepository(userWebService);
    }

    @Provides
    UserProfileViewModel.UserProfileFactory provideUserProfileViewModelFactory(UserRepository userRepository) {
        return new UserProfileViewModel.UserProfileFactory(userRepository);
    }

    @Provides
    public DashboardWebService provideDashboardWebService(Retrofit retrofit) {
        return retrofit.create(DashboardWebService.class);
    }

    @Provides
    public DashboardRepository provideDashboardRepository(DashboardWebService dashboardWebService) {
        return new DashboardRepository(dashboardWebService);
    }

    @Provides
    DashboardViewModel.DashboardFactory provideDashboardViewModuleFactory(DashboardRepository dashboardRepository) {
        return new DashboardViewModel.DashboardFactory(dashboardRepository);
    }

    @Provides
    public LoginWebService provideLoginWebService(Retrofit retrofit) {
        return retrofit.create(LoginWebService.class);
    }

    @Provides
    public LoginRepository provideLoginRepository(LoginWebService loginWebService) {
        return new LoginRepository(loginWebService);
    }

    @Provides
    LoginViewModel.LoginFactory provideLoginViewModuleFactory(LoginRepository loginRepository) {
        return new LoginViewModel.LoginFactory(loginRepository);
    }

    @Provides
    public RegisterWebService provideRegisterWebService(Retrofit retrofit) {
        return retrofit.create(RegisterWebService.class);
    }

    @Provides
    public RegisterRepository provideRegisterRepository(RegisterWebService registerWebService) {
        return new RegisterRepository(registerWebService);
    }

    @Provides
    RegisterViewModel.RegisterFactory provideRegisterViewModuleFactory(RegisterRepository registerRepository) {
        return new RegisterViewModel.RegisterFactory(registerRepository);
    }


    @Provides
    public LogoutWebService provideLogoutWebService(Retrofit retrofit) {
        return retrofit.create(LogoutWebService.class);
    }

    @Provides
    public LogoutRepository provideLogoutRepository(LogoutWebService logoutWebService) {
        return new LogoutRepository(logoutWebService);
    }

    @Provides
    public CourseDetailRepository provideCourseDetailRepository(CourseDetailWebService courseDetailWebService) {
        return new CourseDetailRepository(courseDetailWebService);
    }

    @Provides
    public CourseDetailWebService provideCourseDetailWebService(Retrofit retrofit) {
        return retrofit.create(CourseDetailWebService.class);
    }

    @Provides
    CourseDetailViewModel.CourseDetailFactory provideCourseDetailViewModuleFactory(CourseDetailRepository courseDetailRepository) {
        return new CourseDetailViewModel.CourseDetailFactory(courseDetailRepository);
    }

    @Provides
    public DiscussionRepository provideDiscussionRepository(DiscussionWebService discussionWebService) {
        return new DiscussionRepository(discussionWebService);
    }

    @Provides
    public DiscussionWebService provideDiscussionWebService(Retrofit retrofit) {
        return retrofit.create(DiscussionWebService.class);
    }

    @Provides
    public DiscussionViewModel.DiscussionFactory provideDiscussionViewModuleFactory(DiscussionRepository discussionRepository) {
        return new DiscussionViewModel.DiscussionFactory(discussionRepository);
    }

    @Provides
    DiscussionCoursesViewModel.DiscussionCoursesFactory provideDiscussionCoursesViewModuleFactory(DiscussionRepository discussionRepository) {
        return new DiscussionCoursesViewModel.DiscussionCoursesFactory(discussionRepository);
    }

    @Provides
    public DiscussionTopicsRepository provideDiscussionTopicsRepository(DiscussionWebService discussionWebService) {
        return new DiscussionTopicsRepository(discussionWebService);
    }

    @Provides
    public DiscussionTopicsViewModel.DiscussionTopicsFactory provideDiscussionTopicsViewModuleFactory(DiscussionTopicsRepository discussionTopicsRepository) {
        return new DiscussionTopicsViewModel.DiscussionTopicsFactory(discussionTopicsRepository);
    }

    //trang cuoi
    @Provides
    public DiscussionCommentRepository provideDiscussionCommentRepository(DiscussionWebService discussionWebService) {
        return new DiscussionCommentRepository(discussionWebService);
    }

    @Provides
    public DiscussionCommentViewModel.DiscussionCommentFactory provideDiscussionCommentViewModuleFactory(DiscussionCommentRepository discussionCommentRepository) {
        return new DiscussionCommentViewModel.DiscussionCommentFactory(discussionCommentRepository);
    }

    @Provides
    public DiscussionCreateNewViewModel.DiscussionCreateNewFactory provideDiscussionTopicsRepositoryFactory(DiscussionTopicsRepository discussionTopicsRepository) {
        return new DiscussionCreateNewViewModel.DiscussionCreateNewFactory(discussionTopicsRepository);
    }

}
