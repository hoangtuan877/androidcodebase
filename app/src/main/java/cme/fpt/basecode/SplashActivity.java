package cme.fpt.basecode;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import cme.fpt.basecode.common.Constant;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class SplashActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static int REQUEST_CODE = 10001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        String[] permisssions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        boolean checkResult = true;
        for (int i = 0; i < permisssions.length; i++) {
            if (ContextCompat.checkSelfPermission(this, permisssions[i]) != PackageManager.PERMISSION_GRANTED) {
                checkResult = false;
            }
        }
        if (!checkResult) {
            ActivityCompat.requestPermissions(this,
                    permisssions, REQUEST_CODE);
        } else {
            checkTokenLogin();
        }
    }

    private void checkTokenLogin() {
        SharedPreferences prefFile = getSharedPreferences(Constant.APP_SHAREDPREFERENCES, MODE_PRIVATE);
        if (prefFile.getString(Constant.USER_TOKEN, "").isEmpty()) {
            showActivity(LoginActivity.class);
        } else {
            showActivity(MainActivity.class);
        }
    }

    private void showActivity(Class activity) {
        Intent intent = new Intent(SplashActivity.this, activity);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        checkTokenLogin();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        } else {
            finishAndRemoveTask();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

}
