package cme.fpt.basecode;

import android.net.Uri;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

public class JsonUtil {
    public String getJson(String path) {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(path);
        return JsonUtil.convertByteToString(inputStream);
    }

    public static String convertByteToString(InputStream inputStream) {
        char[] buffer = new char[10240];
        StringBuilder out = new StringBuilder();

        try (Reader in = new InputStreamReader(inputStream, "UTF-8")) {
            for (; ; ) {
                int rsz = in.read(buffer, 0, buffer.length);
                if (rsz < 0)
                    break;
                out.append(buffer, 0, rsz);
            }
        } catch (UnsupportedEncodingException ex) {
            /* ... */
        } catch (IOException ex) {
            /* ... */
        }
        return out.toString();
    }
}
