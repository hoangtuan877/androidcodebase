package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.screens.discussion.repository.DiscussionTopicsRepository;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RunWith(MockitoJUnitRunner.class)
public class DiscussionCreateNewViewModelTest {
    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private MockWebServer mockServer;
    private JsonUtil jsonUtil;

    private DiscussionWebService discussionWebService;
    private DiscussionCreateNewViewModel discussionCreateNewViewModel;
    private DiscussionTopicsRepository discussionTopicsRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();

        discussionWebService = retrofit.create(DiscussionWebService.class);
        discussionTopicsRepository = new DiscussionTopicsRepository(discussionWebService);
        DiscussionCreateNewViewModel.DiscussionCreateNewFactory factory = new DiscussionCreateNewViewModel.DiscussionCreateNewFactory(discussionTopicsRepository);
        discussionCreateNewViewModel = factory.create(DiscussionCreateNewViewModel.class);
    }

    @After
    public void tearDown() throws Exception {
        mockServer.shutdown();
    }

    @Test
    public void init() {
    }

    @Test
    public void createTopic() {
    }

    @Test
    public void updateTopic() {
    }

    @Test
    public void getIsCreateResult() {
    }

    @Test
    public void getNewTopic() {
    }

    @Test
    public void getErrorResponse() {
    }
}