package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.After;
import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.screens.discussion.repository.DiscussionRepository;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class DiscussionCoursesViewModelTest {
    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private MockWebServer mockServer;
    private JsonUtil jsonUtil;

    private DiscussionWebService discussionWebService;
    private DiscussionCoursesViewModel discussionCoursesViewModel;
    private DiscussionRepository discussionRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();

        discussionWebService = retrofit.create(DiscussionWebService.class);
        discussionRepository = new DiscussionRepository(discussionWebService);
        DiscussionCoursesViewModel.DiscussionCoursesFactory factory = new DiscussionCoursesViewModel.DiscussionCoursesFactory(discussionRepository);
        discussionCoursesViewModel = factory.create(DiscussionCoursesViewModel.class);
    }

    @After
    public void tearDown() throws Exception {
        mockServer.shutdown();
    }

    @Test
    public void getListDiscusstionOnAPI() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_discussion_main.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Discussion>> observer = Mockito.mock(Observer.class);

            discussionRepository.getListDiscussionOnAPI(0,10);
            discussionRepository.getListDiscussion().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<List<Discussion>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            assertEquals("Khóa Học Mới 16/6",captor.getValue().get(0).getName());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void init(){

        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_discussion_main.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Discussion>> observerListDiscussion = Mockito.mock(Observer.class);

            discussionRepository.getListDiscussionOnAPI(0,10);
            discussionRepository.getListDiscussion().observeForever(observerListDiscussion);

            Observer<Boolean> observerIsLast = Mockito.mock(Observer.class);

            discussionRepository.getListDiscussionOnAPI(0,10);
            discussionRepository.getIsLastDiscussionLoaded().observeForever(observerIsLast);

            mockServer.takeRequest();

            ArgumentCaptor<List<Discussion>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observerListDiscussion, Mockito.timeout(500)).onChanged(captor.capture());
            assertEquals("Khóa Học Mới 16/6",captor.getValue().get(0).getName());

            ArgumentCaptor<Boolean> captorIsLast = ArgumentCaptor.forClass(Boolean.class);
            Mockito.verify(observerIsLast, Mockito.timeout(500)).onChanged(captorIsLast.capture().booleanValue());
            assertEquals(false, captorIsLast.getValue().booleanValue());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getListDiscussion() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_discussion_main.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Discussion>> observer = Mockito.mock(Observer.class);

            discussionRepository.getListDiscussionOnAPI(0,10);
            discussionRepository.getListDiscussion().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<List<Discussion>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            assertEquals("Khóa Học Mới 16/6",captor.getValue().get(0).getName());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getIsLastDiscussionLoaded() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_discussion_main.json"));
            mockServer.enqueue(mockResponse);

            Observer<Boolean> observer = Mockito.mock(Observer.class);

            discussionRepository.getListDiscussionOnAPI(0,10);
            discussionRepository.getIsLastDiscussionLoaded().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<Boolean> captor = ArgumentCaptor.forClass(Boolean.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture().booleanValue());
            assertEquals(false, captor.getValue().booleanValue());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}