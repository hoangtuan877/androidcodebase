package cme.fpt.basecode.screens.coursedetail.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.io.IOException;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.model.Course;
import cme.fpt.basecode.screens.coursedetail.repository.CourseDetailRepository;
import cme.fpt.basecode.screens.coursedetail.repository.CourseDetailResponse;
import cme.fpt.basecode.screens.coursedetail.webapi.CourseDetailWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest({LifecycleOwner.class, Observer.class})
public class CourseDetailViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();
    private CourseDetailViewModel courseDetailViewModel;
    private CourseDetailRepository courseDetailRepository;
    private CourseDetailWebService courseDetailWebService;
    private MockWebServer mockServer;
    private JsonUtil jsonUtil;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        courseDetailWebService = retrofit.create(CourseDetailWebService.class);

        courseDetailRepository = new CourseDetailRepository(courseDetailWebService);

        CourseDetailViewModel.CourseDetailFactory factory = new CourseDetailViewModel.CourseDetailFactory(courseDetailRepository);

        courseDetailViewModel = factory.create(CourseDetailViewModel.class);
    }

    @After
    public void tearDown() throws IOException {
        mockServer.shutdown();
    }

    @Test
    public void init() {

    }

    @Test
    public void submitRating() {
    }

    @Test
    public void enrollCourse() {
    }

    @Test
    public void getCourse() {
    }

    @Test
    public void setCourse() {
    }

    @Test
    public void getRatingCourse() {
    }

    @Test
    public void setRatingCourse() {
    }

    @Test
    public void getLessonResult() {
    }

    @Test
    public void setLessonResult() {
    }

    @Test
    public void getTraineeStatus() {
    }

    @Test
    public void setTraineeStatus() {
    }

    @Test
    public void getRegistration() {
    }

    @Test
    public void setRegistration() {
    }

    @Test
    public void updateTraineeStatus() {
    }

    @Test
    public void getErrorResponse() {
    }

    @Test
    public void setErrorResponse() {
    }

    @Test
    public void updateSelectedLesson() {
    }

    @Test
    public void getRelatedCourses() {
    }

    @Test
    public void setRelatedCourses() {
    }
}