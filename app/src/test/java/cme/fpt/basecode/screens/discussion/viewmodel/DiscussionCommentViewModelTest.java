package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.model.Comment;
import cme.fpt.basecode.model.Reply;
import cme.fpt.basecode.screens.discussion.repository.DiscussionCommentRepository;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class DiscussionCommentViewModelTest {
    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private MockWebServer mockServer;
    private JsonUtil jsonUtil;

    private DiscussionWebService discussionWebService;
    private DiscussionCommentViewModel discussionCommentViewModel;
    private DiscussionCommentRepository discussionCommentRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();

        discussionWebService = retrofit.create(DiscussionWebService.class);
        discussionCommentRepository = new DiscussionCommentRepository(discussionWebService);
        DiscussionCommentViewModel.DiscussionCommentFactory factory = new DiscussionCommentViewModel.DiscussionCommentFactory(discussionCommentRepository);
        discussionCommentViewModel = factory.create(DiscussionTopicsViewModel.class);
    }

    @After
    public void tearDown() throws Exception {
        mockServer.shutdown();
    }

    @Test
    public void init() {
    }

    @Test
    public void clean() {
    }

    @Test
    public void getReplyByComment() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/lisst_reply.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Reply>> observer = Mockito.mock(Observer.class);

            discussionCommentRepository.getReplyByComment(32);
            discussionCommentRepository.getReplies().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<List<Reply>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            Assert.assertEquals("Dsffsd fsdfsd",captor.getValue().get(0).getContent());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void updateComment() {
    }

    @Test
    public void deleteComment() {
    }

    @Test
    public void callUpdateCommentAPI() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_comment.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Comment>> observer = Mockito.mock(Observer.class);

            discussionCommentRepository.updateCommentList(35,0,10);
            discussionCommentRepository.getAddMoreComments().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<List<Comment>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            Assert.assertEquals("sss",captor.getValue().get(0).getContent());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createReply() {
    }

    @Test
    public void updateReply() {
    }

    @Test
    public void deleteReply() {
    }

    @Test
    public void deleteTopic() {
    }

    @Test
    public void getTopic() {
    }

    @Test
    public void getAddMoreComments() {
    }

    @Test
    public void getReplies() {
    }

    @Test
    public void getNewComment() {
    }

    @Test
    public void getCallAPIResult() {
    }

    @Test
    public void getNewReply() {
    }

    @Test
    public void getErrorResponse() {
    }

    @Test
    public void getOwner() {
    }

    @Test
    public void getDeletedCommentId() {
    }

    @Test
    public void getDeletedReplyId() {
    }

    @Test
    public void getIsLastCommentLoaded() {
    }

    @Test
    public void getDeleteTopicResult() {
    }
}