package cme.fpt.basecode.screens.dashboard.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.model.Content;
import cme.fpt.basecode.model.Notification;
import cme.fpt.basecode.model.StudyClass;
import cme.fpt.basecode.screens.dashboard.repository.DashboardRepository;
import cme.fpt.basecode.screens.dashboard.webapi.DashboardWebService;
import cme.fpt.basecode.screens.login.repository.LoginRepository;
import cme.fpt.basecode.screens.login.repository.LoginResponse;
import cme.fpt.basecode.screens.login.viewmodel.LoginViewModel;
import cme.fpt.basecode.screens.login.webapi.LoginWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;

/**
 * Created by PhucNT17 on 6/19/2018.
 */

@RunWith(MockitoJUnitRunner.class)
public class DashboardViewModelTest {
    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();
    private MockWebServer mockServer;
    private JsonUtil jsonUtil;
    private DashboardWebService dashboardWebService;
    private DashboardRepository dashboardRepository;
    private DashboardViewModel dashboardViewModel;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        dashboardWebService = retrofit.create(DashboardWebService.class);
        dashboardRepository = new DashboardRepository(dashboardWebService);
        DashboardViewModel.DashboardFactory factory = new DashboardViewModel.DashboardFactory(dashboardRepository);
        dashboardViewModel = factory.create(DashboardViewModel.class);
    }

    @After
    public void tearDown() throws Exception {
        mockServer.shutdown();
    }

    @Test
    public void getStudyClasses() throws Exception {
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/dashboard/top_study_classes.json"));
        mockServer.enqueue(mockResponse);

        //  Observer<LoginResponse> observer = Mockito.mock(Observer.class);
        Observer<List<StudyClass>> observer = Mockito.mock(Observer.class);

        dashboardRepository.getTopStudyClass();
        dashboardRepository.getListStudyClass().observeForever(observer);
        //  dashboardViewModel.getStudyClasses().observeForever(observer);

        mockServer.takeRequest();

        ArgumentCaptor<List<StudyClass>> captor = ArgumentCaptor.forClass(List.class);
        Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
        assertEquals("Khóa huấn luyện khởi nghiệp",captor.getValue().get(0).getCategoryName());

    }

    @Test
    public void getNotifications() throws Exception {
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/dashboard/top_notification_classes.json"));
        mockServer.enqueue(mockResponse);
        Observer<List<Notification>> observer = Mockito.mock(Observer.class);

        dashboardRepository.getTopNotifications();
        dashboardRepository.getListNotification().observeForever(observer);
        mockServer.takeRequest();

        ArgumentCaptor<List<Notification>> captor = ArgumentCaptor.forClass(List.class);
        Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
        assertEquals("REGISTER_SUCCESS",captor.getValue().get(0).getType());

    }

    @Test
    public void getDiscussion() throws Exception {
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/dashboard/top_discussion_classes.json"));
        mockServer.enqueue(mockResponse);
        Observer<List<Content>> observer = Mockito.mock(Observer.class);

        dashboardRepository.getTopDiscussions();
        dashboardRepository.getmListContent().observeForever(observer);
        mockServer.takeRequest();

        ArgumentCaptor<List<Content>> captor = ArgumentCaptor.forClass(List.class);
        Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
        assertEquals("Khóa Học 2 bài học",captor.getValue().get(0).getDiscussionName());

    }

    @Test
    public void getSeenNotification() throws Exception {
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/dashboard/top_seen_notification.json"));
        mockServer.enqueue(mockResponse);
        Observer<List<Content>> observer = Mockito.mock(Observer.class);

        dashboardRepository.seenNotification(new int[]{1,2});
        //  dashboardRepository.get
    }

}