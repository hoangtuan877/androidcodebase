package cme.fpt.basecode.screens.login.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.model.UserInfo;
import cme.fpt.basecode.screens.login.repository.LoginRepository;
import cme.fpt.basecode.screens.login.repository.LoginResponse;
import cme.fpt.basecode.screens.login.webapi.LoginWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class LoginViewModelTest {
    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private LoginViewModel loginViewModel;
    private LoginRepository loginRepository;
    private LoginWebService loginWebService;
    private MockWebServer mockServer;
    private JsonUtil jsonUtil;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        loginWebService = retrofit.create(LoginWebService.class);

        loginRepository = new LoginRepository(loginWebService);

        LoginViewModel.LoginFactory factory = new LoginViewModel.LoginFactory(loginRepository);

        loginViewModel = factory.create(LoginViewModel.class);
    }

    @After
    public void tearDown() throws Exception {
        mockServer.shutdown();
    }

    @Test
    public void login() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/login/login.json"));
            mockServer.enqueue(mockResponse);

            Observer<LoginResponse> observer = Mockito.mock(Observer.class);

            loginViewModel.login("SYSTEM_admin", "@bcd1234");
            loginViewModel.getLoginData().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<LoginResponse> captor = ArgumentCaptor.forClass(LoginResponse.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            assertEquals("SYSTEM_admin",captor.getValue().getUserInfo().getUsername().toString());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void loginFailUsername() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(401).setBody(jsonUtil.getJson("json/login/login_fail.json"));
            mockServer.enqueue(mockResponse);

            Observer<LoginResponse> observer = loginResponse -> {
                assertEquals(401, loginResponse.getResponseCode());
            };

            loginViewModel.login("SYSTEM_admin", "@bcd12345");
            loginViewModel.getLoginData().observeForever(observer);
            mockServer.takeRequest();

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void loginFailServer() {
        try {
            MockResponse mockResponse = new MockResponse().setHttp2ErrorCode(500);
            mockServer.enqueue(mockResponse);

            Observer<LoginResponse> observer = loginResponse -> {
                assertEquals(600, loginResponse.getResponseCode());
            };

            loginViewModel.login("SYSTEM_admin", "@bcd1234");
            loginViewModel.getLoginData().observeForever(observer);
            mockServer.takeRequest();

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getLRSAuthVal() {
        String lrsAuth = "MjViOTNmYzhlODFlNGY0ZDgxZTc0OTcxNzY2OGEwNjg5MDhkYjNmODo2ZGQxMWY3MGQ3MWVkM2ZhMjk3ZDgxZGY2M2FjYmRjNjA5MGYxZjVh";
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(lrsAuth);
        mockServer.enqueue(mockResponse);

        Observer<String> observer = s -> Assert.assertNotNull(s);

        loginViewModel.getLRSAuthVal();
        loginViewModel.getLrsBasicAuthValLiveData().observeForever(observer);
        try {
            mockServer.takeRequest();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getLRSEndpoint() {
        // "http://ec2-13-250-179-232.ap-southeast-1.compute.amazonaws.com/data/xAPI"
        String lrsEndpoint = "MjViOTNmYzhlODFlNGY0ZDgxZTc0OTcxNzY2OGEwNjg5MDhkYjNmODo2ZGQxMWY3MGQ3MWVkM2ZhMjk3ZDgxZGY2M2FjYmRjNjA5MGYxZjVh";
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(lrsEndpoint);
        mockServer.enqueue(mockResponse);

        Observer<String> observer = s -> Assert.assertNotNull(s);

        loginViewModel.getLRSEndpoint();
        loginViewModel.getLrsEndPointLiveData().observeForever(observer);
        try {
            mockServer.takeRequest();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}