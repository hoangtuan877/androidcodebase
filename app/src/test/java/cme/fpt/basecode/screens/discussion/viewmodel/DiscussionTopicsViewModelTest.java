package cme.fpt.basecode.screens.discussion.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import cme.fpt.basecode.JsonUtil;
import cme.fpt.basecode.model.Discussion;
import cme.fpt.basecode.model.Topic;
import cme.fpt.basecode.screens.discussion.repository.DiscussionTopicsRepository;
import cme.fpt.basecode.screens.discussion.webapi.DiscussionWebService;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class DiscussionTopicsViewModelTest {
    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private MockWebServer mockServer;
    private JsonUtil jsonUtil;

    private DiscussionWebService discussionWebService;
    private DiscussionTopicsViewModel discussionTopicsViewModel;
    private DiscussionTopicsRepository discussionTopicsRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockServer = new MockWebServer();
        mockServer.start();
        jsonUtil = new JsonUtil();
        String BASE_URL = mockServer.url("/").toString();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();

        discussionWebService = retrofit.create(DiscussionWebService.class);
        discussionTopicsRepository = new DiscussionTopicsRepository(discussionWebService);
        DiscussionTopicsViewModel.DiscussionTopicsFactory factory = new DiscussionTopicsViewModel.DiscussionTopicsFactory(discussionTopicsRepository);
        discussionTopicsViewModel = factory.create(DiscussionTopicsViewModel.class);
    }

    @After
    public void tearDown() throws Exception {
        mockServer.shutdown();
    }

    @Test
    public void init() {
    }

    @Test
    public void getCurrentSection() {

    }

    @Test
    public void getTopics() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_topic_discussion.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Topic>> observer = Mockito.mock(Observer.class);

            discussionTopicsRepository.getListTopicOnAPI(5,0,10);
            discussionTopicsRepository.getTopics().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<List<Topic>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            Assert.assertEquals("Quản trị hệ thống",captor.getValue().get(0).getDiscussionName());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDiscussion() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_topic_discussion.json"));
            mockServer.enqueue(mockResponse);

            Observer<Discussion> observer = Mockito.mock(Observer.class);

            discussionTopicsRepository.getListTopicOnAPI(5,0,10);
            discussionTopicsRepository.getDiscussion().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<Discussion> captor = ArgumentCaptor.forClass(Discussion.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            Assert.assertEquals("Khóa Học 2 bài học",captor.getValue().getName());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getIsEmptyTopis() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_topic_discussion.json"));
            mockServer.enqueue(mockResponse);

            Observer<Boolean> observer = Mockito.mock(Observer.class);

            discussionTopicsRepository.getListTopicOnAPI(5,0,10);
            discussionTopicsRepository.getEmptyTopics().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<Boolean> captor = ArgumentCaptor.forClass(Boolean.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            assertEquals(false, captor.getValue().booleanValue());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getErrorResponse() {
    }

    @Test
    public void getIsLastTopicLoaded() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_discussion_main.json"));
            mockServer.enqueue(mockResponse);

            Observer<Boolean> observer = Mockito.mock(Observer.class);

            discussionTopicsRepository.getListTopicOnAPI(5,0,10);
            discussionTopicsRepository.getIsLastTopicLoaded().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<Boolean> captor = ArgumentCaptor.forClass(Boolean.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            assertEquals(false, captor.getValue().booleanValue());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getListTopicOnAPI() {
        try {
            MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(jsonUtil.getJson("json/discussion/list_topic_discussion.json"));
            mockServer.enqueue(mockResponse);

            Observer<List<Topic>> observer = Mockito.mock(Observer.class);

            discussionTopicsRepository.getListTopicOnAPI(5,0,10);
            discussionTopicsRepository.getTopics().observeForever(observer);

            mockServer.takeRequest();

            ArgumentCaptor<List<Topic>> captor = ArgumentCaptor.forClass(List.class);
            Mockito.verify(observer, Mockito.timeout(500)).onChanged(captor.capture());
            Assert.assertEquals("Khóa Học 2 bài học",captor.getValue().get(0).getDiscussionName());

        } catch (ComparisonFailure e) {
            e.printStackTrace();
            fail();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}